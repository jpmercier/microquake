# README #

Microquake is an open source package to be licensed under the [GNU general Public License, version 3 (GPLv3)](http://www.gnu.org/licenses/gpl-3.0.html). Microquake is an extension of Obspy for the processing of microseismic data

### Package structure ###

The package structure is not final and will settle over time

#### Content ####

* core: core function similar to Obspy
* db: the database related functions and objects (SQL and NoSQL)
* doc: documentation
* examples: examples on how to use the library as ipython notebook
* focmec: proto interface to the Focmec software (not working yet)
* imaging: Imaging functions (see Obspy imaging)
* mag: Magnitude calculations
* mapred: Hadoop map/reduce functions (deprecated)
* nlloc: Interface to NonLinLoc (Anthony Lomax)
* plugin: Plugins for reading various formats
* realtime: Real time data processing (see Obspy realtime)
* signal: (see obspy signal)
* simul: Location error and system sensitivity simulation
* spark: Interface to Apache Spark used as a parallelization engine for some functions
* station: Station
* ui: User interface prototype for picking arrival time (to be improved and a little broken)
* waveform: event detection, onset time picking, event location, event characterization

### What do I need to get microquake running? ###

* Python 2.7
* Numpy 1.9+
* Scipy 0.14+
* Matplotlib
* SimpleJSON
* [PyProj](https://pypi.python.org/pypi/pyproj)
* MySQLdb
* [ObsPy 0.9](http://docs.obspy.org/index.html), (version 1+ not supported yet)
* [Inflect](https://pypi.python.org/pypi/inflect)
* [NonLinLoc 6.0+](http://alomax.free.fr/nlloc/)
* [mrjob](https://pythonhosted.org/mrjob/) (deprecated)
* python-dateutil
* pytz
* lxml
* version
* memoize
* pyevtk
* pyvtk
* pyspark
* py4j (for spark)
* SQLAlchemy 0.9+ (only for experimental dB interaction)
* pymongo (only for experimental dB interaction)
* scikit-fmm'  # for the eikonal solver
* mplstereonet' # to plot the station (experimental)

### Contribution guidelines ###

* Text editor - 
* Please use spaces instead of tabs for Python indentation to avoid conflicts
* TODO.org files are in orgmode format. For instance, you can use the **orgmode** package in Sublime Text Package Control for dynamic checkboxes and more!
* Code format adheres as close as possible to [PEP8](https://www.python.org/dev/peps/pep-0008/) style convention. Please use **Flake8Lint** package if using Sublime Text Package Control for automatic highlight of style errors. A custom style template can be found in Flake8Lint.sublime-settings
* Function documentation in code should be in [reST standard](http://stackoverflow.com/questions/5334531/python-documentation-standard-for-docstring)