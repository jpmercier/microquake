uquakepy.waveform package
=====================

Submodules
----------

uquakepy.waveform.mag module
------------------------

.. automodule:: uquakepy.waveform.mag
    :members:
    :undoc-members:
    :show-inheritance:

uquakepy.waveform.pick module
-------------------------

.. automodule:: uquakepy.waveform.pick
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: uquakepy.waveform
    :members:
    :undoc-members:
    :show-inheritance:
