uquakepy.mag package
====================

Submodules
----------

uquakepy.mag.core module
------------------------

.. automodule:: uquakepy.mag.core
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: uquakepy.mag
    :members:
    :undoc-members:
    :show-inheritance:
