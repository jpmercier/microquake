.. _apiref:

==============
 API Reference
==============

:Release: |version|
:Date: |today|

.. toctree::
    :maxdepth: 1

    uquakepy
    uquakepy.db
    uquakepy.nlloc
    uquakepy.signal
    uquakepy.core.data
    uquakepy.db.schema
    uquakepy.realtime
    uquakepy.signal.trigger
    uquakepy.core
    uquakepy.db.sql
    uquakepy.realtime.signal
    uquakepy.simul
    uquakepy.core.util.attribdict
    uquakepy.imaging
    uquakepy.source_mechanism
    uquakepy.core.util
    uquakepy.mag
    uquakepy.waveform
    uquakepy.station