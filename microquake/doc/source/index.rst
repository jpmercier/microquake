.. uquakepy documentation master file, created by
   sphinx-quickstart on Thu Oct 22 19:56:45 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to uquakepy's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2
   api/index
   setup/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`setup`

