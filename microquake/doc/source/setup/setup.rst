LINUX
=====

==========================
INSTALLING PYTHON PACKAGES
==========================

microquake
----------

installing dependancies

Microquake depends on several python library some of which require non python
 library/package to compile. To build and install the package using PIP the
 following library need to be installed using a package manager such as
 apt-get, aptitude, yum etc. on Linux or Brew on macos.

For exemple on Linux one would need to install the following:

General development/build
.. code:: bash
    sudo apt-get install build-essential python-dev

Compilers/code integration
.. code:: bash
    sudo apt-get install gfortran
    sudo apt-get install swig

Numerical/algebra packages
.. code:: bash
    sudo apt-get install libatlas-dev
    sudo apt-get install liblapack-dev

Fonts (for matplotlib)
.. code:: bash
    sudo apt-get install libfreetype6 libfreetype6-dev

More fonts (for matplotlib on Ubuntu Server 14.04)
.. code:: bash
    sudo apt-get install libxft-dev

Graphviz for pygraphviz, networkx, etc.
.. code:: bash
    sudo apt-get install graphviz libgraphviz-dev

Python require pandoc for document conversions, printing, etc.
.. code:: bash
    sudo apt-get install pandoc

Tinkerer dependencies
.. code:: bash
    sudo apt-get install libxml2-dev libxslt-dev zlib1g-dev

.. code:: bash
    sudo easy_install pip
    sudo pip install ipython  # installing IPython for interractive Python
    sudo pip install notebook  # for running IPython notebook
    sudo pip install numpy # needs to be install othewise Obspy complains
    sudo pip install pandas  # optional but recommended

.. code:: bash
    pip install -e git+https://jpmercier@bitbucket.org/jpmercier/microquake.git#egg=microquake

====================
INSTALLING NONLINLOC
====================


.. code:: bash

    mkdir ~/bin
    cd NLL_DIRECTORY
    wget http://alomax.free.fr/nlloc/soft6.00/tar/NLL6.00_src.tgz
    tar -zxvf NLL6.00_src.tgz
    cd NLL_DIRECTORY/src
    make -R all

Add the following line to the .bashrc

export PATH=$PATH:$HOME/bin
