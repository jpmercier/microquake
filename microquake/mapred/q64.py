"""
.. module:: q64
   :synopsis: A useful module indeed.

.. moduleauthor:: Andrei Pascu <apascu@golder.com>

"""
import numpy as np
import os
import StringIO
import base64
import pickle
from microquake.mapred import io
from microquake.core import logger

# from IPython.core.debugger import Tracer


def load_segy(buf, meta_filename, only_valid=False):
	"""Load a SEGY file from a Q64 encoded buffer string into an
	obspy stream. Additionally, it loads a metadata file into the
	Stream and Trace stats headers.

	:param buf: Q64 encoded buffer string
	:type buf: str
	:param meta_filename: path to the metadata file
	:type meta_filename: str
	:param only_valid: If set to ``False``, the traces that have invalid position
		information will be discarded
	:type only_valid: bool
	:returns:  :py:class:`obspy.core.stream.Stream` -- the stream containing
		all of the SEGY data

	"""

	buf2 = '%s\n' % (buf,)
	buf3 = buf2.replace(' ', '\n')
	data = base64.b64decode(buf3)
	fle = StringIO.StringIO(data)

	st = io.load_segy_with_metadata(fle, meta_filename, only_valid=only_valid)
	return st


def find_stream(data_dir, filename, key):
	"""Load a Q64 file and finds a key in the line header.
	This is done through a index file.

	See also: :func:`readIndexFile`, :func:`makeIndexFile`
	"""
	filepath = '%s/%s.q64' % (data_dir, filename)

	if not os.path.isfile(filepath):
		logger.error('No data')
		return

	seek = read_index_file(data_dir, filename)
	seekIndex = np.where(seek['key'] == key)[0]
	if not seekIndex:
		logger.error('No data')
		return

	seek_pos = seek['seek'][seekIndex[0]]

	with open(filepath, 'r') as fle:

		fle.seek(seek_pos)
		line = fle.readline()

		if len(line) == 0:
			logger.error('No data')
			return

		return line


def read_index_file(data_dir, filename, event_seismogram=True):
	"""Loads an index file for a Q64 data file. The index is a
	CSV file containg the line key and the seek position of the
	line in the Q64 file. If the index file doesn't exist,
	it is created.

	See also: :func:`makeIndexFile`
	"""
	filepath = '%s/%s.index' % (data_dir, filename)
	logger.info('Reading index file: %s' % filepath)

	if not os.path.isfile(filepath):
		logger.info('Generating index file')
		make_index_file(data_dir, filename, event_seismogram=event_seismogram)

	if not os.path.isfile(filepath):
		logger.error('No data')
		return

	if event_seismogram:
		index = np.genfromtxt(filepath, dtype=[('key', 'S24'), ('seek', '<i8')], delimiter=",")
	else:
		index = np.genfromtxt(filepath, dtype=[('key', 'S24')], delimiter=",")

	return index


def make_index_file(data_dir, filename, event_seismogram=True):
	"""Creates an index file for a Q64 data file. The index is a
	CSV file containg the line key and the seek position of the
	line in the Q64 file.
	"""
	filepath = '%s/%s.q64' % (data_dir, filename)
	indexpath = '%s/%s.index' % (data_dir, filename)

	dat = []
	with open(filepath, 'r') as fle:
		pos = 0
		for line in fle:
			if len(line) == 0:
				continue

			if event_seismogram:
				header = line.split('\t')[0]
				dat.append(np.array([header.strip('"'), pos]))
				pos += len(line)
			else:
				pass
				# st = loadSegy(line, metadata)
				# fid = st.traces[0].stats.starttime.strftime('%Y/%m/%d %H:%M:%S')
				# print fid
				# dat.append(fid)
				# outfle2.write('%s\n' % (fid,))

	dat = np.array(dat)
	if event_seismogram:
		np.savetxt(indexpath, dat, fmt=['%s', '%s'], delimiter=',')
	else:
		np.savetxt(indexpath, dat.T, fmt=['%s'], delimiter=",")


def encode_q64(obj):
	"""Encodes any python object as a
	:py:mod:`base64` string.
	"""
	buf = pickle.dumps(obj, pickle.HIGHEST_PROTOCOL)
	buf2 = base64.b64encode(buf)
	buf3 = buf2.replace('\n', ' ')

	return buf3


def load_obj(buf):
	"""Decodes a :py:mod:`base64` string into a
	:py:class:`obspy.core.stream.Stream`.
	"""
	buf2 = '%s\n' % (buf,)
	buf3 = buf2.replace(' ', '\n')
	data = base64.b64decode(buf3)

	obj = pickle.loads(data)

	return obj


def read_q64(filename):
	"""Reads and decodes a file composed of
	:py:mod:`base64` strings into a list of
	:py:class:`obspy.core.trace.Trace`s.
	"""

	f = open(filename, 'rb')
	traces = []

	for line in f:
		data = line.split('\t')
		tr = load_stream(data[1])
		traces.append(tr)
		
	return traces
