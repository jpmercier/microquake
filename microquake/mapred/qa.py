"""
.. module:: q64
   :synopsis: A useful module indeed.

.. moduleauthor:: Andrei Pascu <apascu@golder.com>

"""
# import numpy as np
import os
import errno
import boto
from microquake.mapred import io
from microquake.core import logger

# from IPython.core.debugger import Tracer


def dl_progress(so_far, total):
	print('%d bytes transferred out of %d' % (so_far, total))


def cached_download(fname, bucketname, cache):

	cached_file = '%s%s' % (cache, fname)
	if os.path.exists(cached_file):
		logger.debug('File already in cache!')
		return cached_file

	conn = boto.connect_s3(aws_access_key_id=r'AKIAIJNGECUS5BYKJQ3A',
						   aws_secret_access_key=r'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')
	bucket = conn.get_bucket(bucketname)

	# make_sure_path_exists
	d = os.path.dirname(cached_file)
	try:
		os.makedirs(d)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

	logger.debug('Downloading %s' % fname)
	bucket.get_key(fname).get_contents_to_filename(cached_file, cb=dl_progress, num_cb=100)

	return cached_file


def load_segy(fname, meta_filename, only_valid=False, local=True, cache=None):
	"""Load a SEGY file from a the bucket into an
	obspy stream. Additionally, it loads a metadata file into the
	Stream and Trace stats headers.

	:param fname: file name of input SEGY
	:type fname: str
	:param meta_filename: path to the metadata file
	:type meta_filename: str
	:param only_valid: If set to ``False``, the traces that have invalid position
									information will be discarded
	:type only_valid: bool
	:returns:  :py:class:`obspy.core.stream.Stream` -- the stream containing
									all of the SEGY data

	"""
	if not local:
		bucketname = fname.split('://')[1].split('/')[0]
		fname = fname.split('://')[1].split('/')[1:]
		fname = '/'.join(fname)

		fle = cached_download(fname, bucketname, cache=cache)

	else:
		fle = fname

	st = io.load_segy_with_metadata(fle, meta_filename, only_valid=only_valid)
	return st
