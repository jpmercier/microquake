"""
.. module:: io
   :synopsis: A useful module indeed.

.. moduleauthor:: Andrei Pascu <apascu@golder.com>

"""
from obspy.segy.core import readSEGY
from obspy.station import inventory, network, station, channel
import numpy as np

from microquake.core import logger
from microquake.core.data import SessionManager, SeisData


def combine_dict_values(values):
	values2 = list(values)

	values = {}
	for value in values2:
		valuedict = eval(value)
		for tmpkey in valuedict.keys():
			if tmpkey not in values.keys():
			    values[tmpkey] = [np.array(valuedict[tmpkey])]
			else:
				values[tmpkey].append(np.array(valuedict[tmpkey]))

	for tmpkey in values.keys():
		values[tmpkey] = np.array(values[tmpkey])

	return values


def load_metadata_nrs(meta_filename, only_valid=False):

	metadata = np.genfromtxt(meta_filename, dtype=None, skip_header=9)

	if only_valid:
		valid_traces = np.nonzero(metadata['f18'])[0]
	else:
		valid_traces = range(len(metadata))
		# valid_traces = np.nonzero(metadata['f1'] > 1000)[0]

	# invalid_traces = np.setdiff1d(np.arange(len(metadata)), valid_traces, assume_unique=True).tolist()

	net = network.Network('XXXX')
	inv = inventory.Inventory([net], '')
	data = {'metadata': metadata, 'inventory': inv, 'valid_tr':valid_traces}

	return data


def update_inventory_from_segy_info(inv, st, metadata, valid_traces):

	header_size = 80
	split_header = [st.stats.textual_file_header[i:i + header_size] for i in
					range(0, len(st.stats.textual_file_header), header_size)]
	net_code = split_header[1].split(':')[-1].strip()

	inv.networks[0].code = net_code

	for c in valid_traces:
		cur_meta = metadata[c]

		# find current station
		station_name = '%s_%02d' % (cur_meta[5], cur_meta[7])

		found = False
		sta = None
		for sta in inv.networks[0].stations:
			if sta.code == station_name:
				found = True

		if not found:
			continue

		# find current channel
		cha_name = str(cur_meta[6])

		found = False
		cha = None
		for cha in sta.channels:
			if cha.code == cha_name:
				found = True

		if not found:
			continue

		tr = st.traces[c]

		# update station location
		rx = float(tr.stats.segy.trace_header.group_coordinate_x)
		ry = float(tr.stats.segy.trace_header.group_coordinate_y)
		rz = float(tr.stats.segy.trace_header.datum_elevation_at_receiver_group)
		pos = [rx, ry, rz]

		if rx <= 1000:
			sta.channels.remove(cha)
			sta.selected_number_of_channels -= 1
			sta.total_number_of_channels -= 1

			if not sta.channels:
				inv.networks[0].stations.remove(sta)
				inv.networks[0].selected_number_of_stations -= 1
				inv.networks[0].total_number_of_stations -= 1

			continue

		# sta.latitude = ry
		# sta.longitude = rx
		# sta.elevation = rz
		sta.station_location = pos  # custom attribute
		sta.station_type = cur_meta[5]  # custom attribute

		# update channel gain
		gain = tr.stats.segy.trace_header.gain_type_of_field_instruments

		cha.gain = gain


def update_seisdata_from_inventory(st, inv):

	# keep only traces that exist in the inventory
	all_stations = np.unique([sta.code for sta in inv.networks[0].stations])
	for tr in reversed(st):
		tr.stats.network = inv.networks[0].code

		if tr.stats.station not in all_stations:
			# logger.debug(tr.stats.station)
			st.remove(tr)

def load_segy_with_inventory(fname, inventory_filename):
	"""Load a SEGY file as a SeisData object.
	Additionally, it loads a NRS formatted metadata file.

	:param fname: file name of input SEGY
	:type fname: str
	:param inventory_filename: path to the StationML inventory file
	:type inventory_filename: str
	:returns:  :class:`microquake.core.data.session.SeisSession`
		a session containing the :class:`microquake.core.data.trace.SeisData`
		(the stream containing all of the SEGY data)
		and a seismic inventory containing all the metadata
	"""
	try:
		st = SeisData(readSEGY(fname, unpack_trace_headers=True))
	except Exception, e:
		logger.debug('Failed to load SEGY - %s' % e.message)
		return False

	inv = inventory.read_inventory(inventory_filename)

	update_seisdata_from_inventory(st, inv)

	session = SessionManager().active_session()
	session.load_network(inv)
	return st, inv


def load_segy_with_metadata(fname, meta_filename, only_valid=False):
	"""Load a SEGY file as a SeisData object.
	Additionally, it loads a NRS formatted metadata file.

	:param fname: file name of input SEGY
	:type fname: str
	:param meta_filename: path to the metadata file
	:type meta_filename: str
	:param only_valid: If set to ``False``, the traces that have invalid position
									information will be discarded
	:type only_valid: bool
	:returns:  :class:`microquake.core.data.session.SeisSession`
		a session containing the :class:`microquake.core.data.trace.SeisData`
		(the stream containing all of the SEGY data)
		and a seismic inventory containing all the metadata
	"""
	try:
		st = SeisData(readSEGY(fname, unpack_trace_headers=True))
	except Exception, e:
		logger.debug('Failed to load SEGY - %s' % e.message)
		return False

	metadata = load_metadata_nrs(meta_filename, only_valid=only_valid)
	inv = metadata['inventory']

	for c in metadata['valid_tr']:

		tr = st.traces[c]
		cur_meta = metadata['metadata'][c]

		meta_pos = [cur_meta[1], cur_meta[2], cur_meta[3]]
		orient = [cur_meta[13], cur_meta[14], cur_meta[15]]

		station_name = '%s_%02d' % (cur_meta[5], cur_meta[7])
		cha_name = str(cur_meta[6])

		tr.stats.station = station_name
		tr.stats.channel = cha_name

		# find or create current station
		found = False
		sta = None
		for sta in inv.networks[0].stations:
			if sta.code == station_name:
				found = True

		if not found:
			sta = station.Station(station_name, 0, 0, 0)  # use station location from segy instead
			sta.station_location_meta = meta_pos

			inv.networks[0].stations.append(sta)
			if inv.networks[0].selected_number_of_stations:
				inv.networks[0].selected_number_of_stations += 1
			else:
				inv.networks[0].selected_number_of_stations = 1

			if inv.networks[0].total_number_of_stations:
				inv.networks[0].total_number_of_stations += 1
			else:
				inv.networks[0].total_number_of_stations = 1

		# create new channel for station
		cha = channel.Channel(cha_name, cha_name, 0, 0, 0, 0)  # channel location not used
		cha.orientation = orient  # custom attribute
		sta.channels.append(cha)

		if sta.selected_number_of_channels:
			sta.selected_number_of_channels += 1
		else:
			sta.selected_number_of_channels = 1

		if sta.total_number_of_channels:
			sta.total_number_of_channels += 1
		else:
			sta.total_number_of_channels = 1

		sta.alternate_code = cur_meta[0]

	update_inventory_from_segy_info(inv, st,
									metadata['metadata'],
									metadata['valid_tr'])

	update_seisdata_from_inventory(st, inv)

	session = SessionManager().active_session()
	session.load_network(inv)
	return st, inv
