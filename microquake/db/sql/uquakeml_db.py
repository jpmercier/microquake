from sqlalchemy import Table, Column, ForeignKey, Integer, Float, String, Enum, types, DateTime
from sqlalchemy.orm import mapper, class_mapper, relationship
# from sqlalchemy.orm.exc import UnmappedClassError
# from sqlalchemy.dialects.mysql import DATETIME
# from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Pick(Base):
	__tablename__ = 'pick'

	__onset_values = ('emergent', 'impulsive', 'questionable')
	__polarity_values = ('positive', 'negative', 'undecidable')
	__evaluation_mode_values = ('manual', 'automatic')
	__evaluation_status_values = ('preliminary', 'confirmed', 'reviewed',
		'final', 'rejected', 'reported')

	id = Column(Integer, primary_key=True)
	time = Column(DateTime)
	time_error = Column(Float)
	waveform_id = relationship("WaveformStream", backref="Pick", order_by="waveform.id")
	filter_id = relationship("ResourceIdentifier", backref='Pick', order_by='resource_identifier.id')
	method_id = relationship("ResourceIdentifier", backref='Pick', order_by='resource_identifier.id')
	azimuth = Column(Float)
	azimuth_error = Column(Float)
	take_off_angle = Column(Float)
	take_off_angle_error = Column(Float)
	back_azimuth = Column(Float)
	back_azimuth_error = Column(Float)
	onset = Column(Enum(*__onset_values))
	phase_hint = Column(String)
	polarity = Column(Enum(*__polarity_values))
	evaluation_mode = Column(Enum(*__evaluation_mode_values))
	evaluation_status = Column(Enum(*__evaluation_status_values))
	comments = Column(String)
	creation_info = relationship('CreationInfo', backref='Pick', order_by='creation_info.id')

	def __init__(pk=None):
		if pk:
			time = pk.time.datetime()
			time_error = pk.time_error
			waveform_id = 


