# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: mongo.py
#  Purpose: module to interract with mongo db
#   Author: microquake development team
#    Email: dev@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
module to interract with mongodb

:copyright:
    microquake development team (dev@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""


import json
import numpy as np
from json import dumps
from bson.json_util import loads
from microquake.core.util.attribdict import AttribDict


def connect(uri='mongodb://localhost:27017/', db_name='test'):
    """

    :param uri: uniform resource identifier
    :type uri: str
    :param db_name: database name
    :type db_name: str
    :return: pymongo db object
    """

    from pymongo import MongoClient
    client = MongoClient(uri)
    db = client[db_name]
    return db


def insert_event(db, event, data_path, base_file_name, waveform_id,
                 catalog_index=0):
    """
    Write a microquake.core.event catalog into the project. Microquake
    only one event per catalog which are then stored in individual QuakeML
    files.
    :param event: event:
    :type event: microquake.core.event.Event
    :param data_path: path to raw data (seismogram and QuakeML catalog)
    :type data_path: str
    :param base_file_name: base file name for the seismogram and the QuakeML
    file. Basically the file name without the extension. (extension .xml is
    removed if provided)
    :type base_file_name: str
    :param waveform_id: id of the waveform in the db
    :type waveform_id: int
    :rtype bool
    """
    events_col = db['Event']

    events_to_insert = []

    # Skip if event already in DB
    cursor = events_col.find_one({'base_file_name': base_file_name,
        'data_path': data_path})
    if cursor:
        return None

    # Flatten event data
    ev_dict = flatten_event(event)
    ev_dict['data_path'] = data_path
    ev_dict['base_file_name'] = base_file_name
    ev_dict['waveform_id'] = waveform_id
    ev_dict['catalog_index'] = catalog_index  # the index of the event in
    #  the catalog. Should in principle always be 0. Included for future
    # proofing purposes

    inserted_id = db.Events.insert_one(ev_dict)

    #inserted_id = insert(events_col, ev_dict)

    return inserted_id


def insert_waveform(db, data_path, base_file_name):
    """
    Insert waveform information into the database
    :param data_path: path to raw data (seismogram and QuakeML catalog
    :type data_path: str
    :param base_file_name: base file name for the seismogram and the QuakeML
    file. Basically the file name without the extension.
    :type base_file_name: str
    :rtype bool
    """

    # Skip if waveform already in DB
    waveforms_col = db[waveform_col_name]
    cursor = waveforms_col.find_one({'base_file_name': base_file_name,
        'data_path': data_path})
    if cursor:
        return [cursor["_id"]]

    # Flatten event data
    waveform = {}
    waveform['data_path'] = data_path
    waveform['base_file_name'] = base_file_name

    inserted_ids = insert(waveforms_col, waveform)
    return inserted_ids


#TODO adding the function that are found in the project.py

def stream2dict(st):
    """
    convert an Obspy stream to AttribDict
    """

    stout = {}
    traces = []
    for tr in st:
        trout = {}
        if tr.stats.segy:
            del tr.stats.segy
        trout['data'] = list(tr.data)
        trout['stats'] = dict(tr.stats)
        traces.append(trout)

    stout['traces'] = traces

    return stout

# def cat2mongodb(cat):


def mongoobj2stream(dbobj):
    traces = []
    for tr in dbobj['traces']:
        stats = Stats()
        data = np.array(tr['data'])
        for key in tr['stats'].keys():
            if (key == 'starttime'):
                stats[key] = UTCDateTime(tr['stats'][key]).datetime
            if (key == 'endtime'):
                continue
            stats[key] = tr['stats'][key]
        stats.npts = tr['stats']['npts']
        traces.append(Trace(data=data, header=stats))

    stout = Stream(traces=traces)

    keys = dbobj.keys()
    if 'trigger_time' in keys:
        stout.trigger_time = np.array(dbobj['trigger_time'])
    if 'trigger_station' in keys:
         stout.trigger_station = np.array(dbobj['trigger_station'])
    if 'isvalid' in keys:
        stout.isvalid = dbobj['isvalid']
    
    return stout.copy()


def dict2Attribdict(obj):
    if isinstance(obj, dict):
        out = AttribDict()
        for key in obj.keys():
            out[key] = dict2Attribdict(obj[key])
        try:
            return out
        except:
            pass
    elif isinstance(obj, list):
        out = []
        for item in obj:
            out.append(dict2Attribdict(item))
        return out
    else:
        return obj


def AttribDict2dict(obj):
    if isinstance(obj, AttribDict):
        out = dict()
        for key in obj.keys():
            out[key] = AttribDict2dict(obj[key])
            return out
    elif isinstance(obj, list):
        out = []
        for item in obj:
            out.append(AttribDict2dict(item))
            return out
    else:
        return obj


def insert(col, obj, default=None, **kwargs):
    """
    Convert and insert an object into a mongo db collection
    :param col: mongo db collection
    :type catalog: pymongo.collection.Collection
    :param obj: object to insert
    :type obj: list of object or object
    :param default: "default" function for JSONEncoder for ObsPy objects
    :type default: microquake.core.json.default.Default
    """
    if not isinstance(obj, list):
        obj = [obj]

    tmp_list = []
    # noinspection PyTypeChecker
    for o in obj:
        s = dumps(o, default=default, **kwargs)
        tmp = loads(s)
        tmp_list.append(tmp)

    inserted_ids = col.insert_many(tmp_list).inserted_ids

    # return [(id, obj) for id, obj in zip(tmp_list, inserted_ids)]
    # return tmp_list, inserted_ids
    return inserted_ids

    if not isinstance(obj, list):
        obj = [obj]

    tmp_list = []


def flatten_event(event):
    """
    Extract essential information from an event
    :param event: an event object
    :type event: ~microquake.core.event.Event
    :return: return a dictionary with a selection of event properties
    """

    from microquake.core.util import q64

    ev = {}
    if event.preferred_origin():
        origin = event.preferred_origin()
    elif event.origins:
        origin = event.origins[-1]
    else:
        origin = None

    if event.preferred_magnitude():
        magnitude = event.preferred_magnitude()
    elif event.magnitudes:
        magnitude = event.magnitudes[-1]
    else:
        magnitude = None

    if origin:
        ev['x'] = origin.x
        ev['y'] = origin.y
        ev['z'] = origin.z
        ev['time'] = origin.time.datetime
        if origin.origin_uncertainty:
            if origin.origin_uncertainty.confidence_ellipsoid:
                confidence_ellipsoid = \
                    origin.origin_uncertainty.confidence_ellipsoid
                ev['uncertainty'] = confidence_ellipsoid.semi_major_axis_length
        ev['evaluation_mode'] = origin.evaluation_mode
        ev['status'] = getattr(origin, 'evaluation_status', 'not_picked')
        ev['event_type'] = getattr(event, 'event_type', 'not reported')

        sqr = 0
        ct = 0
        for arrival in origin.arrivals:
            if not arrival:
                continue
            ct += 1
            sqr += arrival.time_residual ** 2
        rms = np.sqrt(np.mean(sqr)) 
        
        ev['time_residual'] = rms
        ev['npick'] = len(origin.arrivals)

    if magnitude:
        ev['magnitude'] = magnitude.mag
        ev['magnitude_type'] = magnitude.magnitude_type

    ev['encoded_quakeml'] = q64.encode(event)

    return ev

def dump_event(event, db, mongo_col, data_path, base_file_name,
               **kwargs):
    """
    Write a microquake.core.event catalog into a mongo db
    :param event: event object
    :type event: microquake.core.event.Event
    :param db: mongodb database
    :type db: pymongo.database.Database
    :param mongo_col: collection name for events in db
    :type mongo_col: str
    :param data_path: path to raw data (seismogram and QuakeML catalog
    :type data_path: str
    :param base_file_name: base file name for the seismogram and the QuakeML
    file. Basically the file name without the extension.
    :type base_file_name: str
    :rtype: bool
    """

    event_col = db[mongo_col]
    default = Default()

    # Skip if event already in DB
    cursor = event_col.find_one({'base_file_name': base_file_name,
        'data_path': data_path})
    if cursor:
        return None

    event.data_path = data_path
    event.base_file_name = base_file_name
    event = flatten_event(event)

    insert(event_col, event, default=default, **kwargs)
