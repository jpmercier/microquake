"""
SQLAlchemy ORM definitions (database layout) for microquake.db.

:copyright:
	the microquake development team
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""

import pickle

from sqlalchemy import (Boolean, Column, DateTime, Float, ForeignKey, Integer,
                        PickleType, String)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation
from sqlalchemy.schema import UniqueConstraint
import numpy as np

from obspy import Trace, UTCDateTime
from obspy.core import event_header

Base = declarative_base()

class ConfidenceEllipsoid(Base):
    __tablename__ = 'confidence_ellipsoid'

    id = Column(Integer, primary_key=True)
    semi_major_axis_length = column(Float)
    semi_intermediate_axis_length = column(Float)
    semi_minor_axis_lenght = column(Float)
    major_axis_plunge = column(Float)
    major_axis_azimuth = column(Float)
    major_axis_rotation = column(Float)

# class 
