import logging

import numpy as np
import os

from microquake.db.schema.extra_db import *
from microquake.db.schema.quakeml_db import *
from microquake.db.schema.fdsn_station_db import *
from microquake.core.data import trace
from microquake.core import SeisCatalog

from IPython.core.debugger import Tracer
# import pdb
# import rpdb

def _cleanDuplicates(session, existing):
	item = None
	if existing:
		if len(existing) == 1:
			item = existing[0]
			create = False
		else:
			for i in existing:
				session.delete(i)

			create = True
	else:
		create = True

	return create, item


def _addPath(session, filepath):
	filepath = '/'

	# fetch or create path
	try:
		# search for existing path
		query = session.query(WaveformPath)
		path = query.filter(WaveformPath.path == filepath).one()
	except:
		path = WaveformPath()
		path.path = filepath
		session.add(path)

	return path


def _addFile(session, path, filename):
	# t0 = time.time()
	query = session.query(WaveformFile).join(WaveformPath)
	files = query.filter(WaveformFile.path_id == path.id,
						 WaveformFile.file == filename).all()
	# ex = session.query(exists().where(and_(WaveformFile.path_id==path.id,
	# WaveformFile.file==data.file))).scalar()

	# print "Existing files: %f secs" % (time.time() - t0,)

	# t0 = time.time()
	create, fle = _cleanDuplicates(session, files)

	if create:
		fle = WaveformFile()
		fle.file = filename
		fle.size = 0
		fle.mtime = 0
		fle.format = 'QUAKEML'
		path.files.append(fle)
		session.add(fle)

	# print "Create file: %f secs" % (time.time() - t0,)

	return fle

# def addCatalog(DBSession, catalog, root_path_to_waveforms, path_to_waveform):
def addCatalog(catalog, root_path_to_waveforms, path_to_waveform,
	host='localhost', port=3306, user='root', passwd='', db='mysql'):
	"""
	Add the information contained in a catalog object into an SQLDB
	:param session: microquake DBsession
	:type session: microquake.db.schema.db.DBSession
	:param catalog: "u"QuakeML catalog object ("obspy catalog object also ok")
	:type catalog: microquake.core.data.SeisCatalog
	:param root_path_to_waveform: root path to access the waveforms
	:type root_path_to_waveform: str
	:param path_to_waveform: full path to waveform including root path to waveform
	:type path_to_waveform: str

	:Example:

	from microquake.db.schema import db
	from microquake.db import utils
	from microquake.core import SeisCatalog
	from obspy.core.event import readEvents

	uri = 'mysql://root@localhost:3306/my_data_base'
	db_session = db.DBSession(uri)

	root_path_to_waveform = /ROOT/PATH/TO/WAVEFORM
	path_to_waveform = /ROOT/PATH/TO/WAVEFORM/YYYY/MM/DD/HH/FILENAME.EXT

	cat = SeisCatalog(readEvents(/PATH/TO/A/QUAKEML/FILE/ASSOCIATED/TO/WAVEFORM/FILE.XML))

	utils.addCatalog(db_session, SeisCatalog, root_path_to_waveform, path_to_waveform)

	.. seealso::
	.. warnings:: This function is not completely finished some catalog element 
	may not be fully copied to the database.
	Picks are associated one to one with picks (this may not be future proof!)
	.. note:: not sure what WaveformFile mtime represents. Always set to 1. 
	.. todo:: The QuakeML definition of the confidence ellipsoid does not allow
	for all the information to be stored only orientation of semi_major_axis can
	be stored. The schema may need to be adapted to better suits microquake needs.
	"""

	import pymysql
	conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=db, autocommit=True)
	cur = conn.cursor()

	cmd = "SELECT id FROM waveformpath"
	cur.execute(cmd)
	wfp = cur.fetchone()
	# session = DBSession.session

	# wfp = session.query(WaveformPath).filter_by(path=root_path_to_waveforms).first()

	if not wfp:
		cmd = "INSERT INTO `waveformpath` ( `path`, `archived`) VALUES ('%s', %d)" % (root_path_to_waveforms, 0)
		cur.execute(cmd)
		wfp_id = conn.insert_id()
	else:
		wfp_id = wfp[0]

	fle = path_to_waveform.replace(root_path_to_waveforms, "")
	
	
	cmd = "SELECT id FROM waveformfile WHERE waveformpath_id = %d" % wfp_id
	cur.execute(cmd)
	wf = cur.fetchone()

	if not wf:

		cmd = "INSERT INTO `waveformfile` ( `file`, `waveformpath_id`, `size`, `format`, `mtime`)" \
		" VALUES ('%s', %d, %f, '%s', %d)" % (fle, wfp_id, os.path.getsize(path_to_waveform),
			fle.split('.')[-1].upper(), 1)
		cur.execute(cmd)
		wfid = conn.insert_id()

	else:
		wfid = wf[0]

	# if isinstance(catalog, obspy.core.event.Catalog):
	catalog = SeisCatalog(catalog)

	for evt in catalog.events:
		# testing if an event with the attached origin exists
		have_evt = False

		for origin in evt.origins:
			dtstr = origin.time.strftime("%Y-%m-%d %H:%M:%S.%f")
			cmd = "SELECT event_id FROM `origin` where time = '%s'" % dtstr
			cur.execute(cmd)
			ori = cur.fetchone()
			if ori:
				eid = ori[0]
				have_evt = True

		if not have_evt:
			if not evt.event_type_certainty:
				evt.event_type_certainty = 'suspected'
			if not evt.event_type:
				evt.event_type = "not existing"

			cmd = "INSERT INTO `event` (`type_`, `typeCertainty`, `waveform_file_id`)" + \
			"VALUES ('%s', '%s', %d)" % (evt.event_type, evt.event_type_certainty, wfid)
			cur.execute(cmd)
			eid = conn.insert_id()

		for pick in evt.picks:
			ptimestr = pick.time.strftime("%Y-%m-%d %H:%M:%S.%f")
			cmd = "SELECT id FROM `pick` WHERE time = '%s' AND phaseHint = '%s' AND event_id = %d" % (ptimestr, pick.phase_hint, eid)
			cur.execute(cmd)
			pck = cur.fetchone()

			if not pck:
				# Tracer()()

				cmd = "SELECT id FROM station WHERE code = '%s'" % (pick.waveform_id.station_code)
				cur.execute(cmd)
				stid = cur.fetchone()[0]

				cmd = "SELECT id FROM channel WHERE station_id = %d AND code = '%s'" % \
				(stid, pick.waveform_id.channel_code)
				cur.execute(cmd)
				chid = cur.fetchone()[0]

				cmd = "SELECT id FROM waveformStreamID WHERE stationCode = '%s' AND networkCode = '%s' AND channelCode = '%s' AND valueOf_ = '%s'" \
				% (pick.waveform_id.station_code, pick.waveform_id.network_code, 
					pick.waveform_id.channel_code.lower(), fle)
				cur.execute(cmd)
				
				wfs = cur.fetchone()

				if not wfs:
					cmd = "INSERT INTO `waveformStreamID` (`networkCode`, `stationCode`, `channelCode`, `valueOf_`)" + \
					" VALUES ('%s', '%s', '%s', '%s')" % (pick.waveform_id.network_code,
														  pick.waveform_id.station_code,
														  pick.waveform_id.channel_code.lower(),
														  fle)
					cur.execute(cmd)
					wfsid = conn.insert_id()
				else:
					wfsid = wfs[0]

				if not pick.onset:
					pick.onset = 'impulsive'
				if not pick.polarity:
					pick.polarity = 'undecidable'
				

				cmd = "INSERT INTO `pick` (`time`, `onset`, `phaseHint`, `polarity`," + \
					" `evaluationMode`, `evaluationStatus`, `event_id`, `waveformID_id`)" + \
					" VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d, %d)" \
					% (ptimestr, pick.onset, pick.phase_hint, pick.polarity, 
					  pick.evaluation_mode, pick.evaluation_status, eid, wfsid)

				cur.execute(cmd)
				pckid = conn.insert_id()
	
		for origin in evt.origins:
			dtstr = origin.time.strftime("%Y-%m-%d %H:%M:%S.%f")
			cmd = "SELECT id FROM origin WHERE time = '%s'" % dtstr
			cur.execute(cmd)
			ori = cur.fetchone()
			
			if not ori:
				cmd = "INSERT INTO origin (`time`, `x`, `y`, `z`, `event_id`" + \
					", `evaluationMode`, `evaluationStatus`, `earthModelID`)" + \
					"VALUES ('%s', %f, %f, %f, %d, '%s', '%s', %d)" \
					% (dtstr, origin.x, origin.y, origin.z, eid, origin.evaluation_mode, 
					   origin.evaluation_status, origin.earth_model)
				cur.execute(cmd)
				oid = conn.insert_id()
			else:
				oid = ori[0]

			cmd = 'SELECT id from originUncertainty WHERE origin_id = %d' % oid
			cur.execute(cmd)
			ori_unc = cur.fetchone() 

			if (not ori_unc) and ('origin_uncertainty' in origin):
				# if ('confidence_ellipsoid' in origin.origin_uncertainty):
				cecat = origin.origin_uncertainty.confidence_ellipsoid

				cmd = "INSERT INTO confidenceEllipsoid (`semiMajorAxisLength`, `semiMinorAxisLength`," + \
					  "`semiIntermediateAxisLength`, `MajorAxisPlunge`, `majorAxisAzimuth`)" + \
					  "VALUES (%f, %f, %f, %f, %f)" \
					  % (cecat.semi_major_axis_length,
					  	 cecat.semi_minor_axis_length,
					  	 cecat.semi_intermediate_axis_length,
					  	 cecat.major_axis_plunge,
					  	 cecat.major_axis_azimuth)
				
				cur.execute(cmd)
				ceid = conn.insert_id()

				cmd = "INSERT INTO originUncertainty (`confidenceEllipsoid_id`, `origin_id`)" + \
				      "VALUES (%d, %d)" % (ceid, oid)

			for i, arrival in enumerate(origin.arrivals):
				pick = evt.picks[i]  # assuming here that all arrivals are associated one to one with a pick
				ptimestr = pick.time.strftime("%Y-%m-%d %H:%M:%S.%f")
				cmd = "SELECT id FROM `pick` WHERE time = '%s' AND phaseHint = '%s' AND event_id = %d" \
				% (ptimestr, pick.phase_hint, eid)
				
				cur.execute(cmd)
				pckid = cur.fetchone()[0]

				cmd = "SELECT id from `arrival` WHERE origin_id = %d AND pickID = %d" % (oid, pckid)
				cur.execute(cmd)
				arr = cur.fetchone()

				if not arr:
					cmd = "INSERT INTO `arrival` (`pickID`, `phase`, `timeCorrection`, `azimuth`," + \
						" `distance`, `takeoffAngle`, `timeResidual`, `origin_id`) VALUES (%d, '%s', %f, %f, %f, %f ,%f, %d)" \
					    % (pckid, arrival.phase, arrival.time_correction, arrival.azimuth,
					    	arrival.distance, arrival.takeoff_angle, arrival.time_residual, oid)

					cur.execute(cmd)

		for magnitude in evt.magnitudes:
			cmd = "SELECT id from `magnitude` WHERE originID = %d AND type_ = '%s' AND evaluationMode = '%s' AND event_id = %d" \
			       % (oid, magnitude.magnitude_type, magnitude.evaluation_mode, eid)
			cur.execute(cmd)
			mag = cur.fetchone()

			if not mag:
				# for origin in evt.origins:
				# 	if origin.resource_id.id == origin_id:
				# 		oid = session.query(Origin).filter_by(time=origin.time).first().id
				cmd = "INSERT INTO magnitude (`mag`, `type_`, `originID`, `stationCount`, `evaluationMode`, " + \
					  "`evaluationStatus`, `event_id`) VALUES (%f, '%s', %d, %d, '%s', '%s', %d)" \
	 				  % (magnitude.mag, magnitude.magnitude_type, oid, magnitude.station_count,
	 				  	magnitude.evaluation_mode, magnitude.evaluation_status, eid)
	 			cur.execute(cmd)



def addSite(DBSession, site):
	"""
	Add site information contained in a station object into an SQLDB
	:param session: microquake DBsession
	:type session: microquake.db.schema.db.DBSession
	:param station: station information
	:type station: microquake.core.station.Site

	..warnings:: the Site object structure is likely to change in a near future
	"""

	session = DBSession.session
	# from microquake.db.schema.fdsn_station_db import *

	ste = session.query(Site).filter_by(Name=site.code).first()

	if ste is None:
		# more properties will need to be added
		ste = Site(Name=site.code)
		session.add(ste)
	
	for network in site:
		net = session.query(Network).filter_by(code=network.code).first()
		ste = session.query(Site).filter_by(Name=site.code).first()
		if net is None:
			net = Network()
			net.code = network.code
			net.TotalNumberStations = len(network)
			session.add(net)
	        session.commit()
		for station in network.stations: # will need to be changed
			sta = session.query(Station).filter_by(code=station.code).first()
			if sta is None:
				sta = Station()
				sta.code = station.code
				sta.X = station.x
				sta.Y = station.y
				sta.Z = station.z
				sta.TotalNumberChannels = len(station)
				sta.network_id = net.id
				session.add(sta)
		session.commit()
		for station in network.stations:
			sta = session.query(Station).filter_by(code=station.code).first()
			equip = session.query(Equipment).filter_by(station_id=sta.id).first()
			# Tracer()()
			if equip is None:
				equip = Equipment()
				# This will need to grow to include other properties
				equip.station_id = sta.id
				equip.Type = station.sensor_type
				session.add(equip)

			for channel in station.channels:
				cha = session.query(Channel).filter_by(code=channel.code).first()
				if cha is None:
					cha = Channel()
					cha.code = channel.code
					cha.Azimuth = channel.azimuth
					cha.Dip = channel.dip
					cha.Type = "TRIGGERED"
					if np.isnan(cha.Dip):
						cha.Dip = 0.0
					if np.isnan(cha.Azimuth):
						cha.Azimuth = 0.0
					cha.Latitude = station.y
					cha.Longitude = station.x
					cha.Elevation = station.z
					cha.X = station.x
					cha.Y = station.y
					cha.Z = station.z
					cha.station_id = sta.id
					print (cha.Dip, cha.Azimuth)
					# Tracer()()
					session.add(cha)

	session.commit()




def createEvent(session, event_id):
	filepath = '/'
	filename = '_'

	# path = _addPath(session, filepath)
	# fle = _addFile(session, path, filepath)

	# t0 = time.time()
	# query = session.query(Event).join(WaveformFile)
	# events = query.filter(WaveformFile.file==fle).all()
	# print "Existing event: %f secs" % (time.time() - t0,)

	# t0 = time.time()
	# create, ev = _cleanDuplicates(session, events)
	create = True

	if create:

		evPar = session.query(EventParameters).first()
		if evPar is None:
			evPar = EventParameters(description='')
			session.add(evPar)

		ev = Event(type_='not reported', typeCertainty='suspected')
		# evDesc = session.query(EventDescription).first()
		# if evDesc is None:
		evDesc = EventDescription(text=event_id, type_='local time')
		session.add(evDesc)

		ev.descriptions.append(evDesc)
		# fle.events.append(ev)
		evPar.events.append(ev)
		session.add(ev)
		session.commit()

	# print "Create event: %f secs" % (time.time() - t0,)

	return ev


def saveTriggers(session, trigger_info, event):
	session.commit()


def savePicks(session, st, event):
	for tr in st:
		if hasattr(tr.stats, 'pick_info'):
			savePick(session, tr.stats.pick_info, event)

	session.commit()


def savePick(session, data, event):
	if data.phase_hint == 'S':
		return
	if data.onset is None:
		return
	if data.polarity is None:
		return

	check_duplicates = True
	create = True

	# t0 = time.time()
	ok = False
	# if check_duplicates:
	# query = session.query(Pick).join(Station)
	# 	picks = query.filter(Pick.event_id == event.id, Station.code == data.waveform_id.station_code, Pick.phaseHint == data.phase_hint).all()
	# 	create, p = _cleanDuplicates(session, picks)
	# else:
	# 	create = session.query(exists().where(and_(Pick.event_id==event.id, Pick.phaseHint == data.phase_hint, Pick.station.has(Station.code == data.waveform_id.station_code)))).scalar()

	if create:

		result = session.query(Station, Channel).join(Channel).filter(Station.code == data.waveform_id.station_code,
																	  Channel.code == data.waveform_id.channel_code).first()

		# rpdb.set_trace()

		if result is None:
			msg = '[Reading data]: Missing station definition %s'
			logging.error(msg % (data.waveform_id.station_code,))
			return

		station = result[0]
		channel = result[1]

		p = Pick(  # time=data.time.datetime.strftime("%Y/%m/%d %H:%M:%S %f"),
				   time=data.time,
				   filterID=None,
				   methodID=None,
				   phaseHint=data.phase_hint,
				   onset=data.onset,
				   polarity=data.polarity,
				   evaluationMode=data.evaluation_mode,
				   evaluationStatus=data.evaluation_status
		)
		p.SNR = data.SNR
		station.picks.append(p)
		channel.picks.append(p)
		event.picks.append(p)
		session.add(p)
		ok = True

	# print "pick: %f secs" % (time.time() - t0,)
	return ok


# def copyPick(session, pickInfo):

# event = pickInfo[0]['event']
# 	result = session.query(Pick, Station).join(Station, Station.id==Pick.station_id).filter(Pick.event_id==event.id, Station.code==pickInfo[0]['station']).first()
# 	if result is None:
# 		return

# 	(oldPick, station) = result

# 	if not oldPick:
# 		return

# 	p = Pick(
# 		time=oldPick.time,
# 		filterID=oldPick.filterID,
# 		methodID=oldPick.methodID,
# 		phaseHint=oldPick.phaseHint,
# 		onset=oldPick.onset,
# 		polarity=oldPick.polarity,
# 		evaluationMode=oldPick.evaluationMode,
# 		evaluationStatus=oldPick.evaluationStatus,
# 	)
# 	p.SNR = oldPick.SNR

# 	for k,v in pickInfo[1].items():
# 		setattr(p, k, v)

# 	# rpdb.set_trace()
# 	station.picks.append(p)
# 	event.picks.append(p)
# 	session.add(p)

def changePick(session, pickInfo):
	picks = session.query(Pick).join(Station, Station.id == Pick.station_id).filter(Pick.event_id == pickInfo[0]['event'].id,
																					Station.code == pickInfo[0][
																						'station']).all()
	# Tracer()()
	# rpdb.set_trace()
	# pdb.set_trace()

	if not picks:
		return

	for p in picks:
		for k, v in pickInfo[1].items():
			setattr(p, k, v)


def saveEventLoc(session, loc2, event, status='preliminary'):
	# duplicate = session.query(exists().where(Origin.event_id==event.id)).scalar()

	# if duplicate:
	# 	return False

	loc = loc2[0]

	o = Origin(
		time=loc['origin']['time'],
		type_='hypocenter',
		evaluationMode='automatic',
		evaluationStatus=status
	)
	o.X = loc['origin']['X']
	o.Y = loc['origin']['Y']
	o.Z = loc['origin']['Z']

	distances = np.array([t['dist'] for t in loc['tt']])
	oq = OriginQuality(
		associatedPhaseCount=len(loc['tt']),
		associatedStationCount=len(loc['tt']),
		standardError=loc['quality']['RMS'],
		azimuthalGap=loc['quality']['gap'],
		minimumDistance=np.min(distances),
		maximumDistance=np.max(distances),
		medianDistance=np.median(distances)
	)
	ou = OriginUncertainty(
		preferredDescription='confidence ellipsoid'
	)
	ell = ConfidenceEllipsoid(
		semiMajorAxisLength=loc['error']['semi_major_axis_length'],
		semiMinorAxisLength=loc['error']['semi_minor_axis_length'],
		semiIntermediateAxisLength=loc['error']['semi_intermediate_axis_length'],
		majorAxisPlunge=loc['error']['semi_major_axis_dip'],
		majorAxisAzimuth=loc['error']['semi_major_axis_azimuth'],
		majorAxisRotation=0
	)

	pickStations = np.array([p.station.code for p in event.picks])
	for tt in loc['tt']:

		curStation = tt['station']
		# logging.info('%s' % (curStation,))
		# logging.info('%s' % (pickStations,))
		curPickIdx = np.nonzero(pickStations == curStation)[0]
		if len(curPickIdx) == 0:
			continue
		curPickIdx = curPickIdx[0]
		curPick = event.picks[curPickIdx]

		a = Arrival(
			phase=tt['phase'],
			timeCorrection=0,
			distance=tt['dist'],
			timeResidual=tt['res'],
			timeWeight=1
		)

		a.pickID = curPick.id
		o.arrivals.append(a)
		session.add(a)

	o.quality = oq
	o.originUncertainties.append(ou)
	ou.confidenceEllipsoid = ell
	event.origins.append(o)

	session.add(o)
	session.add(oq)
	session.add(ou)
	session.add(ell)
	session.commit()

	event.preferredOriginID = o.id
	session.commit()

	return True


def saveMagnitude(session, mag_data, event):
	# duplicate = session.query(exists().where(Magnitude.event_id==event.id)).scalar()

	# if duplicate:
	# 	return False

	query = session.query(Origin)
	preferredOrigin = query.filter(Origin.id == event.preferredOriginID).first()

	m1 = Magnitude(
		type_='magP',
		mag=mag_data['mag']['P']['magnitude'],
		stationCount=mag_data['mag']['P']['station_count'],
		evaluationMode=mag_data['mag']['P']['evaluation_mode'],
		evaluationStatus=mag_data['mag']['P']['evaluation_status'],
	)
	m2 = Magnitude(
		type_='magS',
		mag=mag_data['mag']['S']['magnitude'],
		stationCount=mag_data['mag']['S']['station_count'],
		evaluationMode=mag_data['mag']['S']['evaluation_mode'],
		evaluationStatus=mag_data['mag']['S']['evaluation_status'],
	)
	m3 = Magnitude(
		type_='energyP',
		mag=mag_data['energy']['P']['energy'],
		stationCount=mag_data['energy']['P']['station_count'],
		evaluationMode=mag_data['energy']['P']['evaluation_mode'],
		evaluationStatus=mag_data['energy']['P']['evaluation_status'],
	)
	m4 = Magnitude(
		type_='energyS',
		mag=mag_data['energy']['S']['energy'],
		stationCount=mag_data['energy']['S']['station_count'],
		evaluationMode=mag_data['energy']['S']['evaluation_mode'],
		evaluationStatus=mag_data['energy']['S']['evaluation_status'],
	)

	m1.event = event
	m1.originID = event.preferredOriginID
	m2.event = event
	m2.originID = event.preferredOriginID
	m3.event = event
	m3.originID = event.preferredOriginID
	m4.event = event
	m4.originID = event.preferredOriginID

	session.add(m1)
	session.add(m2)
	session.add(m3)
	session.add(m4)
	session.commit()

	event.preferredMagnitudeID = m1.id
	session.commit()

	return True


def saveTracePlot(session, st, loc, event_id, folder):
	# Plot traces and picks
	station_loc = np.array([tr.stats.station_location for tr in st])
	event_loc = loc[0]['origin']
	event_loc = np.array([event_loc['X'], event_loc['Y'], event_loc['Z']])
	dist = np.linalg.norm(station_loc - event_loc, axis=1)
	indices = np.argsort(dist)
	dist = dist[indices]
	st2 = trace.filter_stream(st, lf=100, hf=1000, copy=True)
	tr_sorted = [st2.traces[k] for k in indices]

	# Plotting the results
	for k, (tr, d2) in enumerate(zip(tr_sorted, dist)):
		if tr.stats.TP > 0:
			pickSample = tr.stats.TP * tr.stats.sampling_rate

		#debug
		# ttpred1 = d2/5000
		# abc = loc[0]['tt']
		# abc2 = np.array([aa.values() for aa in abc])
		# idx = np.nonzero(abc2[:,1] == tr.stats.station)[0][0]
		# ttpred2 = abc2[idx][0]
		# print ttpred1 - ttpred2
		# Tracer()()

		else:
			pickSample = None
		trace.plot_traces(tr, k, len(st), pickSample=pickSample, extra_caption='%f m, %f dB' % (d2, tr.stats.SNR))

	# Create plot and show/write to file
	trace.save_plot(folder, '%s.png' % (event_id,), 8, 2 * len(st))

