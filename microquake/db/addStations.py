#!/usr/bin/env python

from optparse import OptionParser
import sys
import traceback

import numpy as np
from pyproj import Proj

from microquake.db.schema.db import *
from microquake.db.schema.fdsn_station_db import *


# from IPython.core.debugger import Tracer


def add_network(netkey, session):

    net = session.query(Network).filter_by(code=netkey['code']).first()

    if net is None:
        net = Network(**netkey)
        session.add(net)

    return net


def add_station_equipment(eqkey, session):

    eq = session.query(Equipment).filter_by(Type=eqkey['Type']).first()

    if eq is None:
        eq = Equipment(**eqkey)
        eq.CalibrationDate = None
        session.add(eq)

    return eq


def add_station(stkey, net, eq, session):

    st = session.query(Station).filter_by(X=stkey['X'], Y=stkey['Y'], Z=stkey['Z']).first()
    if st is None:
        st = Station()
        for k, v in stkey.items():
            setattr(st, k, v)

        st.network = net
        st.Equipments.append(eq)
        net.Stations.append(st)
        session.add(st)

    return st


def add_channel(chkey, st, session):

    ch = session.query(Channel).join(Station).filter(Station.code == st.code).first()

    if ch is None:
        ch = Channel()
        for k, v in chkey.items():
            setattr(ch, k, v)

        st.Channels.append(ch)
        session.add(ch)

    return ch


def addStations(meta_filename, DB_URI):

    xmin = 1e10
    ymin = 1e10

    # Read station file
    metadata = np.genfromtxt(meta_filename, dtype=None, skip_header=9)
    for cur_meta in metadata:
        if float(cur_meta[1]) < xmin:
            xmin = float(cur_meta[1])
        if float(cur_meta[2]) < ymin:
            ymin = float(cur_meta[2])

    station_names = np.array([m[0] for m in metadata])
    station_names2, indices = np.unique(station_names, return_index=True)

    trace_groups = []
    for n in station_names2:
        trace_groups.append(np.nonzero(station_names == n)[0])

    sta = []
    meta = []
    st_codes = []
    for tg in trace_groups:
        cur_meta = metadata[tg[0]]

        pos = np.array([cur_meta[1], cur_meta[2], cur_meta[3]])
        if pos[0] <= 1000:
            continue
        station_type = cur_meta[5]
        st_code = '%s_%02d' % (station_type, cur_meta[7])

        sta.append(pos)
        meta.append(cur_meta)
        st_codes.append(st_code)
    sta = np.array(sta)

    if DB_URI is not None:
        DB = DBSession(DB_URI, echo=False)
        DB.createTables()
        session = DB.session

        netkey = {'code': 'NR',
                  'Description': 'Nickel Rim Microseismic Network'}
        net = add_network(netkey, session)

        p = Proj(proj='utm', zone=31, ellps='WGS84')
        x_offset, y_offset = p(0, 45)

        for pos, cur_meta, st_code in zip(sta, meta, st_codes):

            xtmp = float(pos[0]) - xmin + x_offset
            ytmp = float(pos[1]) - ymin - y_offset

            lon, lat = p(xtmp, ytmp, inverse=True)
            
            try:
                
                eqkey = {'Type': station_type}
                eq = add_station_equipment(eqkey, session)

                stkey = {'code': st_code,
                         'X': float(pos[0]),
                         'Y': float(pos[1]),
                         'Z': float(pos[2]),
                         'Latitude': lat,
                         'Longitude': lon,
                         'Elevation': pos[2]}

                st = add_station(stkey, net, eq, session)

                for c in tg:
                    cur_meta = metadata[c]

                    orient = [cur_meta[14], cur_meta[13], cur_meta[15]]
                    channel = str(cur_meta[6])

                    chkey = {'code': channel,
                             'Type': 'CONTINUOUS',
                             'X': float(orient[0]),
                             'Y': float(orient[1]),
                             'Z': float(orient[2])}
                    add_channel(chkey, st, session)

                st.TotalNumberChannels = len(tg)

            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback)

        net.TotalNumberStations = len(sta)
                      
        session.commit()

    return sta, st_codes

if __name__ == '__main__':

    help_text = """
    '%prog  <station input file>'

    <station input file> is a csv file with four colunms containing
                         Station name, Sensor type, x, y and z, respectively.
                         The file should not have any header.
                         
    """

    parser = OptionParser(usage=help_text)

    (opts, args) = parser.parse_args()
    meta_filename = args[0]
    DB_URI = args[1]

    sta, st_codes = addStations(meta_filename, DB_URI)
