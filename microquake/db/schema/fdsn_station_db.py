from sqlalchemy import Table, Column, ForeignKey, Integer, Float, String, Enum
from sqlalchemy.orm import mapper, class_mapper, relationship, backref
from sqlalchemy.orm.exc import UnmappedClassError
# from microquake.db.schema import fdsn_station_xsd
from microquake.db.schema import quakeml_db, stationml_xsd
from microquake.db.schema.db import metadata
import inflect

# from IPython.core.debugger import Tracer

inflect_engine = inflect.engine()


class StationBase(object):
	def add_column(self, m, metadata_table):

		if m.name == '':
			return

		isList = type(m.data_type) is list
		if isList:
			dt = m.data_type[0]
			dt2 = m.data_type[1]
		else:
			dt = m.data_type

		td = hasattr(stationml_xsd, dt)

		data_type = None
		if td:
			typedef = getattr(stationml_xsd, dt)
			if hasattr(typedef, 'values'):
				possible_values = typedef.values
				if len(possible_values) == 0:
					if isList:
						dt = dt2
					else:
						data_type = String(50)
				else:
					data_type = Enum(*possible_values)
			elif issubclass(typedef, stationml_xsd.FloatType):
				data_type = Float(asdecimal=True)
			elif issubclass(typedef, stationml_xsd.SampleRateRatioType):
				pass
			elif issubclass(typedef, stationml_xsd.UnitsType):
				data_type = String(50)
			elif issubclass(typedef, stationml_xsd.PersonType):
				data_type = String(50)

		# if not type(m.data_type) is list and not m.data_type[:-4] in __all__:
		# continue

		if data_type is None:
			if dt == 'xs:double' or dt == 'xs:decimal':
				data_type = Float(asdecimal=True)
			elif dt == 'xs:integer':
				data_type = Integer()
			elif dt == 'xs:string' or dt == 'xs:anyURI':
				data_type = String(50)
			elif dt == 'xs:dateTime':
				data_type = quakeml_db.MyUTCDateTime()

		# print m.name, data_type

		if data_type:
			#column_name = m.name.rstrip('_')
			column_name = m.name
			metadata_table.append_column(Column(column_name, data_type))

	def init_columns(self, metadata_table):

		# print '****', metadata_table

		if self.xsd.superclass is not None:
			for m in self.xsd.superclass.member_data_items_:
				self.add_column(m, metadata_table)

		for m in self.xsd.member_data_items_:
			self.add_column(m, metadata_table)


	def init_fk(self, tables):

		properties_dict = {}
		fks = []
		for m in self.xsd.member_data_items_:

			data_type = m.data_type[:-4]
			if m.name == '' or type(data_type) is list:
				continue
			if not data_type in __all__:
				continue

			one_to_one = False
			if m.container == 0:
				one_to_one = True

			rel_name = inflect_engine.plural(m.name)
			current_class_name = self.__class__.__name__
			current_table_name = current_class_name.lower()
			current_table = tables[current_class_name]
			# print rel_name, m.data_type, current_table_name
			fk_class = globals()[data_type]
			fk_table = tables[data_type]

			if one_to_one:
				#fk_table.append_constraint(relationship(fk_class, uselist=False, order_by=fk_table.c.id))
				#properties_dict[rel_name] = relationship(fk_class, uselist=False, order_by=fk_table.c.id)
				fks.append([current_table, m.name, fk_table.c.id])
			# print current_table, '.', m.name, ' -> ', fk_table
			else:
				properties_dict[rel_name] = relationship(fk_class, backref=backref(current_table_name),
														 order_by=fk_table.c.id)
				fks.append([fk_table, current_table_name + '_id', current_table.c.id])
			# print fk_table, '.', current_table_name + '_id', ' -> ', current_table

		for k in fks:
			k[0].append_column(Column(k[1], Integer, ForeignKey(k[2], onupdate="CASCADE", ondelete="CASCADE")))

		return properties_dict


class StationMLSchema():
	def __init__(self):

		## Define SQL tables
		self.tables = {}
		for table_name in __all__:
			self.tables[table_name] = Table(table_name.lower(), metadata,
											Column('id', Integer, primary_key=True),
			)

		# define base classes and simple columns
		dummies = []
		for class_name in __all__:
			# print class_name
			globals()[class_name] = type(class_name, (getattr(stationml_xsd, class_name + 'Type'), StationBase,), {})

			class_def = globals()[class_name]
			dummy = class_def()
			dummy.xsd = dummy
			dummy.init_columns(self.tables[class_name])
			dummies.append(dummy)

		# initialise sqlalchemy relationships
		# add foreign keys on foreign tables
		fks = []
		for t, dummy in zip(__all__, dummies):
			propertyDict = dummy.init_fk(self.tables)
			fks.append(propertyDict)

		self.mappings = {}
		for t, dummy, fk in zip(__all__, dummies, fks):
			self.mappings[dummy.__class__] = [self.tables[t], fk]

	# map base class to base table definition
	# add sqlalchemy relationships
	# need to delay this to allow other classes to modify the schema
	def map(self):
		for k, v in self.mappings.items():
			try:
				class_mapper(k)
			except UnmappedClassError:
				mapper(k, v[0], properties=v[1])


__all__ = ['Station', 'Network', 'Channel', 'Equipment', 'Response', 'Sensitivity', 'Polynomial', 'ResponseStage',
		   'Operator', 'Site', 'ExternalReference']

DB3 = StationMLSchema()

