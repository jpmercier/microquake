from sqlalchemy import MetaData, create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from IPython.core.debugger import Tracer
from sqlalchemy.sql.sqltypes import *

metadata = MetaData()

from microquake.db.schema.quakeml_db import *
from microquake.db.schema.extra_db import *
from microquake.db.schema.fdsn_station_db import *

class DBSession():
	def __init__(self,
				 uri='mysql://root@localhost:3306/Northparkes',
				 echo=False):
		self.engine = create_engine(uri, pool_recycle=600, echo=echo)
		Session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=self.engine))
		self.session = Session()

		from microquake.db.schema.quakeml_db import DB
		from microquake.db.schema.extra_db import DB2
		from microquake.db.schema.fdsn_station_db import DB3

		from microquake.db.schema.quakeml_db import *
		from microquake.db.schema.extra_db import *
		from microquake.db.schema.fdsn_station_db import *

		# WaveformFile <-> Event relationship
		table1, fk1 = DB.mappings[Event]
		table2, fk2 = DB2.mappings[WaveformFile]

		table1.append_column(
			Column('waveform_file_id', Integer, ForeignKey(table2.c.id, onupdate="CASCADE", ondelete="CASCADE")))
		fk2['events'] = relationship(Event, backref=backref('waveformfile'))

		# X, Y, Z columns on Station
		table3, fk3 = DB3.mappings[Station]

		table3.append_column(Column('X', Float))
		table3.append_column(Column('Y', Float))
		table3.append_column(Column('Z', Float))

		# X, Y, Z columns on Channel
		tableCh, fkCh = DB3.mappings[Channel]

		tableCh.append_column(Column('X', Float))
		tableCh.append_column(Column('Y', Float))
		tableCh.append_column(Column('Z', Float))

		# X, Y, Z columns on Origin
		tableOrig, fkOrig = DB.mappings[Origin]

		tableOrig.append_column(Column('X', Float))
		tableOrig.append_column(Column('Y', Float))
		tableOrig.append_column(Column('Z', Float))

		# SNR column on Pick
		tablePick, fkPick = DB.mappings[Pick]
		tablePick.append_column(Column('SNR', Float))

		# Station <-> Pick relationship
		table4, fk4 = DB.mappings[Pick]

		table4.append_column(
			Column('station_id', Integer, ForeignKey(table3.c.id, onupdate="CASCADE", ondelete="CASCADE")))
		fk3['picks'] = relationship(Pick, backref=backref('station'))

		table4.append_column(
			Column('channel_id', Integer, ForeignKey(tableCh.c.id, onupdate="CASCADE", ondelete="CASCADE")))
		fkCh['picks'] = relationship(Pick, backref=backref('channel'))

		# TODO
		# Origin ->backref(OriginQuality)
		# Ellipse ->backref(OriginUncertainty)
		# Event ->FK(preferredOrigin)
		# Event ->FK(preferredMagnitude)
		# Origin ->backref(list[Magnitude])
		# Event ->backref(list[Magnitudes])


		# # Event -> Origin relationship
		# table5, fk5 = DB.mappings[Origin]
		# col = Column('preferredOriginID', Integer, ForeignKey(table5.c.id, use_alter=True, name='fk_event_origin_id'))
		# table1.append_column(col)
		# fk1['preferredOrigin'] = relationship(Origin, uselist=False, foreign_keys=col)

		# # Event -> Magnitude relationship
		# table6, fk6 = DB.mappings[Magnitude]
		# col = Column('preferredMagnitudeID', Integer, ForeignKey(table6.c.id, use_alter=True, name='fk_event_magnitude_id'))
		# table1.append_column(col)
		# fk1['preferredMagnitude'] = relationship(Magnitude, uselist=False, foreign_keys=col)

		# # Event -> FocalMechanism relationship
		# table7, fk7 = DB.mappings[FocalMechanism]
		# col = Column('preferredFocalMechanismID', Integer, ForeignKey(table7.c.id, use_alter=True, name='fk_event_focal_mechanism_id'))
		# table1.append_column(col)
		# fk1['preferredFocalMechanism'] = relationship(FocalMechanism, uselist=False, foreign_keys=col)

		DB.map()
		DB2.map()
		DB3.map()


	def createTables(self):
		metadata.drop_all(self.engine)
		metadata.create_all(self.engine)

	# # then, load the Alembic configuration and generate the
	# # version table, "stamping" it with the most recent rev:
	# from alembic.config import Config
	# from alembic import command
	# alembic_cfg = Config("alembic.ini")
	# command.stamp(alembic_cfg, "head")


if __name__ == '__main__':
	session = DBSession()
	session.createTables()
