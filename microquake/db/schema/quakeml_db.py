from sqlalchemy import Table, Column, ForeignKey, Integer, Float, String, Enum, types
from sqlalchemy.orm import mapper, class_mapper, relationship
from sqlalchemy.orm.exc import UnmappedClassError
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.ext.compiler import compiles
from obspy.core import UTCDateTime
import inflect

from microquake.db.schema import quakeml_xsd
from microquake.db.schema.db import metadata


# from IPython.core.debugger import Tracer

inflect_engine = inflect.engine()

# requires patched /MySQLdb/times.py
class FracDateTime(DATETIME):
	def __init__(self, fractional_seconds=6):
		super(FracDateTime, self).__init__()
		self.fractional_seconds = fractional_seconds


@compiles(FracDateTime, "mysql")
def _frac_date_time(element, compiler, **kw):
	if element.fractional_seconds:
		return "DATETIME(%d)" % element.fractional_seconds
	else:
		return "DATETIME"


# noinspection PyAbstractClass
class MyUTCDateTime(types.TypeDecorator):
	impl = FracDateTime
	# impl = types.DateTime
	def process_bind_param(self, value, dialect):
		if hasattr(value, 'datetime'):
			return value.datetime
		else:
			return None

	def process_result_value(self, value, dialect):
		if not value:
			return None
		else:
			return UTCDateTime(value)


class QuakeBase(object):
	def add_column(self, m, metadata_table):

		if m.name == '' or m.name == 'id':
			return

		if m.container == 1:
			return

		isList = type(m.data_type) is list
		if isList:
			dt = m.data_type[0]
			dt2 = m.data_type[1]
		else:
			dt = m.data_type

		td = hasattr(quakeml_xsd, dt)

		data_type = None
		if td:
			typedef = getattr(quakeml_xsd, dt)
			if hasattr(typedef, 'values'):
				possible_values = typedef.values
				if len(possible_values) == 0:
					if isList:
						dt = dt2
					else:
						data_type = String(256)
				else:
					data_type = Enum(*possible_values)
			elif len(typedef.member_data_items_) == 1:
				dt = typedef.member_data_items_[0].data_type
			elif issubclass(typedef, quakeml_xsd.ResourceReference):
				data_type = Integer()

		if data_type is None:
			if dt == 'RealQuantity':
				data_type = Float()
			if dt == 'xs:double':
				data_type = Float(asdecimal=True)
			elif dt == 'xs:integer' or dt == 'IntegerQuantity' or 'Resource' in dt:
				data_type = Integer()
			elif dt == 'xs:string':
				data_type = String(256)
			elif dt == 'TimeQuantity':
				data_type = MyUTCDateTime()

		# print m.name, data_type

		if data_type:
			# column_name = m.name.rstrip('_')
			column_name = m.name
			metadata_table.append_column(Column(column_name, data_type))

	def init_columns(self, metadata_table):

		# print '****', metadata_table

		for m in self.xsd.member_data_items_:
			self.add_column(m, metadata_table)

	def init_fk(self, tables):

		properties_dict = {}
		fks = []
		for m in self.xsd.member_data_items_:

			if m.name == '':
				continue

			one_to_one = False
			if m.container == 0:

				# Warning: ResourceReference is not a class, so we can't figure out a foreign key constraint
				# This means that all ResourceReference have to be added manually...
				# Unless there is a way to do it automatically from the reference name
				# For now, this is added as an Integer column, with no foreign key
				if not m.data_type in quakeml_xsd.__all__:
					continue
				if 'Quantity' in m.data_type:
					continue
				one_to_one = True

			rel_name = inflect_engine.plural(m.name)
			current_class_name = self.__class__.__name__
			current_table_name = current_class_name.lower()
			current_table = tables[current_class_name]
			fk_class = globals()[m.data_type]
			fk_table = tables[m.data_type]

			# print rel_name, m.data_type, current_table_name

			if len(fk_class.member_data_items_) == 1:
				continue

			if one_to_one:
				# fk_table.append_constraint(relationship(fk_class, uselist=False, order_by=fk_table.c.id))
				# properties_dict[m.name] = relationship(fk_class, uselist=False, foreign_keys=current_class_name + '.' + m.name + '_id')
				# properties_dict[m.name] = relationship(fk_class, uselist=False, primaryjoin='(%s.%s == %s.%s)' % (current_table, m.name + '_id', fk_table, 'id'))
				fks.append([one_to_one, current_table, m.name, fk_table.c.id, m.name, fk_class])
			# print current_table, '.', m.name + '_id', ' -> ', fk_table
			else:
				# properties_dict[rel_name] = relationship(fk_class, backref=current_table_name, order_by=fk_table.c.id)
				fks.append([one_to_one, fk_table, current_table_name, current_table.c.id, rel_name, fk_class])
			# print fk_table, '.', current_table_name + '_id', ' -> ', current_table

		for k in fks:
			col = Column(k[2] + '_id', Integer, ForeignKey(k[3], onupdate="CASCADE", ondelete="CASCADE"))
			k[1].append_column(col)
			if k[0]:
				properties_dict[k[2]] = relationship(k[5], uselist=False, foreign_keys=col)
			else:
				properties_dict[k[4]] = relationship(k[5], backref=k[2], order_by=k[1].c.id)

		return properties_dict


class QuakeMLSchema():
	def __init__(self):

		## Define SQL tables
		self.tables = {}
		for table_name in quakeml_xsd.__all__:
			self.tables[table_name] = Table(table_name.lower(), metadata,
											Column('id', Integer, primary_key=True),
			)

		# define base classes and simple columns
		dummies = []
		for class_name in quakeml_xsd.__all__:
			# def __init__(self, **kwargs):
			# self.xsd = getattr(quakeml_xsd, self.__class__.__name__)(**kwargs)
			# 	QuakeBase.__init__(self)

			# globals()[class_name] = type(class_name, (QuakeBase,), \
			# 	dict(__init__ = __init__) \
			# )

			globals()[class_name] = type(class_name, (getattr(quakeml_xsd, class_name), QuakeBase,), {})

			class_def = globals()[class_name]
			dummy = class_def()
			dummy.xsd = dummy
			dummy.init_columns(self.tables[class_name])
			dummies.append(dummy)

		# initialise sqlalchemy relationships
		# add foreign keys on foreign tables
		fks = []
		for t, dummy in zip(quakeml_xsd.__all__, dummies):
			propertyDict = dummy.init_fk(self.tables)
			fks.append(propertyDict)

		self.mappings = {}
		for t, dummy, fk in zip(quakeml_xsd.__all__, dummies, fks):
			self.mappings[dummy.__class__] = [self.tables[t], fk]

	# map base class to base table definition
	# add sqlalchemy relationships
	# need to delay this to allow other classes to modify the schema
	def map(self):
		for k, v in self.mappings.items():
			try:
				class_mapper(k)
			except UnmappedClassError:
				mapper(k, v[0], properties=v[1])


DB = QuakeMLSchema()

__all__ = [
    "amplitude",
    "arrival",
    "axis",
    "comment",
    "composite_time",
    "confidence_ellipsoid",
    "creation_info",
    "data_used",
    "event",
    "event_description",
    "event_parameters",
    "focal_mechanism",
    "integer_quantity",
    "magnitude",
    "moment_tensor",
    "nodal_plane",
    "nodal_planes",
    "origin",
    "origin_quality",
    "origin_uncertainty",
    "phase",
    "pick",
    "principal_axes",
    "real_quantity",
    "source_time_function",
    "station_magnitude",
    "station_magnitude_contribution",
    "tensor",
    "time_quantity",
    "time_window",
    "Waveform_stream_id"
]
