from sqlalchemy import Table, Column, ForeignKey, Integer, String, Boolean, Enum
from sqlalchemy.orm import mapper, class_mapper, relationship, backref
from sqlalchemy.orm.exc import UnmappedClassError
# from sqlalchemy.schema import UniqueConstraint
# from sqlalchemy.ext.declarative import declarative_base
from microquake.db.schema.db import metadata

# Base = declarative_base()


class WaveformFile(object):
	"""
    DB table containing path to waveform files.
    """

	def __repr__(self):
		return "<WaveformFile('%s')>" % self.id


class WaveformPath(object):
	"""
    DB table containing file directories.
    """

	def __repr__(self):
		return "<WaveformPath('%s')>" % self.id

class VelocityModel(object):
	"""
	DB table containing velocity model description
	"""

	def __repr__(self):
		return "<VelocityModel('%s')>" % self.id

class VelocityPath(object):
	"""
	DB table containing velocity model description
	"""

	def __repr__(self):
		return "<VelocityPath('%s')>" % self.id


class WaveformSchema():
	def __init__(self):

		wave_file_table = Table("waveformfile", metadata,
								Column('id', Integer, primary_key=True),
								Column('file', String(512), nullable=False, index=True, unique=True),
								Column('size', Integer, nullable=False),
								Column('mtime', Integer, nullable=False, index=True),
								Column('format', String(8), nullable=False, index=True),
								Column('waveformpath_id',
									   ForeignKey('waveformpath.id', onupdate="CASCADE", ondelete="CASCADE")),
		)

		wave_path_table = Table("waveformpath", metadata,
								Column('id', Integer, primary_key=True),
								Column('path', String(512), nullable=False, index=True, unique=True),
								Column('archived', Boolean, default=False),
		)

		velocity_model_table = Table("velocitymodel", metadata,
								  	 Column('id', Integer, primary_key=True),
								  	 Column('file', String(512), nullable=True, default=""),
								  	 Column('format', Enum('GridData, EKImageData, NLL'), nullable=False, default="GridData"),
								  	 Column('velocitypath_id', ForeignKey('velocitypath.id', onupdate="CASCADE", ondelete="CASCADE")),
								  	 Column('type', Enum('homogeneous', '3D'), nullable=True, default="homogeneous")
		)

		velocity_path_table = Table("velocitypath", metadata,
									Column('id', Integer, primary_key=True),
									Column('path', String(512), nullable=False, index=True, unique=True),
									Column('archived', Boolean, default=False),
		)

		self.mappings = {}
		self.mappings[WaveformFile] = [wave_file_table, {}]
		self.mappings[VelocityModel] = [velocity_model_table, {}]
		self.mappings[VelocityPath] = [velocity_path_table, {
			'files': relationship(VelocityModel, order_by=velocity_model_table.c.id,
								  backref=backref("velocitypath")),
		}]
		self.mappings[WaveformPath] = [wave_path_table, {
			'files': relationship(WaveformFile, order_by=wave_file_table.c.id,
								  backref=backref("waveformpath")),
		}]

	# map base class to base table definition
	# add sqlalchemy relationships
	# need to delay this to allow other classes to modify the schema
	def map(self):
		for k, v in self.mappings.items():
			try:
				class_mapper(k)
			except UnmappedClassError:
				mapper(k, v[0], properties=v[1])


DB2 = WaveformSchema()
