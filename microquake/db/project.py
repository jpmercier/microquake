# import numpy as np
import os
from bson.objectid import ObjectId
from microquake.db import mongo
from microquake.core import read, read_events, logger
from microquake.core.json import Default
from IPython.core.debugger import Tracer


class Project(object):
    """
    Object to handle interaction with a MongoDB database
    """

    def __init__(self, params):

        self.db = None
        self.events_col = None
        self.waveforms_col = None

        self.events_col_name = 'event'
        self.waveforms_col_name = 'waveform'

        try:
            self.db_uri = params.db_uri
            self.db_name = params.db_name
            self.format_continuous = params.format_continuous
            self.waveform_path = params.waveform_path
            self.event_path = params.event_path
            self.template_dir = params.template_dir
            self.template_file = params.template_file
            self.supported_ext = ['.sgy', '.mseed']
        except Exception as e:
            logger.error("Couldn't initialize project from definitions XML file.")

    def connect(self):

        self.db = mongo.connect(self.db_uri, self.db_name)
        self.events_col = self.db[self.events_col_name]
        self.waveforms_col = self.db[self.waveforms_col_name]

    def insert_event(self, event, data_path, base_file_name, waveform_id,
                     catalog_index=0):
        """
        Write a microquake.core.event catalog into the project. Microquake
        only one event per catalog which are then stored in individual QuakeML
        files.
        :param event: event
        :type event: microquake.core.event.Event
        :param data_path: path to raw data (seismogram and QuakeML catalog)
        :type data_path: str
        :param base_file_name: base file name for the seismogram and the QuakeML
        file. Basically the file name without the extension. (extension .xml is
        removed if provided)
        :type base_file_name: str
        :param waveform_id: id of the waveform in the db
        :type waveform_id: int
        :rtype bool
        """

        default = Default()
        events_to_insert = []

        # Skip if event already in DB
        cursor = self.events_col.find_one({'base_file_name': base_file_name,
            'data_path': data_path})
        if cursor:
            return None

        # Flatten event data
        ev_dict = mongo.flatten_event(event)
        ev_dict['data_path'] = data_path
        ev_dict['base_file_name'] = base_file_name
        ev_dict['waveform_id'] = waveform_id
        ev_dict['catalog_index'] = catalog_index  # the index of the event in
        #  the catalog. Should in principle always be 0. Included for future
        # proofing purposes

        inserted_id = mongo.insert(self.events_col, ev_dict, default=default)
        return inserted_id

    def find_event_by_name(self, base_file_name, data_path):

        cursor = self.events_col.find_one({'base_file_name': base_file_name,
            'data_path': data_path})

        if not cursor:
            return None

        return cursor
        # event = self.read_event(cursor)
        # return event

    def find_event_by_id(self, event_id):

        cursor = self.events_col.find_one({'_id': ObjectId(event_id)})

        if not cursor:
            return None

        return cursor
        # event = self.read_event(cursor)
        # return event

    def find_events_by_waveform_id(self, waveform_id):

        res = self.events_col.find({'waveform_id': ObjectId(waveform_id)})

        if not res:
            return []

        return res

    def read_events(self, res, event_indices=None):
        """
        read a series of event from the database
        :param res: a list of cursor to db
        :param event_indices: list of indices. Nothe that len(event_indices) ==
        len(res). Default value, None
        :return: a list of events
        """

        events = []
        for k, cursor in enumerate(res):
            try:
                event_index = event_indices[k]
            except:
                event_index = 0

            evt = self.read_one_event(cursor, event_index=event_index)
            # event = cat.events[0]
            events.append(evt)
        return events


    def read_one_event(self, cursor, event_index=0):
        """
        read one event from a QuakeML file
        :param cursor: a database cursor
        :param catalog_index: index of the event in the catalog
        :return: event object
        """

        dir = cursor['data_path']
        flename = cursor['base_file_name']
        fle = os.path.join(self.event_path, dir, flename)

        from microquake.core.event import readEvents

        try:
            cat = readEvents(fle, format='QUAKEML')
        except Exception as e:
            return None

        return cat[event_index]


    def update_event(self, event, cursors, data_path, base_file_name,
                     catalog_index=0):
        """
        update event both in quakeML and database
        :param event: an event object
        :type event: ~microquake.core.event.Event
        :param cursors: a series of cursors to db elements
        :param data_path: path to the XML file
        :param base_file_name: file name without the extension
        :param catalog_index: event index in event catalog contained in the
        XML file
        :return: bool
        """

        fle = os.path.join(self.event_path, data_path, base_file_name)

        events = self.read_events(cursors)
        # TODO: merge catalogs

        from microquake.core import readEvents

        try:
            # for future proofing the reader, in case more than one event are
            #  present in each catalog.
            cat = readEvents(fle, format='QUAKEML')
            cat[catalog_index] = event
            cat.write(fle, format="QUAKEML")

        except Exception as e:
            logger.error(e)
            return False

        return True


    def insert_waveform(self, data_path, base_file_name):
        """
        Insert waveform information into the database
        :param data_path: path to raw data (seismogram and QuakeML catalog
        :type data_path: str
        :param base_file_name: base file name for the seismogram and the QuakeML
        file. Basically the file name without the extension.
        :type base_file_name: str
        :rtype bool
        """

        # Skip if waveform already in DB
        cursor = self.waveforms_col.find_one({'base_file_name': base_file_name,
            'data_path': data_path})
        if cursor:
            return [cursor["_id"]]

        # Flatten event data
        waveform = {}
        waveform['data_path'] = data_path
        waveform['base_file_name'] = base_file_name

        inserted_ids = mongo.insert(self.waveforms_col, waveform)
        return inserted_ids


    def find_waveform(self, base_file_name, data_path):
        """
        find a waveform in the database given a base_file_name and a data_path
        :param base_file_name: filename without the extension
        :param data_path: path to the data
        :return: cursor to a database entry
        """

        cursor = self.waveforms_col.find_one({'base_file_name': base_file_name,
            'data_path': data_path})

        return cursor


    def list_waveforms(self, start=0, limit=100):

        # read project filters
        # read project sorting
        cursor = self.waveforms_col.find(skip=start, limit=limit)

        return cursor

    def traverse_folder(self, path, fn):
        """
        traverse the data and waveform paths to collect information to fill
        the database
        :param path: path to walk
        :param fn: function that will be applied to the data (*args and
        **kwargs are passed to this function)
        :return:
        """
        source = path.rstrip(os.sep)
        for root, dirs, files in os.walk(source, fn):

            files = [f for f in files if os.path.splitext(f)[1] in self.supported_ext]

            for f in files:
                fn(f, source, root, self)

