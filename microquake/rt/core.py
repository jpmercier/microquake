# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: core.py
#  Purpose: Functions related to RT environment
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
functions specific to RT-OT environment

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""

from IPython.core.debugger import Tracer

__insert_query = """ INSERT INTO SEISMIC(project_id, microquake_id, event_date, event_time, magnitude_moment, 
                     magnitude_error, num_triggers, x, y, z, location_error, seismic_moment,
                     radiated_energy, radiated_energy_s, radiated_energy_p, corner_frequency, static_stress_drop,
                     esep, accepted, auto_processed, manually_processed, blast, event, event_modification_time)
                     VALUES (%s, %s, %s, %s, %s,
                     %s, %s, %s, %s, %s, %s, %s,
                     %s, %s, %s, %s, %s,
                     %s, %s, %s, %s, %s, %s, %s)"""

__update_query = """ UPDATE SEISMIC SET project_id=%s, event_date=%s, event_time=%s, magnitude_moment=%s, 
                     magnitude_error=%s, num_triggers=%s, x=%s, y=%s, z=%s, location_error=%s, seismic_moment=%s,
                     radiated_energy=%s, radiated_energy_s=%s, radiated_energy_p=%s, corner_frequency=%s, 
                     static_stress_drop=%s, esep=%s, accepted=%s, auto_processed=%s, manually_processed=%s, 
                     blast=%s, event=%s, event_modification_time=%s 
                     WHERE microquake_id = %s"""

def export_cavecad(event, host, database, user, password, time_zone):
    """
    function to export a microquake.core.event.Catalog object to cavecad
    :param cat: catalogue
    :type cat: microquake.core.event.Catalog
    :param host: host name
    :param database: database name
    :param user: user name
    :param password: password
    :param time_zone: Time zone from pytz or UTC offset
    :return: True if successful, False otherwise
    """

    import pymssql
    from pytz import utc

    conn = pymssql.connect(
        host=host,
        database=database,
        user=user,
        password=password
    )

    cursor = conn.cursor()

    project_id = 1
    microquake_id = event.EVENT_NAME.replace('/', '_')
    dt = event.preferred_origin().time.datetime.replace(tzinfo=utc).astimezone(time_zone)
    event_date = dt.strftime("%Y-%m-%d %H:%M:%S.%f")
    event_time = dt.strftime("%Y-%m-%d %H:%M:%S.%f")
    magnitude_moment = event.preferred_magnitude().mag
    magnitude_error = event.preferred_magnitude().mag_errors.uncertainty
    num_triggers = event.NUM_ACCEPTED_TRIGGERS
    x = event.preferred_origin().x
    y = event.preferred_origin().y
    z = event.preferred_origin().z
    location_error = 0
    seismic_moment = 10 ** ((magnitude_moment + 6.07) * 3 / 2)
    radiated_energy = 0 if event.ENERGY == 'None' else float(event.ENERGY)
    radiated_energy_p = 0 if event.ENERGY_P == 'None' else float(event.ENERGY_P)
    radiated_energy_s = 0 if event.ENERGY_S == 'None' else float(event.ENERGY_S)
    corner_frequency = event.preferred_magnitude().corner_frequency
    static_stress_drop = 0 if event.STATIC_STRESS_DROP == 'None' else 0 if event.STATIC_STRESS_DROP == \
                                                                           "'\\xef\\xbf\\xbd'"                                                                                \
                         else float(event.STATIC_STRESS_DROP)
    esep = radiated_energy_s / radiated_energy_p if radiated_energy_p != 0 else 0
    accepted = bool(event.ACCEPTED)
    auto_processed = bool(event.AUTO_PROCESSED)
    manually_processed = not bool(event.AUTO_PROCESSED)
    blast = bool(event.BLAST)
    event_ = not bool(event.BLAST)
    event_modification_time = event.EVENT_MODIFICATION_TIME

    insert_tpl = (project_id, microquake_id, event_date, event_time, magnitude_moment,
                  magnitude_error, num_triggers, x, y, z, location_error, seismic_moment,
                  radiated_energy, radiated_energy_s, radiated_energy_p, corner_frequency, static_stress_drop,
                  esep, accepted, auto_processed, manually_processed, blast, event_, event_modification_time)

    update_tpl = (project_id, event_date, event_time, magnitude_moment,
                  magnitude_error, num_triggers, x, y, z, location_error, seismic_moment,
                  radiated_energy, radiated_energy_s, radiated_energy_p, corner_frequency, static_stress_drop,
                  esep, accepted, auto_processed, manually_processed, blast, event_, event_modification_time,
                  microquake_id)

    # __insert_query = """ INSERT INTO SEISMIC(project_id, microquake_id, event_date, event_time, magnitude_moment,
    #                      magnitude_error, num_triggers, x, y, z, location_error, seismic_moment,
    #                      radiated_energy, radiated_energy_s, radiated_energy_p, corner_frequency, static_stress_drop,
    #                      esep, accepted, auto_processed, manually_processed, blast, event, event_modification_time)
    #                      VALUES (%s, %s, %s, %s, %s,
    #                      %s, %s, %s, %s, %s, %s, %s,
    #                      %s, %s, %s, %s, %s,
    #                      %s, %s, %s, %s, %s, %s, %s)"""

    # check if the event is in the db
    cursor.execute("SELECT seismic_id FROM SEISMIC WHERE microquake_id=%s", microquake_id)
    if cursor.fetchone():
        try:
            cursor.execute("SELECT event_modification_time FROM seismic WHERE microquake_id=%s", microquake_id)

            emt = cursor.fetchone()[0]

            if emt == event.EVENT_MODIFICATION_TIME:
                conn.commit()
                conn.close()
                return


        except Exception as e:
            if hasattr(e, 'message'):
                print(e.message)
            else:
                print(e)
                conn.commit()
                conn.close()
                return

        else:
            #update the event
            try:
                cursor.execute(__update_query, update_tpl)
            except Exception as e:
                if hasattr(e, 'message'):
                    print(e.message)
                else:
                    print(e)
                    conn.commit()
                    conn.close()
                    return

    else:
        cursor.execute(__insert_query, insert_tpl)

    conn.commit()
    conn.close()

