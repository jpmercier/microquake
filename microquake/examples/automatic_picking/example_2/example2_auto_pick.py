
# coding: utf-8

# # Automatic picking and event location

# In[1]:

from microquake.core import Stream, event, station, ctl, read
from microquake.core.stream import composite_traces, is_valid
from microquake.waveform import pick
from microquake.imaging.plot import Plot
from microquake.waveform import mag
import microquake.nlloc as nll
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm


# reading control file

# In[2]:

params = ctl.parse_control_file('input.xml')


# Creating files and directory structure required by NLLOC

# In[3]:

nll_opts = nll.init_from_xml_params(params, base_folder='NLL')
# nll_opts.prepare(create_time_grids=True, tar_files=False)


# Reading seismogram

# In[4]:

st = read('data_example.mseed')
# composite_traces(st.copy()).plot(equal_scale=False)


# Filtering bad traces

# In[5]:

st_valid = is_valid(st, return_stream=True)
# composite_traces(st_valid.copy()).plot(equal_scale=False)


# In[6]:

site = params.sensors.site
st2 = st_valid.copy()
cat_tmp = pick.automatic_picking(st2, site, params, nll_suffix='0_0')
cat_out = mag.measure_magnitude(st2, cat_tmp, site, params.velgrids.vp, params.velgrids.vs)


# In[7]:

composite_traces(st_valid.copy()).plot(equal_scale=False, catalog=cat_out, site=site)


# In[8]:

print cat_out


# In[9]:

cat_out.write('data_example.xml', format='quakeml')


# In[10]:

cat_out.events[0].origins[0].x


# In[ ]:


