
# coding: utf-8

## Automatic picking and event location

# In[1]:

from microquake.core import Stream, event, station, ctl, read
from microquake.core import GridData
from microquake.core.stream import composite_traces
from microquake.waveform import pick
from microquake.waveform import mag
import microquake.nlloc as nll
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from microquake.spark import init_spark_from_params

# reading control file and initializing a spark context
params = ctl.parse_control_file('input.xml')
sc = None
# sc = init_spark_from_params(params)

# Takes long time to run
# only run if it is the first time
nll_opts = nll.init_from_xml_params(params, base_folder='NLL')
nll_opts.prepare(create_time_grids=True, tar_files=False, SparkContext=sc)

vp = params.velgrids.grids.vp
vs = params.velgrids.grids.vs
# plt.imshow(vp.data[:,70,:].T, cmap=cm.RdBu_r)
# plt.xlim([0,150])
# plt.ylim([0,140])
# plt.clim([3500, 4300])
# cb = plt.colorbar()
# plt.title('velocity model')

st = read('data_example.mseed')
# composite_traces(st.copy()).plot(equal_scale=False)

site = params.sensors.site
st2 = st.copy()
cat_tmp = pick.automatic_picking(st2, site, params, nll_suffix='0_0')
cat_3dvel = mag.measure_magnitude(st2, cat_tmp, site, vp, vs)

print cat_3dvel

ct = composite_traces(st.copy())
ct.plot(equal_scale=False, catalog=cat_3dvel, site=site)

cat_3dvel.write('data_example.xml', format='quakeml')
