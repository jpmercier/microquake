from uquakepy.core import read, UTCDateTime, ctl, Stream, Trace
import numpy as np
from pyspark import SparkContext
from IPython.core.debugger import Tracer
import pickle as pickle
import matplotlib.pyplot as plt
from scipy.signal import hilbert


# def gen_flist(starttime, endtime, params):
#     stimes = np.arange(starttime, endtime, 
#                        params.ancc.cross_correlation_length)
#     sfiles = []
#     Tracer()()
#     for stime in stimes:
#         sfiles.append(stime.strftime(params.ancc.data_location +
#                                      params.ancc.base_name))
# 
#     gfiles = []
#     for index in range(0, len(sfiles) - 1):
#         
#         # grouping the files in based cross-correlation stack
#         if index == 0:
#             gfiles2 = []
#         elif index % params.ancc.base_cross_correlation_stack == 0:
#             gfiles.append(gfiles2)
#             gfiles2 = []
# 
#         tstart = stimes[index]
#         tstop = stimes[index + 1]
#         times = np.arange(tstart, tstop, params.ancc.record_length)
#         files = []
#         for time in times:
#             files.append(time.strftime(params.ancc.data_location +
#                                     params.ancc.base_name))
#         gfiles2.append(files)
# 
#     if gfiles2:
#         gfiles.append(gfiles2)
#     return gfiles

def gen_flist(starttime, endtime, params):
    ftimes = np.arange(starttime, endtime, params.ancc.record_length)
        
    nfiles_g1 = params.ancc.cross_correlation_length / params.ancc.record_length
    nfiles_g2 = params.ancc.base_cross_correlation_stack

    nfiles_g3 = int(np.ceil(len(ftimes) * params.ancc.record_length / float(nfiles_g1 * nfiles_g2)))

    ct = 0
    f_g3 = []
    for i in range(0, nfiles_g3):
        f_g2 = []
        for j in range(0, nfiles_g2):
            f_g1 = []
            for k in range(0, nfiles_g1):
                if ct >= len(ftimes):
                    continue

                fname = ftimes[ct].strftime(params.ancc.data_location +
                                            params.ancc.base_name)
                f_g1.append(fname)
                ct += 1

            f_g2.append(f_g1)
        f_g3.append(f_g2)

    return f_g3


def load_segy(filename, site):
    # The line contains previous, current and next file for overlap purposes
    st = read(filename, format='SEGY',  unpack_trace_headers=True)
    traces = []
    for k, tr in enumerate(st):
        x = tr.stats.segy.trace_header.group_coordinate_x
        y = tr.stats.segy.trace_header.group_coordinate_y

        for station in site.stations():
            if (station.x == x) and (station.y == y):
                tmp = site.select(station=station.code)
                tr.stats.network = tmp.networks[0].code
                tr.stats.station = station.code
                traces.append(tr)

    st2 = Stream(traces=traces)

    traces = []
    for station in site.stations():
        sttmp = st2.select(station=station.code).copy()
        if len(sttmp) == 3:
            for k, tr in enumerate(sttmp):
                if k == 0:
                    tr.stats.channel = 'X'
                elif k == 1:
                    tr.stats.channel = 'Y'
                else:
                    tr.stats.channel = 'Z'
                traces.append(tr)
        else:
            if sttmp:
                sttmp.traces[0].stats.channel = 'Z'
                traces.append(sttmp.traces[0])
    return Stream(traces=traces).copy()


def preprocess(fnames):
    traces = []
    for fname in fnames:
        try:
            st = load_segy(fname, site)
        except:
            return []
        
        st = st.detrend('linear')
        st = st.detrend('demean')
        # st.filter('lowpass', freq=sampling_rate / 2.5)
        st.resample(sampling_rate)
       
        # differentiate if velocity meter
        for k, tr in enumerate(st):
            station = site.select(station=tr.stats.station).stations()[0]
            if station.motion_type == 'velocity':
                tr.differentiate()
                tr.stats.motion_type = 'acceleration'
            
            if np.max(np.abs(tr.data)) > 10 * np.std(tr.data):
                tr.data = np.zeros(len(tr.data))

            tr = tr.taper(type='cosine', max_percentage=0.01)
            traces.append(tr)
    
    st2 = Stream(traces=traces).merge(fill_value=0)
    # st2 = st2.resample(sampling_rate, no_filter=False)
    for k, tr in enumerate(st2):
        st2.traces[k] = st2.traces[k].taper(type='cosine', max_percentage=0.001)
        st2.traces[k].data /= np.std(st2.traces[k].data)
            

    stations = st2.unique_stations()
    cc_out = []
    for k, sta1 in enumerate(stations[:-1]):
        for sta2 in stations[k + 1:]:
            key = '%s_%s' % (sta1, sta2)
            st_1 = st2.select(station=sta1).copy()
            st_2 = st2.select(station=sta2).copy()

            for tr1 in st_1:
                for tr2 in st_2:
                    cc = cross_correlate(tr1, tr2, duration=1)
                    cc_out.append((key, cc))
    return cc_out


def whiten(tr):
    # for i, tr in enumerate(st):
    tmp = np.hstack((tr.data, tr.data[-1::-1])) 
    TR = np.fft.fft(tmp)
    phase = np.angle(TR)
    TR_w = np.exp(1j * phase)
    tr_out = Trace()
    tr_out.data = np.real(np.fft.ifft(TR_w))[0: len(tr)]
    tr_out.stats = tr.stats

    return tr_out


def cross_correlate(tr1, tr2, duration=1):
    new_tr1 = tr1.data 
    new_tr2 = tr2.data
    new_tr1[np.isnan(new_tr1)] = 0
    new_tr2[np.isnan(new_tr2)] = 0

    length = int(2 * 2 ** np.ceil(np.log2(len(new_tr1))))

    TR1 = np.fft.fft(new_tr1, length)
    TR2 = np.fft.fft(new_tr2, length)

    CC = np.exp(-1j * np.angle(TR1)) * np.exp(1j * np.angle(TR2))
    cc = np.real(np.fft.ifft(CC)) 
    nsamples = duration * tr1.stats.sampling_rate
    cc = np.fft.fftshift(cc)


    cc = cc[len(cc) / 2 - nsamples / 2: len(cc) / 2 + nsamples / 2]
    return cc
   

def stack_reducer(stack, cc):
    
    if np.max(np.abs(cc)) > 5 * np.std(cc):
        cc[:] = 0
    return stack + cc

def stack_reducer2(stack, cc):
    trtmp = Trace(data=cc)
    trtmp.stats.sampling_rate = sampling_rate
    trtmp.filter('lowpass', freq=200)

    return stack + trtmp.data ** 2


def stack(inp):
    key = inp[0]
    ccs = list(inp[1])
    sta1 = key.split('_')[0]
    sta2 = key.split('_')[1]
    sta1_loc = site.select(station=sta1).stations()[0].loc
    sta2_loc = site.select(station=sta2).stations()[0].loc
    dist = np.linalg.norm(sta1_loc - sta2_loc)
    nbp = dist / vp * sampling_rate
    nbs = dist / vs * sampling_rate

    stack = np.zeros(len(ccs[0]))
    for cc in ccs:
        if np.max(np.abs(cc)) > 5 * np.std(cc):
            continue
        trtmp = Trace(data=cc)
        trtmp.stats.sampling_rate = sampling_rate
        trtmp.filter('lowpass', freq=200)
        s1 = cc[len(cc) / 2 - nbs : len(cc) / 2 - nbp]
        s2 = cc[len(cc) / 2 + nbp : len(cc) / 2 + nbs]
        signal = np.std(np.hstack((s1, s2)))
        noise = np.std(cc)
        SNR = 20 * np.log10(signal / noise)
        if SNR > 1.5:
            stack += cc ** 2

    return (key + '_%d' % dist, stack)

def phase_weight(tr):

    hilb = hilbert(tr.data)
    phase = np.arctan2(hilb.imag, hilb.real)
    weight = np.exp(1j * phase)

    return np.real(weight)



starttime = UTCDateTime(2013,06,16,10,0,0)
endtime = UTCDateTime(2013,06,16,11,0,0)

times = np.arange(starttime, endtime, 5)

idir = '/Volumes/NRS_data_2/Data/'

params = ctl.parse_control_file('input.xml')

sflist = gen_flist(starttime, endtime, params)
global site, sampling_rate, vp, vs
# site = sc.broadcast(params.sensors[0].site)
# vp = 

vp = params.velgrids[0].vp
vs = params.velgrids[0].vs
sampling_rate = params.ancc.resampling_rate
site = params.sensors[0].site

# for sfiles in sflist:
#     for fgroup in sfiles:
#         preprocess(fgroup)


SC = SparkContext(conf=params.spark.conf)

out = []
out1 = []
for sfiles in sflist:
    rdd = SC.parallelize(sfiles, len(sfiles))
    # out = rdd.flatMap(extract_cc).groupByKey().map(stack).collect()
    # out = rdd.flatMap(preprocess).groupByKey().map(stack).collect()
    out.append(rdd.flatMap(preprocess).groupByKey().map(stack).collect())
    # rdd_cc = rdd.flatMap(preprocess)
    # out.append(rdd_cc.reduceByKey(lambda a, b: a + b).collect())
    # out.append(rdd_cc.reduceByKey(stack_reducer).collect())

