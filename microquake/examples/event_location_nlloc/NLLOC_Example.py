# loading libraries
from uquakepy.core import ctl
import uquakepy.nlloc.core as nll
from uquakepy.core.event import readEvents

params = ctl.parseControlFile('input.xml')
# keys = ['velgrids', 'sensors']
# for job_index, job in enumerate(ctl.buildJob(keys, params)):
#    params = ctl.getCurrentJobParams(params, keys, job)
#    nll_opts = nll.init_from_xml_params(params, base_folder='NLL')
#    nll_opts.prepare(create_time_grids=True, tar_files=False)

cat = readEvents('example.xml')
# print cat
# creating an NLL object
# note the suffix means velocity grid 0 and sensors set 0
nll_obj = nll.NLL(params.project_code, suffix='0_0', base_folder='NLL')
# calculating event location using NLLOC
cat_new = nll_obj.run_event(cat.events[0], status='preliminary')