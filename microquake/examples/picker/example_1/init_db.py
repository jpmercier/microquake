import os
from glob import glob
import numpy as np
from microquake.core import ctl, logger, read_events
from microquake.db import Project
# from microquake.core import GridData
# from microquake.core.stream import composite_traces
# from microquake.seis import pick
# from microquake.seis import mag
# import microquake.nlloc as nll
# import matplotlib.pyplot as plt
# from matplotlib import cm
# from microquake.spark import init_spark_from_params


def step_wave_file(f, source, root, project):

    # abspath = os.path.join(root, f)
    relDir = os.path.relpath(root, source)
    seisfle = os.path.join(relDir, f)
    seisfle_name = os.path.basename(seisfle)

    inserted_id = project.insert_waveform(relDir, seisfle_name)
    if not inserted_id:
        logger.error('Error inserting file: %s' % seisfle)
        return False

    inserted_id = inserted_id[0]

    quakeMLfle = '%s.xml' % os.path.splitext(seisfle)[0]
    quakeMLfle = os.path.join(project.events_path, quakeMLfle)

    if not os.path.exists(quakeMLfle):
        return True

    cat = read_events(quakeMLfle)  # reading the catalog file
    num_events = len(cat.events)

    dirname = os.path.dirname(quakeMLfle)
    relDir = os.path.relpath(dirname, project.events_path)
    quakeMLfle_name = os.path.basename(quakeMLfle)
    inserted_ids = project.insert_event(cat, relDir, quakeMLfle_name, inserted_id)

    # Update seis with number of events
    project.waveforms_col.update_one({'_id': inserted_id}, {'$set':{'num_events': num_events}})

    if len(inserted_ids) != num_events:
        logger.warning('Not all of the events were inserted: %s' % quakeMLfle)
        return False

    return True


if __name__ == '__main__':

    params = ctl.parse_control_file('input.xml')

    project = Project(params.project)
    project.connect()

    project.traverse_folder(project.wave_path, step_wave_file)
