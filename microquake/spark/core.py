# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: core.py
#  Purpose: module to interract with apache spark
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
module to interract with apache spark

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""

# from pyspark import SparkConf, SparkContext
from IPython.core.debugger import Tracer

def init_spark_from_params(params):
    """
    create a SparkContext from params return empty list is spark is disable
    :param params: parameter object
    :type params: object return by microquake.core.ctl.parse_control_file()
    function
    :rtype: spark.SparkContext
    """
    if not params.spark_enabled:
        return None

    from pyspark import SparkConf, SparkContext
    return SparkContext(conf=params.spark.conf)


def mq_map(SparkContext, f, iterable, *args, **kwargs):
    """
    function runner which runs the function using spark map or flat_map if a
    SparkContext is defined. If no SparkContext is defined, the function is run
    using the python map function which output is flattened or not depending on
    the value of the flat_map argument.

    :param sc: spark context (can be empty or None)
    :type sc: pyspark.SparkContext
    :param f: a function taking an item of the iterable as first argument
    :type f: function
    :param iterable: list or spark rdd containing a list of objects
    :type iterable: any python iterable (caution some iterable may be hard to
    sparkify)
    :rtype: list or pyspark.SparkContext.rdd
    """

    flat_map = kwargs.get('flat_map')
    sc = SparkContext
    lambda_f = lambda item: f(item, *args, **kwargs)

    if sc:
        rdd = sc.parallelize(iterable, len(iterable))

        if flat_map:
            return rdd.flatMap(lambda_f).collect()
        else:
            return rdd.map(lambda_f).collect()
    else:
        return _map_function(lambda_f, iterable, *args, **kwargs)


def _map_function(f, iterable, *args, **kwargs):
    """
    Simple extension of the Python map function returning a flattened output if
    the flat_map argument is true.

   :param f: a function taking an item of the iterable as first argument
   :type f: function
   :param iterable: list or spark rdd containing a list of objects
   :type iterable: any python iterable
    """
    flat_map = kwargs.get('flat_map')
    # lambda_f = lambda item: f(item, *args, **kwargs)
    if flat_map:
        return [item for sl in map(f, iterable) for item in sl]
    else:
        return map(f, iterable)



