from microquake.core import readStations
from microquake.core import read

site = readStations('test_system.csv', format='CSV')

st = read('test.segy', format='ESG_SEGY', site=site)