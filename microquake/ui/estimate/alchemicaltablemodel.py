from PySide import QtCore, QtGui
# from estimateui import Ui_EstimateUI
# from IPython.core.debugger import Tracer


class AlchemicalTableModel(QtCore.QAbstractTableModel):
	"""
	A Qt Table Model that binds to a SQL Alchemy query
	from https://gist.github.com/harvimt/4699169#file-alchemical_model-py
 
	Example:
	# >>> model = AlchemicalTableModel(Session, [('Name', Entity.name)])
	# >>> table = QTableView(parent)
	# >>> table.setModel(model)
	"""

	def __init__(self, session, query, columns):
		super(AlchemicalTableModel, self).__init__()
		# TODO self.sort_data = None
		self.session = session
		self.fields = columns
		self.query = query

		self.results = None
		self.count = None
		self.sort = None
		self.filter = None

		self.refresh()

	def headerData(self, col, orientation, role):
		if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
			return self.fields[col][0]
		return None

	def setFilter(self, filter):
		"""Sets or clears the filter, clear the filter by setting to None"""
		self.filter = filter
		self.refresh()

	def refresh(self):
		"""Recalculates, self.results and self.count"""

		self.layoutAboutToBeChanged.emit()

		q = self.query
		if self.sort is not None:
			order, col = self.sort
			col = self.fields[col][1]
			if order == QtCore.Qt.DescendingOrder:
				col = col.desc()
		else:
			col = None

		if self.filter is not None:
			q = q.filter(self.filter)

		q = q.order_by(col)

		self.results = q.all()
		self.count = q.count()
		self.layoutChanged.emit()

	def flags(self, index):
		_flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

		if self.sort is not None:
			order, col = self.sort

			if self.fields[col][3].get('dnd', False) and index.column() == col:
				_flags |= QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled

		if self.fields[index.column()][3].get('editable', False):
			_flags |= QtCore.Qt.ItemIsEditable

		return _flags

	def supportedDropActions(self):
		return QtCore.Qt.MoveAction

	def dropMimeData(self, data, action, row, col, parent):
		if action != QtCore.Qt.MoveAction:
			return

		return False

	def rowCount(self, parent):
		return self.count or 0

	def columnCount(self, parent):
		return len(self.fields)

	def data(self, index, role):
		if not index.isValid():
			return None

		elif role not in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
			return None

		row = self.results[index.row()]
		name = self.fields[index.column()][2]

		return unicode(getattr(row, name))

	def setData(self, index, value, role=None):
		row = self.results[index.row()]
		name = self.fields[index.column()][2]

		try:
			setattr(row, name, value.toString())
			self.session.commit()
		except Exception as ex:
			QtGui.QMessageBox.critical(None, 'SQL Error', unicode(ex))
			return False
		else:
			self.dataChanged.emit(index, index)
			return True

	def sort(self, col, order):
		"""Sort table by given column number."""
		self.sort = order, col
		self.refresh()
