import argparse
import sys
from PySide import QtCore, QtGui, QtSql
from estimateui import Ui_EstimateUI
# from subsystemtableview import ComboDelegate
# from designTree import Node
from collections import defaultdict
# import json
# from IPython.core.debugger import Tracer


def tree():
	return defaultdict(tree)


class Estimate(QtGui.QMainWindow):

	def __init__(self, parent=None):

		super(Estimate, self).__init__(parent)

		self.baseUI = Ui_EstimateUI()
		self.baseUI.setupUi(self)

		self.baseUI.butAddSub.clicked.connect(self.insertSubsystem)
		self.baseUI.butRemSub.clicked.connect(self.removeSubsystem)
		self.baseUI.butApplyDesign.clicked.connect(self.baseUI.designTree.saveTree)

		self.baseUI.action_Load_Definition.triggered.connect(self.loadSystemAction)
		self.baseUI.action_Save_Definition.triggered.connect(self.saveSystemAction)
		self.baseUI.actionE_xit.triggered.connect(self.close)
		self.baseUI.action_About.triggered.connect(self.about)

	def loadSystem(self, filename=None):
		if filename is not None:
			pass

		if not self.loadDB():
			return False

		self.updateSubsystems()
		self.baseUI.designTree.loadTree(0)

	def loadDB(self):
		db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
		db.setDatabaseName("db.sqlite")
		if not db.open():
			QtGui.QMessageBox.critical(None, QtGui.qApp.tr("Cannot open database"),
				QtGui.qApp.tr("Unable to establish a database connection."),
				QtGui.QMessageBox.Cancel, QtGui.QMessageBox.NoButton)
			return False
		return True

	def updateSubsystems(self):
		self.subsystemsModel = QtSql.QSqlTableModel(self)
		self.subsystemsModel.setTable("subsystems")
		self.subsystemsModel.setEditStrategy(QtSql.QSqlTableModel.OnFieldChange)
		if not self.subsystemsModel.select():
			print self.subsystemsModel.lastError()
			return False
		self.baseUI.subsystemsView.setModel(self.subsystemsModel)
		self.baseUI.subsystemsView.hideColumn(0)
		self.baseUI.subsystemsView.selectRow(0)
		self.subsystemsSelection = self.baseUI.subsystemsView.selectionModel()
		self.subsystemsSelection.selectionChanged.connect(self.updateCurrentSubsystem)

	# @QtCore.Slot(QtGui.QItemSelection, QtGui.QItemSelection)
	def updateCurrentSubsystem(self, curSel, prevSel):
		if curSel.count() == 0:
			self.baseUI.designTree.setEnabled(False)
			return

		self.baseUI.designTree.setEnabled(True)

		selIndex = curSel.indexes()[0]
		sysId = selIndex.model().index(selIndex.row(), 0).data()
		self.baseUI.designTree.saveTree(prevSel)
		self.baseUI.designTree.loadTree(sysId)

	def insertSubsystem(self):
		row = self.baseUI.subsystemsView.currentIndex().row()

		if row < 0:  # No row selected currently
			row = self.subsystemsModel.rowCount()  # add row at bottom

		sqlRecord = self.subsystemsModel.record()
		self.subsystemsModel.insertRecord(row, sqlRecord)

		index = self.subsystemsModel.index(row, 1)
		self.baseUI.subsystemsView.setCurrentIndex(index)
		self.baseUI.subsystemsView.edit(index)

	def removeSubsystem(self):
		row = self.baseUI.subsystemsView.currentIndex().row()
		self.subsystemsModel.removeRow(row)
		self.subsystemsModel.submitAll()

	def loadTree(self, subsystem_id):

		self.baseUI.designTree.loadTree()
		
	# def onTreeWidgetItemDoubleClicked(self, item, column):
	# 	print item, column
	# 	# if (isEditable(column)) {
	# 	self.baseUI.designTree.editItem(item, column)
	# 	# }

	def about(self):
		pass

	def loadSystemAction(self):
		self.loadSystem('')

	def saveSystemAction(self):
		self.saveSystem('')

	def closeEvent(self, event):
		pass
		# result = QtGui.QMessageBox.question(self,
		# 			"Confirm Exit...",
		# 			"Are you sure you want to exit ?",
		# 			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
		# event.ignore()
	
		# if result == QtGui.QMessageBox.Yes:
		# 	event.accept()

	def cleanup(self):
		print 'Cleaning up'


def main():

	parser = argparse.ArgumentParser(description='microquake System Cost Estimate')
	parser.add_argument('--systemFile', '-f', help='The system definition file to load', default='system.xml', required=False)

	args = parser.parse_args()
	systemFile = args.systemFile

	# Create the GUI application
	qApp = QtGui.QApplication(sys.argv)
	#qApp = QtGui.QApplication()
	
	myWin = Estimate(parent=qApp.activeWindow())
	myWin.show()
	if systemFile is not None:
		myWin.loadSystem(systemFile)

	qApp.connect(qApp, QtCore.SIGNAL("aboutToQuit()"), myWin.cleanup)
	sys.exit(qApp.exec_())


if __name__ == "__main__":
	main()
