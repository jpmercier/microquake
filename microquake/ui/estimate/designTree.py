from PySide import QtGui, QtCore, QtSql


def enum(*sequential, **named):
	enums = dict(zip(sequential, range(len(sequential))), **named)
	enums['len'] = len(sequential)
	return type('Enum', (), enums)


COLUMNS = enum('TYPE', 'SUBSYSTEM', 'NAME', 'SUBTYPE', 'MANUFACTURER', 'MODEL', 'CABLE_LENGTH', 'X', 'Y', 'Z')
DISPLAY_COLS = ["Items", "Subystem", "Name", "Subtype", "Manufacturer", "Model", "Cable Length[m]", "X", "Y", "Z"]
QUERYCOLS = enum('DESIGN_ID', 'DESIGN_NAME', 'SUBSYSTEM', 'SUBSYSTEM_ID', 'PARENT', 'PART_ID',
	'SUBSYSTEM_ID_2', 'SUBSYSTEM_NAME', 'SUBSYSTEM_TYPE',
	'PART_ID_2', 'TYPE', 'SUBTYPE', 'MANUFACTURER', 'MODEL')
QUERYCOLS2 = enum('PART_ID', 'TYPE', 'SUBTYPE', 'MANUFACTURER', 'MODEL', 'SENSOR_ID',
	'SENSOR_ID_2', 'CABLE_LENGTH', 'X', 'Y', 'Z', 'PARENT')


class Node:
	def __init__(self, curId, curSubsystemId, s):
		self.id = curId
		self.subsystemId = curSubsystemId
		self.itemData = s
		self.parent = None
		self.index = None
		self.children = []

	def data(self, column):
		try:
			return self.itemData[column]
		except IndexError:
			return None

	def row(self):
		if self.parent:
			return self.parent.children.index(self)
 
		return 0

	def append(self, n):
		n.parent = self
		self.children.append(n)

	def remove(self):
		if self.parent is None:
			return

		self.parent.children.remove(self)

	@staticmethod
	def makeTreeNode(curId, subsystemId, data):
		subsystem = (data[QUERYCOLS.SUBSYSTEM] == 1)

		if subsystem:
			printCols = [QUERYCOLS.SUBSYSTEM_NAME, QUERYCOLS.SUBSYSTEM, QUERYCOLS.DESIGN_NAME, QUERYCOLS.SUBSYSTEM_TYPE]
		else:
			printCols = [QUERYCOLS.TYPE, QUERYCOLS.SUBSYSTEM, QUERYCOLS.DESIGN_NAME,
				QUERYCOLS.SUBTYPE, QUERYCOLS.MANUFACTURER, QUERYCOLS.MODEL]
		printData = [data[i] for i in printCols]
		return Node(curId, subsystemId, printData)

	@staticmethod
	def makeSensorNode(curId, data):
		printCols = [QUERYCOLS2.TYPE, -1, -1,
			QUERYCOLS2.SUBTYPE, QUERYCOLS2.MANUFACTURER, QUERYCOLS2.MODEL,
			QUERYCOLS2.CABLE_LENGTH, QUERYCOLS2.X, QUERYCOLS2.Y, QUERYCOLS2.Z]
		printData = ['' if i < 0 else data[i] for i in printCols]
		printData[COLUMNS.SUBSYSTEM] = 0
		return Node(curId, None, printData)

	@staticmethod
	def addTreeNode(curNode, curId, subsystemId, data, parent):

		if parent == '' or curNode.subsystemId == parent:
			curNode.append(Node.makeTreeNode(curId, subsystemId, data))
			return True

		for n in curNode.children:
			if Node.addTreeNode(n, curId, subsystemId, data, parent):
				break

		return False

	@staticmethod
	def traverse(curNode, curId):

		if curNode.id == curId:
			return True, curNode

		for n in curNode.children:
			ok, node = Node.traverse(n, curId)
			if ok:
				return True, node

		return False, None

	@staticmethod
	def traverseByType(curNode, t, foundNodes):

		if curNode.data(COLUMNS.TYPE) == t:
			foundNodes.append(curNode)

		for n in curNode.children:
			Node.traverseByType(n, t, foundNodes)


class TreeModel(QtCore.QAbstractItemModel):
#  examples/itemviews/simpletreemodel/simpletreemodel.py
#  https://qt.gitorious.org/pyside/pyside-examples/source/060dca8e4b82f301dfb33a7182767eaf8ad3d024:
# 	examples/itemviews/simpletreemodel/simpletreemodel.py#LNaN-NaN
	def __init__(self, tree):
		super(TreeModel, self).__init__()
		self.rootItem = Node(0, None, DISPLAY_COLS)
		self.rootItem.append(tree)
 
	def flags(self, index):
		if not index.isValid():
			return QtCore.Qt.NoItemFlags

		if self.data2(index, COLUMNS.SUBSYSTEM) == 1 and\
			index.column() > COLUMNS.NAME:
			return QtCore.Qt.ItemIsSelectable \
			 | QtCore.Qt.ItemIsUserCheckable \
			 | QtCore.Qt.ItemIsEditable

		flag = QtCore.Qt.ItemIsEnabled \
			 | QtCore.Qt.ItemIsSelectable \
			 | QtCore.Qt.ItemIsUserCheckable \
			 | QtCore.Qt.ItemIsEditable
			# | QtCore.Qt.ItemIsDragEnabled \
			# | QtCore.Qt.ItemIsDropEnabled
		return flag
 
	def index(self, row, column, parent):
		if not self.hasIndex(row, column, parent):
			return QtCore.QModelIndex()
 
		if not parent.isValid():
			parentItem = self.rootItem
		else:
			parentItem = parent.internalPointer()
 
		childItem = parentItem.children[row]
		if childItem:
			return self.createIndex(row, column, childItem)
		else:
			return QtCore.QModelIndex()
 
	def parent(self, index):
		if not index.isValid():
			return QtCore.QModelIndex()
 
		childItem = index.internalPointer()
		parentItem = childItem.parent
 
		if parentItem == self.rootItem:
			return QtCore.QModelIndex()
 
		return self.createIndex(parentItem.row(), 0, parentItem)
 
	def rowCount(self, parent):
		if parent.column() > 0:
			return 0
 
		if not parent.isValid():
			parentItem = self.rootItem
		else:
			parentItem = parent.internalPointer()
 
		return len(parentItem.children)
 
	def columnCount(self, parent):
		return len(self.rootItem.itemData)

	def headerData(self, section, orientation, role):
		if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
			return self.rootItem.data(section)
 
		return None
 
	def data(self, index, role=QtCore.Qt.DisplayRole):

		return self.data2(index, index.column(), role=role)

	def data2(self, index, column, role=QtCore.Qt.DisplayRole):
		if not index.isValid():
			return None

		node = index.internalPointer()
		data = None

		if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:

			data = node.data(column)
			# print data
			# Tracer()()
		# if role == QtCore.Qt.ToolTipRole:
		# 	node = index.internalPointer()
		# 	data = "ToolTip " + node.txt
		# if role == QtCore.Qt.DecorationRole:
		# 	data = QtGui.QIcon("icon.png")
		return data
 
	def setData(self, index, value, role=QtCore.Qt.DisplayRole):
		node = index.internalPointer()
		node.itemData[index.column()] = value
		return True


class ComboDelegate(QtGui.QItemDelegate):
	"""
	A delegate that places a fully functioning QComboBox in every
	cell of the column to which it's applied
	"""

	EMPTY_ITEM = '<any>'

	def __init__(self, parent):
 
		QtGui.QItemDelegate.__init__(self, parent)
		
	def createEditor(self, parent, option, index):

		choices = []
		if index.column() == COLUMNS.TYPE:
			if index.model().data2(index, COLUMNS.SUBSYSTEM) == 1:
				query = "SELECT name from subsystems WHERE type <> 'System' GROUP BY name"
			else:
				query = "SELECT type from parts WHERE type <> 'Sensor' GROUP BY type"
			# print query
			condition = ''
		else:
			choices.append(self.EMPTY_ITEM)
			if index.column() == COLUMNS.SUBTYPE:
				query = "SELECT subtype from parts %sGROUP BY subtype"
				condition = ''
			elif index.column() == COLUMNS.MANUFACTURER:
				query = "SELECT manufacturer from parts %sGROUP BY manufacturer"
				if not index.model().data2(index, COLUMNS.SUBTYPE):
					condition = ''
				else:
					condition = "AND subtype = '%s' " % index.model().data2(index, COLUMNS.SUBTYPE)
				if not index.model().data2(index, COLUMNS.MODEL):
					condition += ''
				else:
					condition += "AND model = '%s' " % index.model().data2(index, COLUMNS.MODEL)
			elif index.column() == COLUMNS.MODEL:
				query = "SELECT model from parts %sGROUP BY model"
				if not index.model().data2(index, COLUMNS.SUBTYPE):
					condition = ''
				else:
					condition = "AND subtype = '%s' " % index.model().data2(index, COLUMNS.SUBTYPE)
				if not index.model().data2(index, COLUMNS.MANUFACTURER):
					condition += ''
				else:
					condition += "AND manufacturer = '%s' " % index.model().data2(index, COLUMNS.MANUFACTURER)
			else:
				return super(ComboDelegate, self).createEditor(parent, option, index)

			query %= "WHERE type = '%s' %s" % (index.model().data2(index, COLUMNS.TYPE), condition)

		query = QtSql.QSqlQuery(query)
		while query.next():
			val = query.value(0)
			if val != '':
				choices.append(val)

		combo = QtGui.QComboBox(parent)
		combo.addItems(choices)
		self.connect(combo, QtCore.SIGNAL("currentIndexChanged(int)"), self, QtCore.SLOT("currentIndexChanged()"))
		return combo
		
	def setEditorData(self, editor, index):
		if index.column() not in [COLUMNS.TYPE, COLUMNS.SUBTYPE, COLUMNS.MANUFACTURER, COLUMNS.MODEL]:
			return super(ComboDelegate, self).setEditorData(editor, index)

		editor.blockSignals(True)
		modelText = str(index.model().data(index))
		editorIndex = editor.findText(modelText)
		if editorIndex < 0:
			editor.setCurrentIndex(0)
		else:
			editor.setCurrentIndex(editorIndex)
		editor.blockSignals(False)
		
	def setModelData(self, editor, model, index):
		if index.column() not in [COLUMNS.TYPE, COLUMNS.SUBTYPE, COLUMNS.MANUFACTURER, COLUMNS.MODEL]:
			return super(ComboDelegate, self).setModelData(editor, model, index)

		if editor.currentText() == self.EMPTY_ITEM:
			model.setData(index, None)
		else:
			model.setData(index, editor.currentText())
		
	def currentIndexChanged(self):
		self.commitData.emit(self.sender())
		self.closeEditor.emit(self.sender(), QtGui.QAbstractItemDelegate.NoHint)


class DesignTree(QtGui.QTreeView):

	def __init__(self, parent):
		super(DesignTree, self).__init__(parent)

		self.viewSystem = None

		self.setItemDelegate(ComboDelegate(self))

		self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.customContextMenuRequested.connect(self.customMenuRequested)

	def customMenuRequested(self, point):

		index = self.indexAt(point)
		if not index.isValid():
			return

		typeToAdd, canRemove = self.getItemTypeToAddRemove(index)
		if len(typeToAdd) == 0 and not canRemove:
			return

		menu = QtGui.QMenu(self)
		self.addItemActions = []
		for t in typeToAdd:
			a = QtGui.QAction("Add " + t, self)
			menu.addAction(a)
			a.triggered.connect(lambda t=t: self.addItem(index, t))
			self.addItemActions.append(a)

		if canRemove:
			self.removeItemAction = QtGui.QAction("Remove", self)
			menu.addAction(self.removeItemAction)
			self.removeItemAction.triggered.connect(lambda: self.removeItem(index))

		menu.popup(self.viewport().mapToGlobal(point))

	def rowsInserted(self, parent, start, end):

		super(DesignTree, self).rowsInserted(parent, start, end)

		child = self.model().index(start, self.editColumn, parent)
		if not child.isValid():
			print 'Invalid'
			return
			
		self.selectionModel().setCurrentIndex(child, QtGui.QItemSelectionModel.SelectCurrent | QtGui.QItemSelectionModel.Rows)
		self.edit(child)

	def addItem(self, index, itemType):

		node = index.internalPointer()
		lastRow = len(node.children)
		self.model().beginInsertRows(index, lastRow, lastRow)

		if itemType == "Sensor":
			node.append(Node(9001, None, ["Sensor", 0, "", "", "", "", "", "", "", ""]))
		elif itemType == "Component":
			node.append(Node(9001, None, ["", 0, "", "", "", "", "", "", "", ""]))
		elif itemType == "Subsystem":
			node.append(Node(9001, None, ["", 1, ""]))

		if itemType == "Sensor":
			self.editColumn = COLUMNS.SUBTYPE
		else:
			self.editColumn = COLUMNS.TYPE

		self.model().endInsertRows()

	def removeItem(self, index):

		node = index.internalPointer()
		self.model().beginRemoveRows(index.parent(), index.row(), index.row())

		node.remove()

		self.model().endRemoveRows()

	def getItemTypeToAddRemove(self, index):

		if index.model().data2(index, COLUMNS.SUBTYPE) == "System":
			return ["Subsystem"], False
		elif index.model().data2(index, COLUMNS.SUBSYSTEM) == 1:
			canRemove = index.parent().isValid()
			canRemove = canRemove and not index.parent().parent().isValid()
			if self.viewSystem:
				return ["Sensor"], canRemove
			else:
				if not index.parent().isValid():
					return ["Subsystem", "Component", "Sensor"], False
				else:
					return ["Sensor"], canRemove
		elif index.model().data2(index, COLUMNS.TYPE) == "Sensor":
			return [], True

		canRemove = (not index.parent().parent().isValid())
		return [], canRemove

	def loadTree(self, subsystem_id):

		self.showSubComponents = False

		query2 = """SELECT MIN(d.id) FROM design d
			LEFT JOIN subsystems s ON d.subsystem_id=s.id
			WHERE s.id = %s"""
		query = QtSql.QSqlQuery(query2 % subsystem_id)
		query.exec_()
		query.next()
		baseId = query.value(0)

		query2 = """SELECT d.*, s.*, p.*
			FROM design d
			LEFT JOIN subsystems s ON d.subsystem_id=s.id
			LEFT JOIN design_parts p ON d.design_part_id=p.id
			ORDER BY parent_subsystem_id"""

		self.viewSystem = (subsystem_id == 0)

		query = QtSql.QSqlQuery(query2)
		query.exec_()
		root = None

		while query.next():
			d_id = query.value(QUERYCOLS.DESIGN_ID)
			s_id = query.value(QUERYCOLS.SUBSYSTEM_ID)
			parent_id = query.value(QUERYCOLS.PARENT)
			rr = query.record()
			rr_list = [rr.value(i) for i in range(rr.count())]

			if root is None:
				root = Node.makeTreeNode(d_id, s_id, rr_list)
			else:
				if self.showSubComponents or s_id or parent_id == subsystem_id:
					Node.addTreeNode(root, d_id, s_id, rr_list, parent_id)

		query2 = """SELECT p.*, r.*
			FROM design_parts p,  design_sensors r
			WHERE p.sensor_id=r.id"""

		query = QtSql.QSqlQuery(query2)
		query.exec_()

		while query.next():
			val = query.value(QUERYCOLS2.PART_ID)
			parent_id = query.value(QUERYCOLS2.PARENT)
			rr = query.record()
			rr_list = [rr.value(i) for i in range(rr.count())]

			ok, node = Node.traverse(root, parent_id)
			if ok:
				node.append(Node.makeSensorNode(val, rr_list))

		ok, subtree = Node.traverse(root, baseId)
		if not ok:
			QtGui.QMessageBox.warning(self, 'Subsystem',
				"Please add the subsystem to your design before editing it.")
			return

		print root.itemData, subtree.itemData, baseId
		self.designTreeModel = TreeModel(subtree)
		self.setModel(self.designTreeModel)

		self.setColumnWidth(COLUMNS.CABLE_LENGTH, 100)
		self.setColumnWidth(COLUMNS.X, 50)
		self.setColumnWidth(COLUMNS.Y, 50)
		self.setColumnWidth(COLUMNS.Z, 50)

		self.expandAll()
		self.resizeColumnToContents(0)

	def getSubsystemRows(self, sysId):

		q = """SELECT d.id, p.id
			FROM design d
			LEFT JOIN design_parts p
			ON d.design_part_id = p.id
			WHERE parent_subsystem_id = :sysId"""

		query = QtSql.QSqlQuery()
		query.prepare(q)
		query.bindValue(":sysId", sysId)
		query.exec_()

		designsFound = []
		partsFound = []
		while query.next():
			d_id = query.value(0)
			part_id = query.value(1)
			designsFound.append(d_id)
			if part_id:
				partsFound.append(part_id)

		return designsFound, partsFound

	def getSensorsToDelete(self, sysId):

		q = """SELECT p.id, s.id, s.parent
			FROM design_parts p, design_sensors s
			WHERE p.sensor_id = s.id"""

		query = QtSql.QSqlQuery(q)
		query.exec_()

		partsFound = []
		sensorsFound = []
		while query.next():
			part_id = query.value(0)
			sens_id = query.value(1)
			design_id = query.value(2)

			#search parent node in design tree
			ok, subtree = Node.traverse(self.model().rootItem, design_id)
			if ok:
				partsFound.append(part_id)
				sensorsFound.append(sens_id)

		return partsFound, sensorsFound

	def findSubsystemByName(self, allSubsystems, name):
		curSubId = None
		for r in range(allSubsystems.rowCount()):
			if name == allSubsystems.data(allSubsystems.index(r, 1)):
				curSubId = r
				break
		return curSubId

	def getDesigns(self):
		q = """SELECT d.id, d.name
			FROM design d"""

		query = QtSql.QSqlQuery(q)
		query.exec_()

		designs = []
		while query.next():
			designs.append((query.value(0), query.value(1)))

		return designs

	def findDesignIdByName(self, designs, name):

		for d_id, d_name in designs:
			if d_name == name:
				return d_id
		return None

	def saveTree(self, sel):

		estimateMain = self.window()
		if sel is None:
			selIndex = estimateMain.baseUI.subsystemsView.currentIndex()
		elif sel.count() == 0:
			return

		selIndex = sel.indexes()[0]
		sysId = selIndex.model().index(selIndex.row(), 0).data()
		# print 'Saving ', sysId

		designs, parts = self.getSubsystemRows(sysId)
		parts2, sensors = self.getSensorsToDelete(sysId)
		# print 'Deleting', designs, parts, parts2, sensors

		QtSql.QSqlDatabase.database().transaction()

		# delete relevant rows from DB
		q = """DELETE FROM design
			WHERE id IN (%s)"""
		query = QtSql.QSqlQuery(q % (','.join([str(e) for e in designs])))
		if not query.exec_():
			print query.lastError()
			QtSql.QSqlDatabase.database().rollback()
			return
		parts = parts + parts2
		q = """DELETE FROM design_parts
			WHERE id IN (%s)"""
		query = QtSql.QSqlQuery(q % (','.join([str(e) for e in parts])))
		if not query.exec_():
			print query.lastError()
			QtSql.QSqlDatabase.database().rollback()
			return
		# print query.lastQuery()
		q = """DELETE FROM design_sensors
			WHERE id IN (%s)"""
		query = QtSql.QSqlQuery(q % (','.join([str(e) for e in sensors])))
		if not query.exec_():
			print query.lastError()
			QtSql.QSqlDatabase.database().rollback()
			return

		allSubsystems = estimateMain.subsystemsModel

		# add subsystems and components for the current view
		rootNode = self.model().rootItem.children[0]
		for n in rootNode.children:
			itemType = n.data(COLUMNS.TYPE)
			if itemType == 'Sensor':
				continue

			part_id = None
			if n.data(COLUMNS.SUBSYSTEM) == 0:
				q = """INSERT INTO design_parts
					(part_type, part_subtype, part_manufacturer, part_model, sensor_id)
					VALUES (:type, :subtype, :manufacturer, :model, :sens_id)"""
				query = QtSql.QSqlQuery()
				if not query.prepare(q):
					print query.lastError()
					QtSql.QSqlDatabase.database().rollback()
					return

				query.bindValue(":type", n.data(COLUMNS.TYPE))
				query.bindValue(":subtype", n.data(COLUMNS.SUBTYPE))
				query.bindValue(":manufacturer", n.data(COLUMNS.MANUFACTURER))
				query.bindValue(":model", n.data(COLUMNS.MODEL))
				query.bindValue(":sens_id", None)
				if not query.exec_():
					print query.lastError()
					QtSql.QSqlDatabase.database().rollback()
					return

				part_id = query.lastInsertId()

			if n.data(COLUMNS.SUBSYSTEM) == 1:
				curSubId = self.findSubsystemByName(allSubsystems, n.data(COLUMNS.TYPE))
			else:
				curSubId = None

			q = """INSERT INTO design
				(name, is_subsystem, subsystem_id, parent_subsystem_id, design_part_id)
				VALUES (:name, :is_sub, :sub_id, :parent_id, :part_id)"""
			query = QtSql.QSqlQuery()
			if not query.prepare(q):
				print query.lastError()
				QtSql.QSqlDatabase.database().rollback()
				return

			query.bindValue(":name", n.data(COLUMNS.NAME))
			query.bindValue(":is_sub", n.data(COLUMNS.SUBSYSTEM))
			query.bindValue(":sub_id", curSubId)
			query.bindValue(":parent_id", sysId)
			query.bindValue(":part_id", part_id)
			if not query.exec_():
				print query.lastError()
				QtSql.QSqlDatabase.database().rollback()
				return

		sensorNodes = []
		Node.traverseByType(rootNode, 'Sensor', sensorNodes)
		designs = self.getDesigns()

		for n in sensorNodes:
			design_name = n.parent.data(COLUMNS.NAME)
			parent_design = self.findDesignIdByName(designs, design_name)
			if parent_design is None:
				print 'ERROR'
				QtSql.QSqlDatabase.database().rollback()
				return

			q = """INSERT INTO design_sensors
				(cable_length, X, Y, Z, parent)
				VALUES (:cable_length, :x, :y, :z, :parent_id)"""
			query = QtSql.QSqlQuery()
			if not query.prepare(q):
				print query.lastError()
				QtSql.QSqlDatabase.database().rollback()
				return

			query.bindValue(":cable_length", n.data(COLUMNS.CABLE_LENGTH))
			query.bindValue(":x", n.data(COLUMNS.X))
			query.bindValue(":y", n.data(COLUMNS.Y))
			query.bindValue(":z", n.data(COLUMNS.Z))
			query.bindValue(":parent_id", parent_design)
			if not query.exec_():
				print query.lastError()
				QtSql.QSqlDatabase.database().rollback()
				return

			sensor_id = query.lastInsertId()

			q = """INSERT INTO design_parts
				(part_type, part_subtype, part_manufacturer, part_model, sensor_id)
				VALUES (:type, :subtype, :manufacturer, :model, :sensor_id)"""
			query = QtSql.QSqlQuery()
			if not query.prepare(q):
				print query.lastError()
				QtSql.QSqlDatabase.database().rollback()
				return

			query.bindValue(":type", n.data(COLUMNS.TYPE))
			query.bindValue(":subtype", n.data(COLUMNS.SUBTYPE))
			query.bindValue(":manufacturer", n.data(COLUMNS.MANUFACTURER))
			query.bindValue(":model", n.data(COLUMNS.MODEL))
			query.bindValue(":sensor_id", sensor_id)
			if not query.exec_():
				print query.lastError()
				QtSql.QSqlDatabase.database().rollback()
				return

		QtSql.QSqlDatabase.database().commit()
