import numpy as np
import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.transforms import offset_copy

from PyQt4 import QtGui, QtCore


class SeisFigure(Figure):
	def __init__(self, baseWindow, useMainWindow):

		Figure.__init__(self)

		self.baseWindow = baseWindow
		self.useMainWindow = useMainWindow

		self.shortcuts = {'st_next': 'c',
						  'st_previous': 'x',
						  'filter_apply': 'f',
						  'pick_p': 'q',
						  'pick_s': 'w',
						  'pick_custom': 't',
						  'pick_remove': 'r',
						  'view_station': 'v'
		}

		self.set_dpi(100)
		self.set_facecolor('.86')

		self.canvas = FigureCanvas(self)
		self.canvas.setFocusPolicy(QtCore.Qt.StrongFocus)
		self.canvas.mpl_connect('scroll_event', self.onScroll)
		self.canvas.mpl_connect('motion_notify_event', self.onDrag)
		self.canvas.mpl_connect('button_release_event', self.onButtonRelease)
		self.canvas.mpl_connect('button_press_event', self.onClick)

	def getBaseApp(self):

		if self.useMainWindow:
			return self.baseWindow
		else:
			return self.baseWindow.baseApp

	def setData(self, dataStream):

		self.dataStream = dataStream
		self.drawFig()

	def drawFig(self):

		"""
        Draws all matplotlib figures
        """
		self.clear()

		num_plots = len(self.dataStream)
		if num_plots == 0:
			return

		# self._appFilter(draw=False)
		for i, tr in enumerate(self.dataStream):
			ax = self.add_subplot(num_plots, 1, i)
			delta = tr.stats.delta * 1000
			maxTime = delta * len(tr.data)
			timeData = np.arange(0, maxTime, delta)
			ax.plot(timeData, tr.data, 'k', rasterized=True)
			ax.axhline(0, color='k', alpha=.05)
			ax.set_xlim([0, maxTime])
			if self.useMainWindow:
				ax.text(.85, .9, '%s - %s' % (tr.stats.station, tr.stats.channel),
						transform=ax.transAxes, va='top', ma='left', fontsize=8)
			else:
				ax.text(.93, .9, tr.stats.channel,
						transform=ax.transAxes, va='top', ma='left', fontsize=8)
			ax.delta = delta
			ax.station = tr.stats.station
			ax.channel = tr.stats.channel
			if i == 0:
				ax.set_xlabel('Time (ms)')

			ax.tick_params(axis='both', which='major', labelsize=8)
			ax.tick_params(axis='both', which='minor', labelsize=6)

			if self.useMainWindow:
				ax.get_yaxis().set_visible(False)
			else:
				if 'G' in ax.station:
					ax.set_ylabel(r'$\mathrm{Velocity\/(m/s)}$')
				else:
					ax.set_ylabel(r'$\mathrm{Acceleration\/(m/s^2)}$')

		# plot picks
		self.drawPicks(draw=False)
		if self.useMainWindow:
			self.subplots_adjust(left=0.05, right=0.95, bottom=0.02, top=0.98, hspace=0.25)
		else:
			self.suptitle('%s - %s - %s' % (self.dataStream[-1].stats.network,
											self.dataStream[-1].stats.station,
											self.dataStream[-1].stats.starttime.isoformat()),
						  x=.2, fontsize=10)

		self.canvas.setMinimumSize(100, 100 * (num_plots + 1))

		#self._updateSB()
		self.canvasDraw()

	def canvasDraw(self):

		"""
        Redraws the canvas and re-sets mouse focus
        """
		# for i, ax in enumerate(self.get_axes()):
		#    ax.set_xticklabels(ax.get_xticks())

		self.canvas.draw()
		self.canvas.setFocus()

	def drawPicks(self, draw=True):
		"""
        Draw picklines onto axes
        """
		picks = self.getBaseApp().getPicks(self.baseWindow.currentStation, self.useMainWindow)
		xpicks = self.getBaseApp().getPickXPosition(picks)

		for ax in self.get_axes():
			lines = []
			labels = []
			transOffset = offset_copy(ax.transData, fig=self,
									  x=5, y=0, units='points')
			for i, xpick in enumerate(xpicks):
				if picks[i].phase_hint == 'S':
					color = 'r'
				elif picks[i].phase_hint == 'P':
					color = 'g'
				else:
					color = 'b'

				if ax.station != picks[i].waveform_id.station_code:
					continue

				if (not self.useMainWindow) and (not picks[i].waveform_id.channel_code in ax.channel):
					continue

				# alpha = .1
				#else:
				alpha = .8

				#xsample = xpick / ax.delta

				lines.append(matplotlib.lines.Line2D([xpick, xpick],
													 [ax.get_ylim()[0] * .9, ax.get_ylim()[1] * .8],
													 color=color, alpha=alpha))
				lines[-1].obspy_pick = picks[i]

				labels.append(matplotlib.text.Text(xpick,
												   ax.get_ylim()[1] * .8, text=picks[i].phase_hint,
												   color=color, size=10, alpha=alpha,
												   transform=transOffset))

			# delete all artists
			del ax.artists[0:]
			# add updated objects
			for line in lines:
				ax.add_artist(line)
			for label in labels:
				ax.add_artist(label)

		if draw:
			self.canvasDraw()

	# Plot Controls
	def onScroll(self, event):
		"""
        Scrolls/Redraws the plots along x axis
        """
		if event.inaxes is None:
			return

		if event.key == 'control':
			axes = [event.inaxes]
		else:
			axes = self.get_axes()

		for ax in axes:
			left = ax.get_xlim()[0]
			right = ax.get_xlim()[1]
			extent = right - left
			dzoom = .2 * extent
			aspect_left = (event.xdata - ax.get_xlim()[0]) / extent
			aspect_right = (ax.get_xlim()[1] - event.xdata) / extent

			if event.button == 'up':
				left += dzoom * aspect_left
				right -= dzoom * aspect_right
			elif event.button == 'down':
				left -= dzoom * aspect_left
				right += dzoom * aspect_right
			else:
				return
			ax.set_xlim([left, right])
		self.canvasDraw()

	def onDrag(self, event):
		"""
        Drags/Redraws the plot upon drag
        """
		if event.inaxes is None:
			return

		if event.key == 'control':
			axes = [event.inaxes]
		else:
			axes = self.get_axes()

		if event.button == 2:
			if self.drag is None:
				self.drag = event.xdata
				return
			for ax in axes:
				ax.set_xlim([ax.get_xlim()[0] +
							 (self.drag - event.xdata),
							 ax.get_xlim()[1] + (self.drag - event.xdata)])
		else:
			return
		self.canvasDraw()

	def onButtonRelease(self, event):
		"""
        On Button Release Reset drag variable
        """
		self.drag = None


	def onClick(self, event):

		if event.key is not None:
			event.key = event.key.lower()
		if event.inaxes is None:
			return

		station = event.inaxes.station
		channel = event.inaxes.channel
		tr_amp = event.inaxes.lines[0].get_ydata()[int(event.xdata) + 3] - \
				 event.inaxes.lines[0].get_ydata()[int(event.xdata)]

		if tr_amp < 0:
			polarity = 'negative'
		elif tr_amp > 0:
			polarity = 'positive'
		else:
			polarity = 'undecideable'

		baseApp = self.getBaseApp()

		if event.key == self.shortcuts['pick_p'] and event.button == 1:
			baseApp.addPicks(event.xdata, phase='P', station=station, polarity=polarity)
		elif event.key == self.shortcuts['pick_s'] and event.button == 1:
			baseApp.addPicks(event.xdata, phase='P', station=station, polarity=polarity)
		elif event.key == self.shortcuts['pick_custom'] and event.button == 1:
			text, ok = QtGui.QInputDialog.getItem(self, 'Custom Phase',
												  'Enter phase name:', self.getPhases())
			if ok:
				baseApp.addPicks(event.xdata, phase=text, station=station, polarity=polarity)
		elif event.key == self.shortcuts['pick_remove']:
			baseApp.delPicks(station=station)
		elif self.useMainWindow and event.key == self.shortcuts['view_station'] and event.button == 1:
			print 'open'
			self.baseWindow.openStationPlot(station)
		else:
			return

		# self._updateSB()
		self.drawPicks()

		#if not event.dblclick:
		#    return
