#!/usr/bin/env python
import os
import sys
from time import sleep
import optparse
from collections import defaultdict
from PyQt4 import QtGui, QtCore

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
    
try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

from obspy.core import event, UTCDateTime
import numpy as np
from PyQt4.QtCore import Qt
from microquake.core import logger

from microquake.ui.picker.uQuakeWaveui import Ui_uQuakeWaveUI
from microquake.ui.picker.stationplot import StationPlot
from microquake.ui.picker.seisfigure_pyplot import SeisFigure
from microquake.ui.picker.stereoplot import StereoPlot
from microquake.ui.picker.model import HybridWaveModel
from microquake.nlloc import init_nlloc_from_params

from IPython.core.debugger import Tracer


# def tree(): return defaultdict(tree)

class TreeItemDelegate(QtGui.QStyledItemDelegate):
    def __init__(self, parent):
        super(TreeItemDelegate, self).__init__(parent)

        # def paint(self, painter, option, index):
        #
        # 	if index.data(5) == 1 && option.state.testFlag(QStyle::State_Selected):
        # 		newOption = QStyleOptionViewItem(option)
        # 		newOption.palette.setBrush(QPalette::Normal, QPalette::Highlight , someOtherBrush);
        # 		QStyledItemDelegate::paint(painter, newOption, index);
        # 	  return;
        #  }
        # 	super(TreeItemDelegate, self).paint(painter, option, index)


class TreeWidgetItem(QtGui.QTreeWidgetItem):
    pass


# def __lt__(self, a):
#
# 	if self.childCount() == 0 or a.childCount() == 0:
# 		return self.text(0) < a.text(0)
#
# 	print self.text(1), a.text(1)
# 	if self.treeWidget().sortColumn() == 1:
# 		return float(self.text(1)) < float(a.text(1))
# 	else:
# 		return self.text(2) < a.text(2)


class TaskThread(QtCore.QThread):
    taskFinished = QtCore.pyqtSignal()

    def __init__(self):
        QtCore.QThread.__init__(self)

        self.function = None
        self.args = None
        self.kwargs = None
        self.cb = None

    def init(self, function, cb, *args, **kwargs):
        self.function = function
        self.args = args
        self.kwargs = kwargs

        if self.cb:
            self.taskFinished.disconnect(self.cb)

        self.cb = cb
        if cb:
            self.taskFinished.connect(cb)

    def __del__(self):
        self.wait()

    def run(self):
        self.function(*self.args, **self.kwargs)
        self.taskFinished.emit()


class uQuakeWave(QtGui.QMainWindow):
    def __init__(self):

        QtGui.QMainWindow.__init__(self)

        self.baseUI = Ui_uQuakeWaveUI()
        self.baseUI.setupUi(self)

        self.currentStation = None
        self.stationPlotUI = None

        self.model = None
        self.ordered_files = []

        self.mode = 'none'
        self.modes = ['none', 'pick_p', 'pick_s', 'pick_end', 'pick_custom',
                      'pick_remove', 'pick_None']
        self.shortcuts = {'pick_p': Qt.Key_Q,
                          'pick_s': Qt.Key_W,
                          'pick_end': Qt.Key_E,
                          'pick_custom': Qt.Key_T,
                          'pick_remove': Qt.Key_R,
                          'pick_None': Qt.Key_Escape,
                          'type_U': Qt.Key_1,
                          'type_E': Qt.Key_2,
                          'type_B': Qt.Key_3,
                          'type_M': Qt.Key_4,
                          'accept_reject': Qt.Key_Space}

        # self._longTask = None

        self._initUI()
        self.toggleFilter(False, draw=False)
        self.show()

    def _initUI(self):

        self.mainArea = self.baseUI.mainArea
        self.scrollContents = self.mainArea.widget()

        # ---- Menubar ----

        showGroup = QtGui.QActionGroup(self.baseUI.menuBar)
        showGroup.addAction(self.baseUI.actionShowOnlyAccepted)
        showGroup.addAction(self.baseUI.actionShowBoth)

        self.connect(self.baseUI.actionShowOnlyAccepted,
                     QtCore.SIGNAL("triggered()"), self.toggleEventVisibility)
        self.connect(self.baseUI.actionShowBoth, QtCore.SIGNAL("triggered()"),
                     self.toggleEventVisibility)

        # ---- Main toolbar ----

        self.connect(self.baseUI.actionOpen, QtCore.SIGNAL("triggered()"),
                     self.connect_data_source)
        self.connect(self.baseUI.actionCommit, QtCore.SIGNAL("triggered()"),
                     self.commitModifications)

        # ---- Seismogram selector toolbar ----

        self.seisCountLabel = QtGui.QLabel(self.baseUI.seisToolBar)
        self.seisCountLabel.setText('<b>(1/1)</b>')
        self.baseUI.seisToolBar.addWidget(self.seisCountLabel)

        prevSeis = QtGui.QAction(self.baseUI.seisToolBar)
        iconPrevSeis = QtGui.QIcon()
        iconPrevSeis.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/resultset_previous.png")),
                               QtGui.QIcon.Normal, QtGui.QIcon.Off)
        prevSeis.setIcon(iconPrevSeis)
        prevSeis.setIconText(_translate("uQuakeWaveUI", "&Prev", None))
        self.connect(prevSeis, QtCore.SIGNAL("triggered()"), self.prevSeis)
        self.baseUI.seisToolBar.addAction(prevSeis)

        nextSeis = QtGui.QAction(self.baseUI.seisToolBar)
        iconNextSeis = QtGui.QIcon()
        iconNextSeis.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/resultset_next.png")),
                               QtGui.QIcon.Normal, QtGui.QIcon.Off)
        nextSeis.setIcon(iconNextSeis)
        nextSeis.setIconText(_translate("uQuakeWaveUI", "&Next", None))
        self.connect(nextSeis, QtCore.SIGNAL("triggered()"), self.nextSeis)
        self.baseUI.seisToolBar.addAction(nextSeis)

        # ---- Event selector toolbar ----

        self.butCloneOrigin = QtGui.QAction(self.baseUI.eventToolBar)
        iconCloneOrigin = QtGui.QIcon()
        iconCloneOrigin.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/page_copy.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.butCloneOrigin.setIcon(iconCloneOrigin)
        self.butCloneOrigin.setIconText(_translate("uQuakeWaveUI", "&Clone Origin", None))
        self.connect(self.butCloneOrigin, QtCore.SIGNAL("triggered()"), self.cloneOrigin)
        self.baseUI.eventToolBar.addAction(self.butCloneOrigin)

        self.butDeleteOrigin = QtGui.QAction(self.baseUI.eventToolBar)
        iconDeleteOrigin = QtGui.QIcon()
        iconDeleteOrigin.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/delete.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.butDeleteOrigin.setIcon(iconDeleteOrigin)
        self.butDeleteOrigin.setIconText(_translate("uQuakeWaveUI", "&Delete Origin", None))
        self.butDeleteOrigin.setEnabled(False)
        self.connect(self.butDeleteOrigin, QtCore.SIGNAL("triggered()"), self.deleteOrigin)
        self.baseUI.eventToolBar.addAction(self.butDeleteOrigin)

        # ---- Event Type toolbar ----

        self.eventType = QtGui.QActionGroup(self.baseUI.mainToolBar)

        typeUndetermined = QtGui.QAction(self.eventType)
        typeUndetermined.setText(_translate("uQuakeWaveUI", "U", None))
        typeUndetermined.setCheckable(True)
        typeEvent = QtGui.QAction(self.eventType)
        typeEvent.setText(_translate("uQuakeWaveUI", "E", None))
        typeEvent.setCheckable(True)
        typeBlast = QtGui.QAction(self.eventType)
        typeBlast.setText(_translate("uQuakeWaveUI", "B", None))
        typeBlast.setCheckable(True)
        typeMechanical = QtGui.QAction(self.eventType)
        typeMechanical.setText(_translate("uQuakeWaveUI", "M", None))
        typeMechanical.setCheckable(True)

        self.connect(self.eventType, QtCore.SIGNAL("selected(QAction *)"), self.changeEventType)
        self.baseUI.mainToolBar.addActions(self.eventType.actions())
        self.baseUI.mainToolBar.addSeparator()

        # ---- Event Status toolbar ----

        self.eventStatusGroup = QtGui.QActionGroup(self.baseUI.mainToolBar)

        statusAccepted = QtGui.QAction(self.eventStatusGroup)
        statusAccepted.setText(
            _translate("uQuakeWaveUI", "Accepted", None))
        statusAccepted.setCheckable(True)
        statusRejected = QtGui.QAction(self.eventStatusGroup)
        statusRejected.setText(
            _translate("uQuakeWaveUI", "Rejected", None))
        statusRejected.setCheckable(True)

        self.connect(self.eventStatusGroup, QtCore.SIGNAL("selected(QAction *)"), self.changeEventStatus)
        self.baseUI.mainToolBar.addActions(self.eventStatusGroup.actions())
        self.baseUI.mainToolBar.addSeparator()

        # ---- Pick Mode toolbar ----

        self.pickModeGroup = QtGui.QActionGroup(self.baseUI.mainToolBar)

        modeNone = QtGui.QAction(self.pickModeGroup)
        modeNone.setText(_translate("uQuakeWaveUI", "None", None))
        modeNone.setCheckable(True)
        modeNone.setChecked(True)
        modeP = QtGui.QAction(self.pickModeGroup)
        modeP.setText(_translate("uQuakeWaveUI", "P", None))
        modeP.setCheckable(True)
        modeS = QtGui.QAction(self.pickModeGroup)
        modeS.setText(_translate("uQuakeWaveUI", "S", None))
        modeS.setCheckable(True)
        modeE = QtGui.QAction(self.pickModeGroup)
        modeE.setText(_translate("uQuakeWaveUI", "E", None))
        modeE.setCheckable(True)
        modeR = QtGui.QAction(self.pickModeGroup)
        modeR.setText(_translate("uQuakeWaveUI", "R", None))
        modeR.setCheckable(True)

        self.connect(self.pickModeGroup, QtCore.SIGNAL("selected(QAction *)"), self.changePickMode)
        self.baseUI.mainToolBar.addActions(self.pickModeGroup.actions())
        self.baseUI.mainToolBar.addSeparator()

        # ---- Filter toolbar ----

        self.filterToggle = QtGui.QAction(self.baseUI.mainToolBar)
        self.filterToggle.setText(
            _translate("uQuakeWaveUI", "&Filter", None))
        self.filterToggle.setCheckable(True)
        self.connect(self.filterToggle, QtCore.SIGNAL("toggled(bool)"), self.toggleFilter)
        self.baseUI.mainToolBar.addAction(self.filterToggle)

        self.LFLabel = QtGui.QLabel(self.baseUI.mainToolBar)
        self.LFLabel.setText('LF :')
        self.baseUI.mainToolBar.addWidget(self.LFLabel)

        self.LFText = QtGui.QLineEdit(self.baseUI.mainToolBar)
        self.LFText.setText('10.0')
        self.connect(self.LFText, QtCore.SIGNAL("returnPressed()"), self.toggleFilter)
        self.baseUI.mainToolBar.addWidget(self.LFText)

        self.HFLabel = QtGui.QLabel(self.baseUI.mainToolBar)
        self.HFLabel.setText('HF :')
        self.baseUI.mainToolBar.addWidget(self.HFLabel)

        self.HFText = QtGui.QLineEdit(self.baseUI.mainToolBar)
        self.HFText.setText('1000.0')
        self.connect(self.HFText, QtCore.SIGNAL("returnPressed()"), self.toggleFilter)
        self.baseUI.mainToolBar.addWidget(self.HFText)

        # ---- NLL toolbar ----

        self.baseUI.mainToolBar.addSeparator()
        self.butNLL = QtGui.QAction(self.baseUI.mainToolBar)
        self.butNLL.setText(
            _translate("uQuakeWaveUI", "&Locate with NonLinLoc", None))
        self.connect(self.butNLL, QtCore.SIGNAL("triggered()"), self.runNLL)
        self.baseUI.mainToolBar.addAction(self.butNLL)

        # ---- View toolbar ----
        self.connect(self.baseUI.actionZoomIn, QtCore.SIGNAL("triggered()"), self.zoomIn)
        self.connect(self.baseUI.actionZoomOut, QtCore.SIGNAL("triggered()"), self.zoomOut)


        # ---- Toolbox plot ----
        verticalLayout = QtGui.QVBoxLayout(self.baseUI.stereoPlotFrame)
        verticalLayout.setMargin(0)

        self.stereoPlot = StereoPlot(self, True)
        verticalLayout.addWidget(self.stereoPlot)


        # ---- Main layout ----

        verticalLayout = QtGui.QVBoxLayout(self.scrollContents)
        verticalLayout.setMargin(0)

        self.fig = SeisFigure(self, True)
        verticalLayout.addWidget(self.fig)

        self.connect(self.baseUI.seisTree, QtCore.SIGNAL("itemDoubleClicked(QTreeWidgetItem *,int)"), self.changedSeis)
        self.baseUI.seisTree.setItemDelegate(TreeItemDelegate(self.baseUI.seisTree))

        self.connect(self.baseUI.eventTree, QtCore.SIGNAL("itemDoubleClicked(QTreeWidgetItem *,int)"), self.changedOrigin)

        # Create a progress bar as a floating widget
        self.loadingFrame = QtGui.QWidget(None, Qt.FramelessWindowHint)
        self.loadingFrame.setFixedSize(500, 60)
        self.loadingFrame.setWindowModality(Qt.WindowModal)
        frameLayout = QtGui.QVBoxLayout(self.loadingFrame)
        self.progressMessage = QtGui.QLabel(self.loadingFrame)
        self.progressMessage.setAlignment(Qt.AlignCenter)
        self.progressBar = QtGui.QProgressBar(self.loadingFrame)
        self.progressBar.setRange(0, 1)
        frameLayout.addWidget(self.progressMessage)
        frameLayout.addWidget(self.progressBar)

        self._longTask = TaskThread()
        self._longTask.taskFinished.connect(self.taskFinished)

    def taskStart(self, message):

        pos_x = (self.width() - self.loadingFrame.width()) / 2
        pos_y = (self.height() - self.loadingFrame.height()) / 2
        self.loadingFrame.move(pos_x, pos_y)
        self.setEnabled(False)
        self.progressBar.setRange(0, 0)
        self.progressMessage.setText(message)
        self.loadingFrame.show()
        self._longTask.start()

    def taskFinished(self):

        self.setEnabled(True)
        self.loadingFrame.hide()
        self.progressBar.setRange(0, 1)

    def keyPressEvent(self, event):

        for k, v in self.shortcuts.items():
            if not event.key() == v:
                continue

            if 'pick' in k:
                if self.mode == k:
                    selMode = 'none'

                elif 'None' in k:
                    selMode = 'None'
                else:
                    selMode = k.split('_')[1][0]

                for a in self.pickModeGroup.actions():

                    if selMode == str(a.text()).lower():
                        a.setChecked(True)
                        self.changePickMode(a)
                        break

                    elif selMode == str(a.text()):
                        a.setChecked(True)
                        self.changePickMode(a)
                        break

            elif 'type' in k:
                selType = k.split('_')[1]
                for a in self.eventType.actions():
                    if a.text() == selType:
                        a.setChecked(True)
                        self.changeEventType(a)
                        break
            elif 'accept_reject' in k:
                for a in self.eventStatusGroup.actions():
                    a.setChecked(True)
                    self.changeEventStatus(a)
                    break
            break

        QtGui.QMainWindow.keyPressEvent(self, event)

    def read_settings(self, control_file):

        # TODO: read default path from .settings file
        # path = os.getcwd()
        path = control_file
        # path = '/media/raid/Projects/2013/Northparkes_TBM/05 Data analysis/04_AutomaticProcessing/02_DBImport/TBS_QML_bak.sqlite'
        # path = 'mysql://apascu:Marvin123@10.47.7.105/NckelRim_QuakeSQL?unix_socket=/tmp/mysql.sock'

        return path

    def select_data_source(self):

        # TODO: open data source picker dialog
        return self.read_settings()

    def connect_data_source(self, data_source=None):

        if not data_source:
            data_source = self.select_data_source()

        if not data_source:
            return

        source_type = 'Hybrid'
        # if source_type == 'DB':
        #     self.model = DBWaveModel(data_source)
        # elif source_type == 'QuakeML':
        #     self.model = QuakeMLWaveModel(data_source)
        if source_type == 'Hybrid':
            self.model = HybridWaveModel(data_source)
        else:
            raise NotImplementedError

        if self.model.dataDict:
            _ = self.initSeisTree()
            self.changedSeisIndex(0)  # triggers loadData() with first tree item data path

    def loadData(self, dataPath):

        self._longTask.init(self.model.load_stream, self.data_available, dataPath)
        self.taskStart('Reading data - %s' % dataPath)

    def data_available(self):

        self.initOriginTree()

        self.fig.setDataModel(self.model, dist=self.model.dist)
        self.stereoPlot.setDataModel(self.model)
        self.updateEventLabel()

    def searchValidSeis(self, forward=True):

        if forward:
            searchRange = range(self.curDataIndex + 1, len(self.ordered_files))
        else:
            searchRange = range(self.curDataIndex - 1, -1, -1)

        found = False
        for index in searchRange:
            fle = self.ordered_files[index]
            if self.baseUI.actionShowBoth.isChecked() or self.model.dataDict[fle]['eventStatus']:
                found = True
                break
        if not found:
            return -1

        return index

    def prevSeis(self):

        searchSeisIndex = self.searchValidSeis(False)
        if searchSeisIndex == -1:
            return

        self.changedSeisIndex(searchSeisIndex)

    def nextSeis(self):

        searchSeisIndex = self.searchValidSeis(True)
        if searchSeisIndex == -1:
            return

        self.changedSeisIndex(searchSeisIndex)

    def cloneOrigin(self):

        new_orig_id = self.model.cloneOrigin()

        # if len(self.model.eventDict) == 1:
        #     self.butDeleteOrigin.setEnabled(True)

        self.model.changePreferredOrigin(new_orig_id)
        self.data_available()

    def deleteOrigin(self):

        if len(self.model.eventDict) == 1:
            return

        new_orig_id = self.model.deleteOrigin()

        # if len(self.model.eventDict) == 1:
        #     self.butDeleteOrigin.setEnabled(False)

        self.model.changePreferredOrigin(new_orig_id)
        self.data_available()

    def changedSeis(self, item, index):

        if not item.text(1):
            return

        fle = str(item.text(1))
        eventID = self.ordered_files.index(fle)
        self.changedSeisIndex(eventID)

    def changedSeisIndex(self, index):

        ok = self.commitModifications()
        if not ok:
            QtGui.QMessageBox.critical(self, 'Database error',
                                       'Unable to commit modifications to current event. Please ensure the database is accessible and that the data hasn\'t changed.')
            return

        self.curDataIndex = index
        self.curDataFile = self.ordered_files[index]

        self.loadData(self.curDataFile)

    def changedOrigin(self, item, index):

        if not item.text(1):
            return

        orig_id = str(item.text(1))

        self.model.changePreferredOrigin(orig_id)
        self.data_available()

    def runNLL(self):
        event = self.nll.run_event(self.model.current_event,
                             status='preliminary')
        print event
        origin = event.origin
        # TODO update the current object to account for the new origin and
        # arrival
        self.model.eventDict.origin = event.origin.loc
        self.model.eventDict.evaluationStatus = event

       # 'origin': [10233.0, 10284.3, 2867.88], 'evaluationStatus':
        # u'preliminary', 'origin_id': ResourceIdentifier(id="smi:local/1b41e4c6-0fc8-422f-88de-6b7a7e7a6621"), 'eventType': 'not reported', 'uncertainty': 9.666158, 'evaluationMode': u'automatic', 'magnitude_type': 'Mw', 'magnitude_mag': 1.21973808782}, {'origin': None, 'evaluationStatus': u'preliminary', 'origin_id': ResourceIdentifier(id="smi:local/e97aeb8c-b501-44da-8658-8fe92ff17adc"), 'eventType': 'not reported', 'uncertainty': None, 'evaluationMode': u'manual', 'magnitude_type': 'Mw', 'magnitude_mag': 1.21973808782}

        logger.info("Running NLL")

    def toggleFilter(self, state, draw=True):

        f = self.filterToggle.isChecked()

        self.LFLabel.setEnabled(f)
        self.LFText.setEnabled(f)
        self.HFLabel.setEnabled(f)
        self.HFText.setEnabled(f)

        '''
		Apply bandpass filter
		'''

        if self.model:
            self.model.is_filtered = f
            if f:
                lf = float(self.LFText.text())
                hf = float(self.HFText.text())
                self.model.filtered_stream = self.model.composite_stream.copy()
                self.model.filtered_stream.filter('bandpass',
                                                  freqmin=lf,
                                                  freqmax=hf,  # corners=self.bpfilter[_i]['corners']
                                                  )
            if draw:
                self.fig.setDataModel(self.model, dist=self.model.dist)

        self.updateSB('Filter toggled %s' % ('ON' if f else 'OFF'))

    def toggleEventVisibility(self):

        self.initSeisTree()
        if not self.model.curDataDict()['eventStatus']:
            self.nextSeis()

    def zoomIn(self):

        self.fig.plotHeight += 20
        self.fig.drawFig()

    def zoomOut(self):

        if self.fig.plotHeight > 40:
            self.fig.plotHeight -= 20
            self.fig.drawFig()

    def _build_tree_rec(self, node, parent_item, depth, top_level_items, full_key):

        for key, val in sorted(node.iteritems()):

            comp_key = '%s/%s' % (full_key, key)

            if not val:
                # this must be a file
                state = 0
                try:
                    data = self.model.dataDict[comp_key]
                except:
                    continue

                # if not data['eventStatus']:
                #     state = 1
                # elif data['hasManualPicks']:
                #     state = 2

                # cols = ["Event Time", "Data File", "Error", "State"]
                fname = os.path.splitext(key)[0]
                t4 = TreeWidgetItem(parent_item,
                                    [fname, comp_key, str(data['error']), str(state)])
                self._update_tree_item_text_color(t4, data)
                parent_item.addChild(t4)
            else:
                # this must be a directory - recurse
                if not parent_item:
                    tree_item = TreeWidgetItem([key])
                else:
                    tree_item = TreeWidgetItem(parent_item, [key])
                if depth == 0:
                    top_level_items.append(tree_item)
                tree_item.setExpanded(True)

                # if depth == 0:
                #     comp_key = full_key
                if depth == 0:
                    comp_key = key

                self._build_tree_rec(val, tree_item, depth + 1, top_level_items, comp_key)

    def _update_tree_item_text_color(self, item, data):

        if not data['eventStatus']:
            item.setTextColor(0, Qt.red)
        elif data['hasManualPicks']:
            item.setTextColor(0, Qt.green)
        else:
            item.setTextColor(0, Qt.black)

    def build_file_tree(self):

        data = self.model.file_tree
        items = []

        # base_dir = '/'.join(self.model.base_dir.split('/')[:-1])
        base_dir = '.'
        self._build_tree_rec(data, None, 0, items, base_dir)

        return items

    def build_origin_tree(self):

        data = self.model.eventDict
        items = []
        root_item = TreeWidgetItem(['Event 1'])
        for i, dat in enumerate(data):

            active = (i == self.model.preferredOriginIndex)
            readOnly = (dat['origin'] is not None)

            node_dat = ['Origin %d' % i, dat['origin_id'], active, readOnly, dat['uncertainty']]
            node_dat = [str(d) for d in node_dat]
            node = TreeWidgetItem(root_item, node_dat)

            if active:
                labels = ['Location', 'Uncertainty', 'Magnitude']
                try:
                    mag_data = "%s = %0.1f" % (dat["magnitude_type"], dat["magnitude_mag"])
                except:
                    mag_data = ""
                meta = [dat['origin'], dat['uncertainty'], mag_data]
                for l, m in zip(labels, meta):
                    node2 = TreeWidgetItem(node, [l, "", str(m)])
                    node2.setFlags(node2.flags() & ~Qt.ItemIsSelectable)
                    # node2.setFlags(node2.flags() & ~Qt.ItemIsSelectable | ~Qt.ItemIsUserCheckable | ~Qt.ItemIsEnabled)
                    node.addChild(node2)

            root_item.addChild(node)

        items.append(root_item)
        return items

    def _iterate_ordered_tree(self):

        self.ordered_files = []
        it = QtGui.QTreeWidgetItemIterator(self.baseUI.seisTree)
        while it.value():
            if it.value().text(1):
                self.ordered_files.append(str(it.value().text(1)))
            it += 1

    def initSeisTree(self):

        self.baseUI.seisTree.setSortingEnabled(False)
        self.baseUI.seisTree.clear()

        items = self.build_file_tree()

        self.baseUI.seisTree.addTopLevelItems(items)
        self.baseUI.seisTree.expandAll()
        self.baseUI.seisTree.hideColumn(1)
        # self.baseUI.seisTree.hideColumn(3)
        self.baseUI.seisTree.resizeColumnToContents(0)
        self.baseUI.seisTree.resizeColumnToContents(2)
        self.baseUI.seisTree.resizeColumnToContents(3)
        # self.baseUI.seisTree.setSortingEnabled(True)

        # TODO: Subclass QAbstractItemModel and call seisTree.setModel()
        # TODO: Move data storage for tree purposes to derived class
        # TODO: Reimplement QAbstractItemModel.sort() to allow sorting by columns
        # TODO: Remove this function.
        self._iterate_ordered_tree()

        return

    def updateSeisTree(self):

        filename = self.curDataFile.split()[-1]
        currentItem = self.baseUI.seisTree.findItems(filename, Qt.MatchExactly | Qt.MatchRecursive, 1)

        if len(currentItem) == 0:
            return

        currentItem = currentItem[0]

        self.baseUI.seisTree.scrollToItem(currentItem)
        self.baseUI.seisTree.clearSelection()
        self.baseUI.seisTree.setItemSelected(currentItem, True)

    def updateEventLabel(self):
        """This is called only after loading a new set of data"""

        self.seisCountLabel.setText('(<b>%d/%d</b>)' % (self.curDataIndex + 1, len(self.ordered_files)))
        self.updateSeisTree()

        cur_data = self.model.curDataDict()
        if not cur_data:
            return

        curType = cur_data['eventType']
        for a in self.eventType.actions():
            if a.text() == self.model.types[curType]:
                a.setChecked(True)
                break

        curStatus = cur_data['eventStatus']
        for a in self.eventStatusGroup.actions():
            if (a.text() == 'Rejected') ^ curStatus:
                a.setChecked(True)
                break

    def initOriginTree(self):

        self.baseUI.eventTree.setSortingEnabled(False)
        self.baseUI.eventTree.clear()

        items = self.build_origin_tree()

        self.baseUI.eventTree.addTopLevelItems(items)
        self.baseUI.eventTree.expandAll()
        self.baseUI.eventTree.hideColumn(1)
        self.baseUI.eventTree.resizeColumnToContents(0)
        self.baseUI.eventTree.resizeColumnToContents(2)
        # self.baseUI.seisTree.setSortingEnabled(True)

        self.butDeleteOrigin.setEnabled(len(self.model.eventDict) > 1)

        # TODO: Subclass QAbstractItemModel and call eventTree.setModel()
        # TODO: Move data storage for tree purposes to derived class
        # TODO: Reimplement QAbstractItemModel.sort() to allow sorting by columns
        # TODO: Remove this function.

    def changeEventType(self, action):

        self.model.modifiedEventType = str(action.text())
        self.model.curEventDict()['eventType'] = str(action.text()).lower()
        self.notifyModifications()

    def changeEventStatus(self, action):

        self.model.modifiedStatus = str(action.text())
        self.model.curEventDict()['evaluationStatus'] = str(action.text()).lower()
        self.notifyModifications()

    def changePickMode(self, action):

        self.mode = str(action.text()).lower()

        if not self.mode == 'none':
            self.mode = 'pick_%s' % ('end' if self.mode == 'e' else ('remove' if self.mode == 'r' else self.mode))

        self.fig.changedMode(self.mode)

    def notifyModifications(self):

        self.baseUI.actionCommit.setEnabled(True)

    def commitModifications(self):

        ok = True
        if not self.model or not self.model.current_file:
            self.baseUI.actionCommit.setEnabled(False)
            return ok

        if not self.model.is_clean():
            ok = self.model.commitModifications()

        curData = self.model.curDataDict()
        filename = self.curDataFile.split('/')[-1]
        currentTreeItem = self.baseUI.seisTree.findItems(filename, Qt.MatchExactly | Qt.MatchRecursive, 1)

        if len(currentTreeItem) > 0:
            currentTreeItem = currentTreeItem[0]
            self._update_tree_item_text_color(currentTreeItem, curData)

        self.baseUI.actionCommit.setEnabled(False)
        return ok

    def addPicks(self, xdata, phase, station, polarity='undecideable'):

        self.model.addPicks(xdata, phase, station, polarity)
        self.notifyModifications()

    def delPicks(self, station):
        """
		Deletes pick from catalog
		"""
        self.model.delPicks(station)
        self.notifyModifications()

    def openStationPlot(self, station):

        if self.stationPlotUI is None:
            self.stationPlotUI = StationPlot(self, self.model)

        stationIndex = np.nonzero(self.model.station_names == station)[0][0]
        self.stationPlotUI.setData(stationIndex)
        self.stationPlotUI.show()

    def updateSB(self, text):
        self.baseUI.statusBar.showMessage(text)

    def cleanup(self):

        logger.debug('Cleaning up')

    def init_nlloc(self, control_file):
        from microquake.core import ctl
        params = ctl.parse_control_file(control_file)
        self.nll = init_nlloc_from_params(params)


def picker(control_file):
    """
	Gets executed when the program starts.
	"""

    # Create the GUI application
    qApp = QtGui.QApplication(sys.argv)

    uquakewave = uQuakeWave()
    qApp.processEvents()

    data_source = uquakewave.read_settings(control_file)
    uquakewave.connect_data_source(data_source)
    uquakewave.init_nlloc(control_file)

    qApp.connect(qApp, QtCore.SIGNAL("aboutToQuit()"), uquakewave.cleanup)
    os._exit(qApp.exec_())


import argparse
parser = argparse.ArgumentParser(description='Microquake picker program')
parser.add_argument('control_file', type=str,
                    help="XML configuration file")

if __name__ == '__main__':
    args = parser.parse_args()
    control_file = args.control_file
    picker(control_file)
