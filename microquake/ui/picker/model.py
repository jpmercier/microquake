import os
import numpy as np
from datetime import datetime
from microquake.core import UTCDateTime
from microquake.core import logger
from microquake.core import Trace, Stream, event, ctl, read
from microquake.core.stream import composite_traces, is_valid
from microquake.db import Project
from microquake.core.event import Catalog

from IPython.core.debugger import Tracer

class AbstractWaveModel(object):
    def __init__(self, source):

        self.base_dir = source
        self.current_file = None
        self.current_stream = None
        self.cleaned_stream = None
        self.composite_stream = None
        self.filtered_stream = None
        self.is_filtered = False
        self.use_q64 = False
        self.dataDict = {}
        self.eventDict = []
        self.preferredOriginIndex = None

        # TODO replace all the properties below event by current event
        self.current_event = None # current event object with preferred origin
        self.station_names = []
        self.station_locations = []
        self.station_locations_all = {}
        self.eventData = []
        self.picks = []
        self.dist = []
        self.eventStatus = []
        self.eventType = []

        self.angles = []

        self.onset_types = ['emergent', 'impulsive', 'questionable']
        self.types = {'not reported': 'U', 'earthquake': 'E',
                      'mining explosion': 'B', 'other event': 'M'}

        self.supported_ext = ['.sgy', '.mseed']

        self.modifiedStatus = None
        self.modifiedEventType = None
        self.modifiedPicks = []

    def curDataDict(self):

        if not self.current_file:
            return None

        return self.dataDict[self.current_file]

    def curEventDict(self):

        if self.preferredOriginIndex is None:
            return None

        return self.eventDict[self.preferredOriginIndex]

    def changePreferredOrigin(self, orig_id):

        # print orig_id
        index = [i for i, x in enumerate(self.eventDict) if str(x['origin_id']) == orig_id]
        # print index
        if not index:
            return

        self.preferredOriginIndex = index[0]
        self.sort_by_distance()
        self.clean_stream()
        self.composite_traces()

        # print self.preferredOriginIndex

    def cloneOrigin(self):

        from microquake.core.event import Pick, Arrival

        wave_id = self.curDataDict()['waveform_id']
        res = self.project.find_events_by_waveform_id(wave_id)
        base_file_name = res[0]['base_file_name']
        data_path = res[0]['data_path']
        # TODO this is not pretty need to be fixed
        cats = self.project.read_events(res)
        cat = cats[0]
        evt = cat.events[0]

        org = evt.preferred_origin()
        new_org = event.Origin()

        new_org.evaluation_mode = "manual"
        new_org.evaluation_status = "preliminary"

        arrivals = org.arrivals
        for a in arrivals:
            p = a.pick_id.get_referred_object()
            new_pick = Pick()
            for key in p.keys():
                new_pick[key] = p[key]
            new_arrival = Arrival()
            for key in a.keys():
                new_arrival[key] = a[key]
            new_arrival.pick_id = new_pick.resource_id.id
            new_org.arrivals.append(new_arrival)
            evt.picks.append(new_pick)
        evt.origins.append(new_org)
        evt.preferred_origin_id = new_org.resource_id

        self.project.update_event(cat, res, data_path, base_file_name)
        self.load_events()

        index = [str(x['origin_id']) for x in self.eventDict]
        # print index
        # print evt.preferred_origin_id

        return evt.preferred_origin_id

    def deleteOrigin(self):

        wave_id = self.curDataDict()['waveform_id']
        res = self.project.find_events_by_waveform_id(wave_id)
        base_file_name = res[0]['base_file_name']
        data_path = res[0]['data_path']
        cats = self.project.read_events(res)
        cat = cats[0]
        evt = cat.events[0]

        org = evt.preferred_origin()
        for a in org.arrivals:
            p = a.pick_id.get_referred_object()
            evt.picks.remove(p)

        # print [oo.resource_id for oo in evt.origins]
        evt.preferred_origin_id = evt.origins[-2].resource_id
        evt.origins.remove(org)

        self.project.update_event(cat, res, data_path, base_file_name)
        self.load_events()

        index = [str(x['origin_id']) for x in self.eventDict]
        # print index
        # print evt.preferred_origin_id

        return evt.preferred_origin_id

    def _read_stream(self, data_path):

        logger.debug("Reading stream %s" % data_path)

        if self.use_q64:

            from microquake.mapred import q64

            # assume data_path is a 5 second timestamp in format "2015_01_31 10:23:30"
            # search for a 1-hour Q64 file
            f = data_path.split()  # ["2015_01_31", "10:23:30"]
            ff = f[1].split(':')  # ["10", "23", "30"]
            filename = 'NickelRimData_%s_%s' % (f[0], ff[0])  # "NickelRimData_2015_01_31_10"  this is a Q64 for 1 hour
            key = '%s %s' % (f[0].replace('_', '/'), f[1])  # "2015/01/31 10:23:30"

            line = q64.find_stream(self.base_dir, filename, key)
            if not line:
                return

            data = line.split('\t')
            data2 = data[1]
            return q64.load_stream(data2)

        else:
            # use obspy to read data from path relative to base_dir
            try:
                fle = os.path.join(self.project.waveform_path, data_path)
                fle = os.path.abspath(fle)
                ext = os.path.splitext(fle)[1]
                if ext == '.sgy':

                    from microquake.mapred import qa

                    sensor_metadata = 'SensorInfo.txt'
                    metadata_fle = '%s/%s' % (self.base_dir, sensor_metadata)
                    st, inv = qa.load_segy(fle, metadata_fle)
                else:
                    st = read(fle)
            except Exception as e:
                logger.error(e)
                return None

            return st

    def load_stream(self, data_path):

        self.cleanup_data()

        self.current_stream = self._read_stream(data_path)
        if not self.current_stream:
            return

        # Finding max time
        min_time = UTCDateTime(datetime(2500, 1, 1))
        max_time = UTCDateTime(datetime(1900, 1, 1))

        for tr in self.current_stream:
            if tr.stats.starttime < min_time:
                min_time = tr.stats.starttime
            if tr.stats.endtime > max_time:
                max_time = tr.stats.endtime

        self.current_stream.detrend('demean')
        self.current_stream.detrend('linear')

        self.current_stream.trim(starttime=min_time, endtime=max_time,
                                 pad=True, fill_value=0)

        self.current_file = data_path

        self.current_stream.detrend('linear')
        self.current_stream.detrend('demean')
        self.station_names = self.current_stream.unique_stations()
        self.station_locations = []
        for name in self.station_names:
            if name in self.station_locations_all:
                self.station_locations.append(self.station_locations_all[name])
            else:
                self.station_locations.append([0, 0, 0])

        self.load_events()
        self.sort_by_distance()
        self.clean_stream()
        self.composite_traces()

    def load_events(self):
        raise NotImplementedError

    def clean_stream(self):

        self.cleanup_stream(self.cleaned_stream)  # release memory first
        self.cleaned_stream = self.current_stream.copy()  # deep copy first
        is_valid(self.cleaned_stream)  # in-place

    def composite_traces(self):
        from microquake.core.stream import composite_traces

        logger.debug('Composite traces')

        use_clean_only = True

        compositeTraces = []
        # for s in self.station_names:
        if use_clean_only:
            self.composite_stream = composite_traces(self.cleaned_stream)
        else:
            self.composite_stream = composite_traces(self.current_stream)


    def cleanup_data(self):

        self.cleanup_stream(self.current_stream)
        self.cleanup_stream(self.composite_stream)
        for p in self.picks:
            del p

    def cleanup_stream(self, st):

        if st is not None:
            for tr in st:
                del tr
            del st

    def is_clean(self):

        clean = (not self.modifiedEventType)
        clean &= (not self.modifiedPicks)
        clean &= (not self.modifiedStatus)

        return clean

    def commitModifications(self):
        raise NotImplementedError

    def getPicks(self, currentStation, onePerStation):
        """
        Create a list of picks for the current plot
        """
        this_st_picks = []
        for i, pick in enumerate(self.picks):
            if not hasattr(pick, 'waveform_id'):
                continue
            if (pick.waveform_id.station_code == currentStation or currentStation is None) and \
                                    self.current_stream[0].stats.starttime < \
                                    pick.time < self.current_stream[0].stats.endtime:

                # if (onePerStation and 'X' in pick.waveform_id.channel_code) or (not onePerStation):
                if (onePerStation) or (not onePerStation):
                    if not pick.waveform_id.channel_code:
                        pick.waveform_id.channel_code = 'X'
                    this_st_picks.append(i)

        return [self.picks[i] for i in this_st_picks]

    def getPickXPosition(self, picks):
        """
        Convert picktimes into relative positions along x-axis
        """
        xpicks = []
        for pick in picks:
            curTrace = self.current_stream.select(station=pick.waveform_id.station_code)[0]
            t = (pick.time - curTrace.stats.starttime)
            xpicks.append(t)

        return np.array(xpicks)

    def addPicks(self, xdata, phase, station, polarity='undecideable'):

        # Overwrite existing phase's picktime
        # TODO: support for multiple events per stream
        # TODO: add UI interface for adding, removing, selecting current event
        removedPicks = self.getPicks(station, onePerStation=False)[:]
        for pick in removedPicks:
            if pick.phase_hint == phase and pick.waveform_id.station_code == station:
                if pick.evaluation_mode == 'automatic':
                    self.modifiedPicks.append(['r', [station, pick.waveform_id.channel_code[-1], phase]])
                self.picks.remove(pick)

        self.setPick(xdata, phase=phase, station=station, channel='X', polarity=polarity)
        self.setPick(xdata, phase=phase, station=station, channel='Y', polarity=polarity)
        self.setPick(xdata, phase=phase, station=station, channel='Z', polarity=polarity)

    def setPick(self, xdata, phase, station, channel, polarity='undecideable'):

        """
        Write obspy.core.event.Pick into self._picks list
        """
        curTrace = self.current_stream.select(station=station)[0]
        picktime = curTrace.stats.starttime + \
                   xdata

        this_pick = event.Pick()

        creation_info = event.CreationInfo(
            author='ObsPy.Stream.pick()',
            creation_time=UTCDateTime())
        # Create new event.Pick()
        this_pick.time = picktime
        this_pick.phase_hint = phase
        this_pick.waveform_id = event.WaveformStreamID(
            network_code=curTrace.stats.network,
            station_code=station,
            location_code=curTrace.stats.location,
            channel_code=channel)
        this_pick.evaluation_mode = 'manual'
        this_pick.creation_info = creation_info
        # this_pick.onset = self.onset_types[self.onsetGrp.checkedId()]
        this_pick.onset = self.onset_types[0]
        this_pick.evaluation_status = 'preliminary'
        this_pick.polarity = polarity
        # if self._current_filter is not None:
        #    this_pick.comments.append(event.Comment(
        #                text=str(self.bpfilter[self.fltrcb.currentIndex()])))
        self.picks.append(this_pick)

        self.modifiedPicks.append(['a', [station, channel, phase, picktime, polarity]])

    def delPicks(self, station):
        """
        Deletes pick from catalog
        """
        for i, pick in enumerate(self.picks):
            if pick.waveform_id.station_code == station:
                self.picks.remove(pick)
                self.modifiedPicks.append(['r', [station, pick.waveform_id.channel_code, pick.phase_hint]])


class HybridWaveModel(AbstractWaveModel):
    def __init__(self, source):
        super(HybridWaveModel, self).__init__(source)
        from microquake.core import readStations

        num_waveforms = 0
        self.base_dir = os.path.dirname(source)
        self.file_tree = {}
        self.dataDict = {}
        self.use_q64 = False

        src_dir, src_flename = os.path.split(source)
        print source, src_dir, src_flename
        if src_dir != '':
            os.chdir(src_dir)

        params = ctl.parse_control_file(src_flename)
        Tracer()()
        self.project = Project(params.project)
        self.project.connect()
        self.site = params.sensors.site

        # self.station_locations_all = {key : value for (key, value) in zip(
        # params.sensors.name, params.sensors.pos)}

        waveforms = self.project.list_waveforms(start=0, limit=50)

        for wave in waveforms:
            seisfle = os.path.join(wave["data_path"], wave["base_file_name"])

            # events = self.project.list_events()

            dataDict = {}
            dataDict['eventID'] = num_waveforms
            dataDict['waveform_id'] = wave['_id']
            dataDict['eventStatus'] = True
            dataDict['hasManualPicks'] = False
            dataDict['eventType'] = 'not reported'
            dataDict['error'] = 0

            num_waveforms += 1

            # abs_file = os.path.join(root, f)
            self.dataDict[seisfle] = dataDict

        if not self.dataDict:
            logger.error('No data files found in the given folder!')
            return
        self.init_directory_structure()

    def init_directory_structure(self):
        """
        Creates a nested dictionary that represents the folder structure
        The list of files used is stored in the keys of self.dataDict
        This function produces a nest dict and stores it in self.file_tree
        """
        self.file_tree = {}
        rootdir = self.project.waveform_path.rstrip(os.sep)
        start = rootdir.rfind(os.sep) + 1
        # for path, dirs, files in os.walk(rootdir):
        for f in self.dataDict.iterkeys():
            fname = os.path.basename(f)
            path = os.path.dirname(f)
            folders = path[start:].split(os.sep)
            parent = self.file_tree
            for i, d in enumerate(folders):
                subdir = {}
                parent = reduce(dict.get, folders[:i], self.file_tree)
                parent[d] = subdir
            subdir = {fname: None}
            # parent = reduce(dict.get, folders[:-1], self.file_tree)
            parent[folders[-1]] = subdir

    def load_events(self):

        self.eventDict = []
        self.picks = []

        wave_id = self.curDataDict()['waveform_id']
        res = self.project.find_events_by_waveform_id(wave_id)
        events = self.project.read_events(res)

        if not events:

            dct = {}
            dct['origin_id'] = None
            dct['origin'] = []
            dct['eventType'] = 'not reported'
            dct['evaluationMode'] = 'manual'
            dct['evaluationStatus'] = 'preliminary'
            dct['uncertainty'] = None
            self.eventDict.append(dct)
            self.current_event = None
            return
        else:
            # TODO: add support for multiple events in XML catalog
            # TODO this funtion is a mess an need to be considerable improved
            evt = events[0]
            self.current_event = evt
            if evt.origins:

                pref_origin = evt.preferred_origin()
                self.preferredOriginIndex = None

                for i, org in enumerate(evt.origins):

                    dct = {}

                    if org.resource_id == pref_origin.resource_id:
                        self.preferredOriginIndex = i

                    dct['origin_id'] = org.resource_id
                    if org.x and org.y and org.z:
                        dct['origin'] = [org.x, org.y, org.z]
                    else:
                        dct['origin'] = None
                    dct['evaluationMode'] = org.evaluation_mode
                    dct['evaluationStatus'] = org.evaluation_status

                    if hasattr(org, 'origin_uncertainty') and \
                        hasattr(org.origin_uncertainty, 'confidence_ellipsoid'):
                        dct['uncertainty'] = org.origin_uncertainty.confidence_ellipsoid.semi_major_axis_length
                    else:
                        dct['uncertainty'] = None

                    dct['eventType'] = getattr(org, 'event_type', 'not reported')

                    magnitude = evt.preferred_magnitude()
                    if magnitude is not None:
                        dct['magnitude_type'] = magnitude.magnitude_type
                        dct['magnitude_mag'] = magnitude.mag
                    else:
                        dct['magnitude_type'] = None
                        dct['magnitude_mag'] = None

                    self.eventDict.append(dct)

            arrivals = evt.preferred_origin().arrivals

            if not arrivals:
                self.picks = evt.picks
            else:

                self.angles = []
                self.picks = []
                for ar in arrivals:
                    self.angles.append((ar.azimuth, ar.takeoff_angle))

                    # TODO: fix bug with get_referred_object(), when reloading the same event
                    self.picks.append(ar.pick_id.get_referred_object())

    def sort_by_distance(self):
        """
        sort the station_names list by distance from the event origin
        :return:
        """

        self.dist = None

        if not self.current_event:
            return

        eloc = self.current_event.preferred_origin().loc
        if not eloc:
            return

        self.dist = []
        traces = []
        for sta in self.current_stream.unique_stations():
            try:
                station = self.site.select(station=sta).stations()[0]
            except:
                continue
            sloc = station.loc
            self.dist.append(np.linalg.norm(sloc - eloc))

        sort_indices = np.argsort(self.dist)
        self.dist = np.array(self.dist)
        self.dist = self.dist[sort_indices]
        self.station_names = np.array(self.station_names)
        self.station_names = self.station_names[sort_indices]

    def commitModifications(self):

        logger.debug('Committing modifications')

        wave_id = self.curDataDict()['waveform_id']
        res = self.project.find_events_by_waveform_id(wave_id)
        if not res:
            logger.error("Event not found")
            return False

        base_file_name = res[0]['base_file_name']
        data_path = res[0]['data_path']

        cat = Catalog()
        origin = event.Origin()

        evt = event.Event(picks=self.picks, origins=[origin])
        event_type = self.curEventDict()['eventType']
        # event_type = self.curEventDict()['eventType']  # 'u', 'e', 'b', 'm'
        # event_type_index = self.types.values().index(event_type.upper())
        # event_type = self.types.keys()[event_type_index]

        if self.curEventDict()['evaluationStatus'].lower() == 'rejected':
            origin.evaluation_status = 'rejected'
        else:
            origin.evaluation_status = 'preliminary'
        origin.event_type = event_type
        # origin.evaluation_mode = self.curEventDict()['evaluationMode']
        origin.evaluation_mode = 'manual'

        cat.events.append(evt)
        self.project.update_event(cat, res, data_path, base_file_name)

        self.modifiedEventType = None
        self.modifiedPicks = []
        self.modifiedStatus = None

        return True


#
# class DBWaveModel(AbstractWaveModel):
#     def __init__(self, source):
#         super(DBWaveModel, self).__init__(source)
#
#         try:
#             self.db = DBSession(source, echo=False)
#             session = self.db.session
#
#             logger.debug('Reading events')
#             # cursor = self.db.execute("SELECT e.id, e.event_status, COUNT(p.event_id)>0 as has_picks, data_file, semi_major_axis_length, SUBSTR(data_file, 103, 10) as date " +\
#             #                              "FROM event e, seismogram s, error_ellipsoid r LEFT OUTER JOIN " +\
#             #                              "(SELECT p.event_id FROM pick p WHERE p.evaluation_mode = 'manual') p " +\
#             #                              "ON p.event_id=e.id WHERE e.id = s.event_id AND e.id = r.event_id " +\
#             #                              "GROUP BY e.id, e.event_status, data_file, semi_major_axis_length " +\
#             #                              "ORDER BY date, semi_major_axis_length")
#
#             t = session.query(Origin.event_id).group_by(Origin.event_id).having(func.count(Origin.id) == 1).subquery('t')
#
#             results = session.query(Event.id, Origin.id, Origin.evaluationStatus, EventDescription.text,
#                                     ConfidenceEllipsoid.semiMajorAxisLength) \
#                 .join(Origin).join(EventDescription).join(OriginUncertainty).join(ConfidenceEllipsoid) \
#                 .filter(and_(
#                 ConfidenceEllipsoid.semiMajorAxisLength < 35,
#                 # Origin.time.between(UTCDateTime('2013-06-19 15:00:00'), UTCDateTime('2013-06-19 16:00:00')),
#                 or_(Origin.event_id.in_(t),
#                     Origin.evaluationStatus == 'confirmed'
#                     )
#             )).all()
#
#             results = np.array(results)
#
#             self.dataDict = {}
#             for r in results:
#                 dataFile = r[3].replace('/', '_')
#
#                 dataDict = {}
#                 dataDict['eventID'] = int(r[0])
#                 dataDict['originID'] = int(r[1])
#                 Tracer()()
#                 dataDict['eventStatus'] = r[2] == 'preliminary'
#                 dataDict['hasManualPicks'] = True
#                 dataDict['errors'] = r[4]
#
#                 self.dataDict[dataFile] = dataDict
#
#             if len(self.dataDict) <= 0:
#                 raise Exception()
#
#             logger.debug('Reading station data')
#             self.station_names = []
#
#             cursor1 = session.query(Station.id, Station.code, Station.X, Station.Y, Station.Z).all()
#             for r1 in cursor1:
#                 station_id = int(r1[0])
#
#                 cursor2 = session.query(Channel.code, Channel.X, Channel.Y, Channel.Z).filter(
#                     Channel.station_id == station_id).all()
#                 cData = []
#                 for r2 in cursor2:
#                     cData.append(r2)
#
#                 stationRow = list(r1)
#                 stationRow.append(cData)
#                 self.station_names.append(stationRow)
#
#             self.station_names = np.array(self.station_names, dtype='object')
#
#         except Exception as e:
#             logger.debug('oh no! %s ' % e)
#             pass
#         finally:
#             pass
#
#     def load_events(self):
#
#         session = self.db.session
#
#         self.eventData = session.query(Origin.X, Origin.Y, Origin.Z, Event.type_, Origin.evaluationMode,
#                                        Origin.evaluationStatus).join(Event).filter(
#             Origin.event_id == self.curEventID
#         ).all()
#
#         if len(self.eventData) == 1:
#             self.eventData = self.eventData[0]
#         else:
#             for x in self.eventData:
#                 if x[-1] == 'confirmed':
#                     self.eventData = x
#                     break
#
#         self.picks = []
#         # cursor = self.db.execute("SELECT p.*, st.name, c.component FROM pick p, seismogram se, component c, station st WHERE p.seismogram_id = se.id AND p.component_id = c.id AND c.station_id = st.id AND se.event_id = ?", [self.curEventID])
#         cursor = session.query(Pick, Station.code, Channel.code).join(Station).join(Channel).filter(
#             Pick.event_id == self.curEventID
#         ).all()
#
#         for r in cursor:
#             pick = r[0]
#             phase = pick.phaseHint
#             picktime = pick.time
#             polarity = pick.polarity
#             evaluation_mode = pick.evaluationMode
#             evaluation_status = pick.evaluationStatus
#             onset = pick.onset
#             station = r[1]
#             channel = r[2]
#             network = 'TB'
#             location = 'NPM'
#
#             this_pick = event.Pick()
#
#             creation_info = event.CreationInfo(
#                 author='ObsPy.Stream.pick()',
#                 creation_time=UTCDateTime())
#             # Create new event.Pick()
#             this_pick.time = picktime
#             this_pick.phase_hint = phase
#             this_pick.waveform_id = event.WaveformStreamID(
#                 network_code=network,
#                 station_code=station,
#                 location_code=location,
#                 channel_code=channel)
#             this_pick.evaluation_mode = evaluation_mode
#             this_pick.creation_info = creation_info
#             this_pick.onset = onset
#             this_pick.evaluation_status = evaluation_status
#             this_pick.polarity = polarity
#
#             self.picks.append(this_pick)
#
#     def sort_by_distance(self):
#
#         # self.sensorsList = []
#         # for tr in self.currentStream:
#         #    self.sensorsList.append(tr.stats.station)
#
#         # self.sensorsList = np.unique(self.sensorsList)
#
#         eventLocation = np.array(self.eventData[0:3])
#         self.dist = []
#         for s in self.station_names:
#             sPos = np.array(s[2:5])
#             self.dist.append(np.linalg.norm(sPos - np.array(eventLocation)))
#         self.dist = np.array(self.dist)
#
#         sortIndices = np.argsort(self.dist)
#         self.dist = self.dist[sortIndices]
#         self.station_names = self.station_names[sortIndices]
#
#     def composite_traces(self):
#
#         compositeTraces = []
#         for s in self.station_names:
#             name = s[1]
#             st2 = self.current_stream.select(station=name)
#             if len(st2.traces) == 0:
#                 logger.warning('station %s has no data' % name)
#                 continue
#
#             data = 0
#
#             # Minimizing the risk to concatenate a trace containing noise
#             maxEnerg = 0
#             imaxEnerg = 0
#             for i, tr2 in enumerate(st2):
#                 if np.var(tr2.data) > maxEnerg:
#                     maxEnerg = np.var(tr2.data)
#                     imaxEnerg = i
#
#             for tr2 in st2:
#                 if np.var(tr2.data) > maxEnerg / 10:
#                     data += tr2.data ** 2
#
#             data = np.sqrt(data) * np.sign(st2[imaxEnerg])
#
#             newtr = Trace(data=data, header=st2.traces[0].stats)
#             newtr.stats.channel = 'CMP'
#             compositeTraces.append(newtr)
#             self.cleanup_stream(st2)
#
#         self.compositeStream = Stream(traces=compositeTraces)
#
#     def commitModifications(self):
#
#         logger.debug('Committing modifications')
#
#         curEventID = self.curDataDict()['eventID']
#
#         # self.modifiedEventType
#         if self.modifiedEventType is not None:
#             curType = ''
#             for k, v in self.types.items():
#                 if v == self.modifiedEventType:
#                     curType = k
#                     break
#
#             try:
#                 self.db.execute("UPDATE event SET event_type = ? WHERE id = ?", [curType, curEventID])
#             except Exception as e:
#                 logger.debug('oh no 1! %s ' % e)
#                 return False
#
#         # self.modifiedPicks
#         for c in self.modifiedPicks:
#             op = c[0]
#             pickData = c[1]
#
#             if op == 'r':
#                 station_name = pickData[0]
#                 component = pickData[1]
#                 pick_type = pickData[2]
#                 try:
#                     self.db.execute("DELETE FROM pick WHERE id IN (SELECT p.id FROM pick p, component c, station s " + \
#                                     "WHERE p.component_id = c.id and c.station_id = s.id " + \
#                                     "AND c.component = ? AND p.event_id = ? AND s.name = ? AND p.type = ?)",
#                                     [component, curEventID, station_name, pick_type])
#                 except Exception as e:
#                     logger.debug('oh no 2! %s ' % e)
#
#             else:
#                 station_name = pickData[0]
#                 component = pickData[1]
#                 pick_type = pickData[2]
#                 pick_time = pickData[3].strftime('%Y/%m/%d %H:%M:%S %f')
#                 polarity = pickData[4]
#                 try:
#                     cursor = self.db.execute("SELECT p.id FROM pick p, component c, station s " + \
#                                              "WHERE p.component_id = c.id and c.station_id = s.id " + \
#                                              "AND c.component = ? AND p.event_id = ? AND s.name = ? AND p.type = ? " + \
#                                              "AND p.evaluation_mode = ?",
#                                              [component, curEventID, station_name, pick_type, 'manual'])
#                     r = cursor.fetchone()
#                     if r is None:
#                         logger.debug('Pick not in DB')
#                         try:
#                             cursor = self.db.execute("SELECT se.id, c.id FROM component c, seismogram se, station s " + \
#                                                      "WHERE c.station_id = s.id " + \
#                                                      "AND s.name = ? AND c.component = ? AND se.data_file = ? ",
#                                                      [station_name, component, self.current_file])
#                             r = cursor.fetchone()
#                             if not r is None:
#                                 logger.debug('IDs found')
#                                 seismogram_id = r[0]
#                                 component_id = r[1]
#
#                                 vals = [seismogram_id, curEventID, component_id, pick_type, pick_time, polarity,
#                                         'manual', 'preliminary']
#                                 self.db.execute(
#                                     "INSERT INTO pick (seismogram_id, event_id, component_id, type, date, polarity, evaluation_mode, evaluation_status) " + \
#                                     "VALUES(?, ?, ?, ?, ?, ?, ?, ?)", vals)
#
#                         except Exception as e:
#                             logger.debug('oh no 3! %s ' % e)
#                     else:
#                         pick_id = r[0]
#                         logger.debug('Pick in DB: %s' % pick_id)
#                         try:
#                             self.db.execute("UPDATE pick SET date = ? WHERE id = ?", [pick_time, pick_id])
#
#                         except Exception as e:
#                             logger.debug('oh no 4! %s ' % e)
#
#                 except Exception as e:
#                     logger.debug('oh no 5! %s ' % e)
#
#         # self.modifiedStatus
#         if self.modifiedStatus is not None:
#             self.curDataDict()['eventStatus'] = (self.modifiedStatus == 'confirmed')
#
#         # self.modifiedStatus
#         if self.modifiedStatus is not None:
#             try:
#                 self.db.execute("UPDATE event SET event_status = ?, evaluation_mode = ? WHERE id = ?",
#                                 [self.modifiedStatus.lower(), 'manual', self.curEventID])
#             except Exception as e:
#                 logger.debug('oh no! %s ' % e)
#                 return False
#
#         self.modifiedEventType = None
#         self.modifiedPicks = []
#         self.modifiedStatus = None
#
#         return True
#
#
# class QuakeMLWaveModel(AbstractWaveModel):
#     def __init__(self, source):
#         super(QuakeMLWaveModel, self).__init__(source)
#
#         num_events = 0
#         self.file_tree = {}
#         self.dataDict = {}
#         source = source.rstrip(os.sep)
#         start = source.rfind(os.sep) + 1
#         for root, dirs, files in os.walk(source):
#             files = [f for f in files if os.path.splitext(f)[1] in self.supported_ext]
#             folders = root[start:].split(os.sep)
#             subdir = dict.fromkeys(files)
#             parent = reduce(dict.get, folders[:-1], self.file_tree)
#             parent[folders[-1]] = subdir
#             for f in files:
#
#                 relDir = os.path.relpath(root, source)
#                 seisfle = os.path.join(relDir, f)
#                 quakeMLfle = '%s.xml' % os.path.splitext(seisfle)[0]
#                 quakeMLfle = os.path.join(source, quakeMLfle)
#
#                 originID = 0
#                 eventStatus = 'not_picked'
#                 hasManualPicks = False
#                 eventType = 'not reported'
#                 error = None
#                 if os.path.exists(quakeMLfle):
#                     cat = obevent.readEvents(quakeMLfle)  # reading the catalog file
#                     for evt in cat.events:
#                         hasManualPicks = True
#                         if evt.event_type:
#                             eventType = evt.event_type
#
#                             # loop over origins
#                             # for origin in evt.origins:
#
#                 # This will need to be fixed ...
#                 try:
#                     eventStatus = cat.events[0].origins[0].evaluation_status
#                     error = cat.events[0].origins[0].origin_uncertainty.confidence_ellipsoid.semi_major_axis_length
#                 except:
#                     eventStatus = 'unknown'
#                     error = np.nan
#
#                 dataDict = {}
#                 dataDict['eventID'] = num_events
#                 dataDict['originID'] = originID
#                 dataDict['eventStatus'] = (not (eventStatus == 'rejected'))
#                 dataDict['hasManualPicks'] = hasManualPicks
#                 dataDict['eventType'] = eventType
#                 dataDict['error'] = error
#
#                 num_events += 1
#
#                 # abs_file = os.path.join(root, f)
#                 self.dataDict[seisfle] = dataDict
#
#         if not self.dataDict:
#             logger.error('No data files found in the given folder!')
#             return
#
#     def load_events(self):
#
#         quakeMLfle = '%s.xml' % os.path.splitext(self.current_file)[0]
#         quakeMLfle = os.path.join(self.base_dir, quakeMLfle)
#
#         self.curEventDict = {}
#         self.curEventDict['origin'] = []
#         self.curEventDict['eventType'] = 'not reported'
#         self.curEventDict['evaluationMode'] = 'manual'
#         self.curEventDict['evaluationStatus'] = 'preliminary'
#         self.picks = []
#
#         if os.path.exists(quakeMLfle):
#             self.current_catalog = SeisCatalog(obevent.readEvents(quakeMLfle))  # reading the catalog file
#             if self.current_catalog.events:
#                 evt = self.current_catalog.events[0]
#                 if evt.origins:
#                     org = evt.origins[0]
#                     self.curEventDict['origin'] = [org.x, org.y, org.z]
#                     self.curEventDict['evaluationMode'] = org.evaluation_mode
#                     self.curEventDict['evaluationStatus'] = org.evaluation_status
#                 if evt.event_type:
#                     self.curEventDict['eventType'] = evt.event_type
#                 # try:
#                 # 	if org.evaluationStatus in ["preliminary", "confirmed", "reviewed", "final"]:
#                 # 		self.curEventDict['evaluationStatus'] = 'Accepted'
#                 # 	else:
#                 # 		self.curEventDict['evaluationStatus'] = 'Accepted'
#                 # except:
#                 # 	self.curEventDict['evaluationStatus'] = 'Accepted'
#
#                 self.picks = evt.picks
#         else:
#             self.current_catalog = None
#
#     def sort_by_distance(self):
#
#         self.dist = []
#
#         if not self.current_catalog:
#             return
#
#         eventLocation = self.curEventDict['origin']
#         if not eventLocation:
#             return
#
#             # TODO: implement sorting by distance
#             # for s in self.station_names:
#             # 	sPos = np.array(s[2:5])
#             # 	self.dist.append(np.linalg.norm(sPos - np.array(eventLocation)))
#             # self.dist = np.array(self.dist)
#             #
#             # sortIndices = np.argsort(self.dist)
#             # self.dist = self.dist[sortIndices]
#             # self.station_names = self.station_names[sortIndices]
#
#     def composite_traces(self):
#
#         compositeTraces = []
#         for s in self.station_names:
#             st2 = self.current_stream.select(station=s)
#             if len(st2.traces) == 0:
#                 logger.warning('station %s has no data' % s)
#                 continue
#
#             data = 0
#             for tr2 in st2:
#                 data += tr2.data ** 2
#
#             data = np.sqrt(data) * np.sign(st2[-1])
#
#             newtr = Trace(data=data, header=st2.traces[0].stats)
#             newtr.stats.channel = 'CMP'
#             compositeTraces.append(newtr)
#             self.cleanup_stream(st2)
#
#         self.composite_stream = Stream(traces=compositeTraces)
#
#     def commitModifications(self):
#
#         logger.debug('Committing modifications')
#
#         # Write QuakeML file
#         quakeml_file = '%s.xml' % os.path.splitext(self.current_file)[0]
#         quakeml_file = os.path.join(self.base_dir, quakeml_file)
#
#         cat = SeisCatalog()
#         origin = event.Origin()
#         if self.curEventDict['evaluationMode'].lower() == 'accepted':
#             origin.evaluation_status = 'preliminary'
#         else:
#             origin.evaluation_status = 'rejected'
#
#         # opicks = []
#         #
#         # for p in self.picks:
#         # 	opicks.append(p)
#
#         evt = SeisEvent(picks=self.picks, origins=[origin])
#         evt.event_type
#         # evt.origins = event_type = self.curEventDict['eventType']
#         # evt.event_status = self.curEventDict['evaluationMode']
#
#
#
#         cat.events.append(evt)
#         cat.write(quakeml_file, format="QUAKEML")
#
#         self.modifiedEventType = None
#         self.modifiedPicks = []
#         self.modifiedStatus = None
#
#         return True
