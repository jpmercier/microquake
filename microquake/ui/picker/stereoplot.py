from functools import partial

import numpy as np
import pyqtgraph as pg

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
from PyQt4.Qt import QColor


QtCore.pyqtRemoveInputHook()


class StereoPlot(QtGui.QWidget):
    def __init__(self, baseWindow, useMainWindow):

        ## Switch to using white background and black foreground
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

        QtGui.QWidget.__init__(self)

        verticalLayout = QtGui.QVBoxLayout(self)
        verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(verticalLayout)
        self.setMaximumHeight(300)
        self.setMaximumWidth(300)

        self.initFig()

        self.baseWindow = baseWindow
        self.model = None

        self.plotHeight = 100

    def initFig(self):

        # Creates and adds GraphicsView to layout
        self.fig = pg.GraphicsView()
        self.layout().addWidget(self.fig)

        self.ci = pg.GraphicsLayout()
        for n in ['nextRow', 'nextCol', 'nextColumn', 'addPlot', 'addViewBox', 'addItem', 'getItem', 'addLabel',
                  'addLayout', 'addLabel', 'addViewBox', 'removeItem', 'itemIndex', 'clear']:
            setattr(self.fig, n, getattr(self.ci, n))
        self.fig.setCentralItem(self.ci)

        self.stereo = self.fig.addPlot()

        # self.proxy1 = pg.SignalProxy(self.scene().sigMouseMoved, rateLimit=60, slot=self.onMove)
        # self.proxy2 = pg.SignalProxy(self.fig.scene().sigMouseClicked, rateLimit=60, slot=self.onClick)

    # self.proxy3 = pg.SignalProxy(self.scene().keyPressEvent, rateLimit=60, slot=self.keyPress)
    # self.connect(self.scene(), QtCore.SIGNAL("keyPressEvent()"), self.keyPress)
    # self.scene().keyPressEvent.connect(self.keyPress)

    def setDataModel(self, model):

        self.model = model
        self.drawFig()

    def drawFig(self):

        event = self.model.current_event
        ori = event.preferred_origin()
        az = []
        toa = []
        for arr in ori.arrivals:
            az.append(arr.azimuth)
            toa.append(arr.takeoff_angle)

        self.stereo.plot(az, toa)
