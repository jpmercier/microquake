from obspy import Stream
import numpy as np

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
from seisfigure_pyplot import SeisFigure

class StationPlot(QtGui.QWidget):

    def __init__(self, baseApp, model):
        
        QtGui.QWidget.__init__(self)

        self.baseApp = baseApp
        self.model = model
        self.setWindowModality(Qt.WindowModal)
        self.currentStation = None
        self.pickAdded = False

        self.fig = SeisFigure(self, False)
        self.setAttribute(Qt.WA_DeleteOnClose, False)
        self.statusBar = QtGui.QStatusBar(self)
        self.stationToolBar = QtGui.QToolBar(self)

        # ---- Event selector toolbar ----

        self.stationCountLabel = QtGui.QLabel(self.stationToolBar)
        self.stationCountLabel.setText('<b>(1/1)</b>')
        self.stationToolBar.addWidget(self.stationCountLabel)
 
        prevStation = QtGui.QAction(self.stationToolBar)
        prevStation.setText(QtGui.QApplication.translate("GoldPickUI", "<< &Prev", None, QtGui.QApplication.UnicodeUTF8))
        self.connect(prevStation, QtCore.SIGNAL("triggered()"), self.prevStation)
        self.stationToolBar.addAction(prevStation)

        nextStation = QtGui.QAction(self.stationToolBar)
        nextStation.setText(QtGui.QApplication.translate("GoldPickUI", "&Next >>", None, QtGui.QApplication.UnicodeUTF8))
        self.connect(nextStation, QtCore.SIGNAL("triggered()"), self.nextStation)
        self.stationToolBar.addAction(nextStation)

        verticalLayout = QtGui.QVBoxLayout(self)
        verticalLayout.addWidget(self.fig)
        verticalLayout.addWidget(self.stationToolBar)
        verticalLayout.addWidget(self.statusBar)

        self.show()

    def keyPressEvent(self, event):
        self.baseApp.keyPressEvent(event)

    def setData(self, stationIndex):

        self.currentStationIndex = stationIndex

        self.currentStation = self.model.station_names[stationIndex]

        use_clean_only = True
        # if use_clean_only:
        #     st = self.model.cleaned_stream
        # else:
        st = self.model.current_stream

        self.plotStream = st.select(station = self.currentStation)

        comp = []
        for s in self.plotStream:
            comp.append(s.stats.channel)

        comp = np.sort(np.array(comp))

        orderedTraces = []
        for c in comp:
            orderedTraces.append(self.plotStream.select(channel = c)[0])

        self.plotStream = Stream(traces=orderedTraces)

        self.updateStationLabel()
        self.fig.setDataModel(self.model, stream=self.plotStream)

    def prevStation(self):

        if self.currentStationIndex <= 0:
            return

        self.setData(self.currentStationIndex - 1)

    def nextStation(self):

        if self.currentStationIndex >= (len(self.model.station_names) - 1):
            self.setData(0)

        self.setData(self.currentStationIndex + 1)

    def updateStationLabel(self):

        self.stationCountLabel.setText('(<b>%d/%d</b>)' % (self.currentStationIndex+1, len(self.model.station_names)))

    def updateSB(self, text):
        self.statusBar.showMessage(text)

    def closeEvent(self, event):
        if len(self.model.modifiedPicks) > 0:
            self.baseApp.fig.drawFig()

        QtGui.QWidget.closeEvent(self, event)
