from functools import partial

import numpy as np
import pyqtgraph as pg

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
from PyQt4.Qt import QColor

# debug stuff
from IPython.core.debugger import Tracer

# import objgraph
# import gc
QtCore.pyqtRemoveInputHook()


class SeisFigure(QtGui.QWidget):
    def __init__(self, baseWindow, useMainWindow):

        ## Switch to using white background and black foreground
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

        QtGui.QWidget.__init__(self)

        verticalLayout = QtGui.QVBoxLayout(self)
        verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(verticalLayout)

        self.initFig()

        self.baseWindow = baseWindow
        self.useMainWindow = useMainWindow
        self.model = None
        self.dataStream = None
        self.pickItems = []
        self.plots = []

        self.plotHeight = 100

    def initFig(self):

        # Creates and adds GraphicsView to layout
        self.fig = pg.GraphicsView()
        self.layout().addWidget(self.fig)

        self.ci = pg.GraphicsLayout()
        for n in ['nextRow', 'nextCol', 'nextColumn', 'addPlot', 'addViewBox', 'addItem', 'getItem', 'addLabel',
                  'addLayout', 'addLabel', 'addViewBox', 'removeItem', 'itemIndex', 'clear']:
            setattr(self.fig, n, getattr(self.ci, n))
        self.fig.setCentralItem(self.ci)

        # self.proxy1 = pg.SignalProxy(self.scene().sigMouseMoved, rateLimit=60, slot=self.onMove)
        self.proxy2 = pg.SignalProxy(self.fig.scene().sigMouseClicked, rateLimit=60, slot=self.onClick)

    # self.proxy3 = pg.SignalProxy(self.scene().keyPressEvent, rateLimit=60, slot=self.keyPress)
    # self.connect(self.scene(), QtCore.SIGNAL("keyPressEvent()"), self.keyPress)
    # self.scene().keyPressEvent.connect(self.keyPress)

    def getBaseApp(self):

        if self.useMainWindow:
            return self.baseWindow
        else:
            return self.baseWindow.baseApp

    def setDataModel(self, model, stream=None, dist=None):

        self.model = model
        if stream is None:
            if model.is_filtered:
                self.dataStream = model.filtered_stream
            else:
                self.dataStream = model.composite_stream
        else:
            self.dataStream = stream

        self.dist = dist

        self.baseWindow.updateSB('Data ready.')
        self.drawFig()

    def drawFig(self):

        self.clearPlots()

        if not self.dataStream:
            return

        num_plots = len(self.dataStream)
        if num_plots == 0:
            return

        firstPlot = None

        for i, tr in enumerate(self.dataStream):

            p = self.fig.addPlot()
            self.plots.append(p)

            p.disableAutoRange(pg.ViewBox.XAxis)
            delta = tr.stats.delta
            tr_data = tr.data[:-1]
            maxTime = delta * len(tr_data)
            timeData = np.arange(0, maxTime, delta)

            p.plot(timeData, tr_data, pen=0.5, clear=True)
            p.getAxis('bottom').setScale(1000)
            yAxis = p.getAxis('left')
            if self.useMainWindow:
                yAxis.setTicks([[(0, '')]])
            else:
                if 'G' in tr.stats.station:
                    yAxis.setLabel(text='Velocity', units='m/s')
                else:
                    yAxis.setLabel(text='Acceleration', units='m/s2')

            if i == 0:
                firstPlot = p
            else:
                p.setXLink(firstPlot)

            p.setXRange(0, maxTime)
            # p.setMouseEnabled(y=False)
            p.showGrid(y=True, alpha=0.05)

            p.delta = delta
            p.stationIndex = i
            p.station = tr.stats.station
            p.channel = tr.stats.channel
            # if self.useMainWindow:
            # p.dist = self.dist[i]

            if self.useMainWindow:
                lbl = tr.stats.station
                if self.dist is not None:
                    lbl = '%s - Dist=%.2f m' % (lbl, self.dist[i])
                p.label = pg.TextItem(text=lbl, color='k', anchor=(1, 0))
            else:
                p.label = pg.TextItem(text=tr.stats.channel, color='k', anchor=(1, 0))

            p.addItem(p.label)

            if i == (len(self.dataStream) - 1):
                p.getAxis('bottom').setLabel(text='Time', units='s')

            if i == 0:
                if self.model.curEventDict():
                    evaluationMode = 'manually'\
                        if self.model.curEventDict()['evaluationMode'] == 'manual'\
                        else 'automatically'
                    evaluationStatus = self.model.curEventDict()['evaluationStatus']

                    if self.useMainWindow:
                        p.setTitle(
                            'Event picked %s %s' % (evaluationMode, evaluationStatus))
                    else:
                        p.setTitle('Event picked %s %s\n%s - %s - %s' % (
                            evaluationMode, evaluationStatus,
                            self.dataStream[-1].stats.network,
                            self.dataStream[-1].stats.station,
                            self.dataStream[-1].stats.starttime.isoformat()))

            # draw text for crosshair
            p.crosshair = pg.TextItem(text='', color=(0, 0, 0), html=None, anchor=(0, 0), border=None, fill='w', angle=0)
            # draw lines
            p.vLine = pg.InfiniteLine(angle=90, movable=False, pen='r')
            p.hLine = pg.InfiniteLine(angle=0, movable=False, pen='r')

            p.crosshair.setVisible(False)
            p.vLine.setVisible(False)
            p.vLine.setVisible(False)

            # add to viewBox
            p.addItem(p.vLine, ignoreBounds=True)
            p.addItem(p.hLine, ignoreBounds=True)
            p.addItem(p.crosshair)

            def mouseMoved(p, event):

                mode = self.getBaseApp().mode

                inside = p.sceneBoundingRect().contains(event.x(), event.y())
                pickmode = mode in self.getBaseApp().modes[1:5] and inside
                if pickmode:
                    dataPos = p.vb.mapSceneToView(pg.Point(event.x(), event.y()))
                    self.baseWindow.updateSB('Cursor at: %f, %f.' % (dataPos.x(), dataPos.y()))

                    p.crosshair.setText("   %s=%0.3f,   %s=%0.3f" % ("x", dataPos.x(), "y", dataPos.y()), color='k')
                    p.crosshair.setPos(p.vb.viewRange()[0][0],
                                       p.vb.viewRange()[1][1])
                    p.vLine.setPos(dataPos.x())
                    p.hLine.setPos(dataPos.y())

                p.crosshair.setVisible(pickmode)
                p.vLine.setVisible(pickmode)
                p.hLine.setVisible(pickmode)

            p.vb.scene().sigMouseMoved.connect(partial(mouseMoved, p))

            def rangeChanged(p, event):
                p.label.setPos(p.vb.viewRange()[0][1], p.vb.viewRange()[1][1])

            p.sigRangeChanged.connect(partial(rangeChanged, p))

            self.fig.nextRow()

        # plot picks
        self.drawPicks(draw=False)

        if self.useMainWindow:
            self.fig.setFixedHeight(self.plotHeight * (num_plots + 1))

        self.baseWindow.updateSB('Plot ready.')
        self.canvasDraw()

    def canvasDraw(self):

        """
        Redraws the canvas and re-sets mouse focus
        """

    # for i, ax in enumerate(self.get_axes()):
    #    ax.set_xticklabels(ax.get_xticks())

    # self.canvas.draw()
    # self.canvas.setFocus()

    def drawPicks(self, draw=True):

        picks = self.model.getPicks(self.baseWindow.currentStation, self.useMainWindow)
        xpicks = self.model.getPickXPosition(picks)

        # delete all annotations
        for r in self.pickItems:
            p = r[0]
            i = r[1]
            p.removeItem(i)
            del i

        self.pickItems = []
        for p in self.plots:
            lines = []
            labels = []
            # transOffset = offset_copy(ax.transData, fig=self,
            #                x=5, y=0, units='points')
            for i, xpick in enumerate(xpicks):
                if picks[i].phase_hint == 'S':
                    color = 'red'
                elif picks[i].phase_hint == 'P':
                    color = 'green'
                elif picks[i].phase_hint == 'E':
                    color = 'blue'
                else:
                    color = 'blue'

                alpha = .8
                col = QColor(color)
                col.setAlphaF(alpha)

                if p.station != picks[i].waveform_id.station_code:
                    continue

                # if (not self.useMainWindow) and (not picks[i].waveform_id.channel_code in p.channel):
                # continue

                lines.append(pg.InfiniteLine(pos=xpick, pen=col))
                label = pg.TextItem(anchor=pg.Point(0, 0), text=picks[i].phase_hint, color=col)

                label.setPos(pg.Point(xpick, p.viewRect().bottom()))
                labels.append(label)

            # add updated objects
            for line in lines:
                self.pickItems.append([p, line])
                p.addItem(line)

            for label in labels:
                self.pickItems.append([p, label])
                p.addItem(label)

        if draw:
            self.canvasDraw()

    def changedMode(self, mode):

        # pickmode = mode in self.getBaseApp().modes[1:5]
        pickmode = False
        for p in self.plots:
            p.crosshair.setVisible(pickmode)
            p.vLine.setVisible(pickmode)
            p.hLine.setVisible(pickmode)

    def wheelEvent(self, event):
        if self.useMainWindow and not event.modifiers() & Qt.ControlModifier:
            self.baseWindow.mainArea.wheelEvent(event)
            return

        pg.GraphicsLayoutWidget.wheelEvent(self, event)

    def onClick(self, event):

        event = event[0]
        if event.currentItem is None:
            return

        curPlot = event.currentItem.parentItem()

        stationIndex = curPlot.stationIndex
        station = curPlot.station
        channel = curPlot.channel

        dataPos = curPlot.vb.mapToView(pg.Point(event.pos().x() - curPlot.getScale("bottom").x(), event.pos().y()))
        dataPosX = dataPos.x()
        data = curPlot.dataItems[0].getData()
        idx = (np.abs(data[0] - dataPosX)).argmin()

        tr_amp = data[1][idx + 3] - data[1][idx]

        if tr_amp < 0:
            polarity = 'negative'
        elif tr_amp > 0:
            polarity = 'positive'
        else:
            polarity = 'undecideable'

        baseApp = self.getBaseApp()

        mode = self.getBaseApp().mode

        if mode == 'pick_p':
            baseApp.addPicks(dataPosX, phase='P', station=station, polarity=polarity)
            self.baseWindow.updateSB(
                'Added %s pick with polarity %s for station %s at time = %f' % ('P', polarity, station, dataPosX))
        elif mode == 'pick_s':
            baseApp.addPicks(dataPosX, phase='S', station=station, polarity=polarity)
            self.baseWindow.updateSB(
                'Added %s pick with polarity %s for station %s at time = %f' % ('S', polarity, station, dataPosX))
        elif mode == 'pick_end':
            baseApp.addPicks(dataPosX, phase='E', station=station, polarity=polarity)
            self.baseWindow.updateSB(
                'Added %s pick with polarity %s for station %s at time = %f' % ('E', polarity, station, dataPosX))
        elif mode == 'pick_remove':
            baseApp.delPicks(station=station)
        elif mode == 'pick_none':

            self.baseWindow.updateSB('Removed all picks for station %s' % station)
        elif self.useMainWindow and event.double():
            self.baseWindow.openStationPlot(station)
        else:
            return

        self.drawPicks()

    def clearPlots(self):

        """
        for p in self.plots:
            p.clear()
            p.clearPlots()
            #p.close()

            if not p.ctrlMenu is None: ## already shut down
                p.ctrlMenu.setParent(None)
                del p.ctrlMenu
                p.ctrlMenu = None

            for k in p.axes:
                i = p.axes[k]['item']
                #i.close()
                i.scene().removeItem(i.label)
                del i.label
                i.label = None
                i.scene().removeItem(i)
                del i
  
            p.axes.clear()
            p.axes = None
            p.scene().removeItem(p.vb)
            #p.vb.clear()
            for i in p.vb.addedItems[:]:
                p.vb.removeItem(i)
            for ch in p.vb.childGroup.childItems():
                if hasattr(ch, 'setParent'):
                    ch.setParent(None)
            
            del p.vb.menu
            del p.vb
            p.vb = None
 
            del p

        gc.collect()
        """

        # self.clear()
        """
        self.scene().clear()
        del self.centralWidget
        self.centralWidget = None
        self.setCentralItem(QtGui.QGraphicsWidget())
        self.centralLayout = QtGui.QGraphicsGridLayout()
        self.centralWidget.setLayout(self.centralLayout)
        self.ci = pg.GraphicsLayout()
        for n in ['nextRow', 'nextCol', 'nextColumn', 'addPlot', 'addViewBox', 'addItem', 'getItem', 'addLabel', \
                      'addLayout', 'addLabel', 'addViewBox', 'removeItem', 'itemIndex', 'clear']:
            setattr(self, n, getattr(self.ci, n))
        self.setCentralItem(self.ci)
        """

        self.layout().setEnabled(False)
        # Removes previous items in layout
        # Reference: http://stackoverflow.com/questions/4528347/clear-all-widgets-in-a-layout-in-pyqt
        for i in reversed(range(self.layout().count())):
            # self.layout().itemAt(i).widget().setParent(None)
            self.layout().itemAt(i).widget().deleteLater()

        self.initFig()
        self.layout().setEnabled(True)

        self.fig.items()[0].currentRow = 0
        self.plots = []
        self.pickItems = []
