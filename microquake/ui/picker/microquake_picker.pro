#-------------------------------------------------
#
# Project created by QtCreator 2016-02-10T13:03:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = microquake_picker
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui \
    uQuakeWaveui.ui

RESOURCES += \
    icons.qrc
