import numpy as np
import os
import argparse
from mrjob.job import MRJob
from mrjob.compat import get_jobconf_value
from microquake.core.data import GridData
from microquake.core import ctl, logger, nll, SeisPlot, SeisEvent, SeisCatalog
import pickle as pickle
from IPython.core.debugger import Tracer
from microquake.simul import eik
from microquake.waveform import mag
import pandas as pd
import matplotlib.pyplot as plt
from microquake.core import event
import matplotlib.pyplot as plt
from microquake.core.cli import ProgressBar


def calculate_uncertainty(frechet, pick_uncertainty=1e-3):
    """
    :param frechet: frechet derivative matrix m lines, n dimension columns
    unit s/m or s/km
    :type frechet: np.array
    :param pickuncertainty: pick uncertainty
    :type pickuncertainty: float (seconds)
    :rparam: error ellipsoid orientation and magnitude (m or km)
    """
    hessian = np.linalg.inv(np.dot(frechet.T, frechet)) 
    tmp = hessian * pick_uncertainty ** 2
    w, v = np.linalg.eig(tmp)
    return np.max(w)


def measure_sensitivity(values, min_sensitivity=2):
    indices = np.argsort(values['sensitivity'])

    stations = []
    for i in indices:
        if values['stname'][i] not in stations:
            stations.append(values['stname'][i])
        if len(stations) == 5:
            sensitivity = values['sensitivity'][i]
            return sensitivity

    return min_sensitivity


def reducer(self, key, values):
    """
    This function calculates traveltimes from one stations to every grid point
    """

    if 'mag' not in self.params.keys():
        logger.critical('No magnitude specified')
    if 'pick_perturb' not in self.params.keys():
        logger.critical('No pick perturbation')

    key = eval(key)
    values = combine_dict_values(values)

    suffix = ctl.NLLSuffixFromKey(key)

    stname = key['stname']

    velgrid_index = key['velgrids']
    homogeneous = self.params.velgrids[velgrid_index].homogeneous

    evlocs = values['evloc']
    stloc = values['stloc'][0]

    logger.info("Loading grids")

    Vp = GridData()
    Vs = GridData()
    
    basename = self.NLL_BASE + '/model/%s_%s' % (self.params.project_code, suffix)
    Vp.readNLL(basename + '.P.mod', velocity=True)
    Vs.readNLL(basename + '.S.mod', velocity=True)

    if not self.QP:
        Qp = None
    else:
        Qp = pickle.load(open(self.QP))
    if not self.QS:
        Qs = None
    else:
        Qs = pickle.load(open(self.QS))

    SSD = self.params.SSD

    if homogeneous:
        buf = 0
    else:
        buf = 2

    AttGrid_p, tt_p = mag.CalculateAttenuationGrid(stloc, Vp, quality=Qp, locations=evlocs,
                                             triaxial=True, orientation=(0, 0, 1),
                                             buf=buf, return_tt=True, homogeneous=homogeneous)

    AttGrid_s, tt_s = mag.CalculateAttenuationGrid(stloc, Vs, quality=Qs, locations=evlocs,
                                             triaxial=True, orientation=(0, 0, 1),
                                             buf=buf, return_tt=True, homogeneous=homogeneous)

    Sensitivity_p = mag.DetectionLevelSTALTAGrid(AttGrid_p, Vp, Vs,
                    noise_level=self.params.noise_magnitude, SSD=SSD)
    Sensitivity_s = mag.DetectionLevelSTALTAGrid(AttGrid_s, Vp, Vs,
                    noise_level=self.params.noise_magnitude * 2, SSD=SSD)

    outkey = {}

    # forming the output key
    outkey['velgrids'] = key['velgrids']
    outkey['sensors'] = key['sensors']

    outdict = {}
        
    pb = ProgressBar(max=len(evlocs))

    frechet_p = eik.sensitivity_location(Vp, stloc, evlocs)
    frechet_s = eik.sensitivity_location(Vs, stloc, evlocs)


    for k, (evloc, sens_p, sens_s, ttp, tts, fp, fs) in enumerate(zip(evlocs, Sensitivity_p.data,
                                                Sensitivity_s.data, tt_p.data, tt_s.data, 
                                                frechet_p, frechet_s)):
        pb()

        # frechet_p = eik.sensitivity_location(ttgrid_p, evloc)
        # frechet_s = eik.sensitivity_location(ttgrid_s, evloc)
        
        distance = np.linalg.norm(evloc - stloc)
        outdict['frechet'] = list(fp)
        outdict['distance'] = distance
        outdict['evloc'] = list(evloc)
        outdict['stloc'] = list(stloc)
        outdict['stname'] = stname
        outdict['tt'] = ttp
        outdict['sensitivity'] = sens_p
        outdict['phase'] = 'P'

        suffix = ctl.NLLSuffixFromKey(key)
        outkey['evloc'] = list(evloc)
        outkey['suffix'] = suffix

        # outdict['key'] = outkey

        yield str(outkey), str(outdict)

        outdict['sensitivity'] = sens_s
        outdict['frechet'] = list(fs)
        outdict['phase'] = 'S'
        outdict['tt'] = tts

        yield str(outkey), str(outdict)
