from microquake.simul import eik
from microquake.core import GridData
import numpy as np
from scipy.ndimage.filters import gaussian_filter
from IPython.core.debugger import Tracer


def test_full():
    """Eikonal solver and raytracer"""

    vtmp = gaussian_filter(np.random.rand(100, 100, 100), 10)
    vtmp = (vtmp - np.mean(vtmp)) * 600 / np.std(vtmp)  + 5000

    vel = GridData(shape_or_data=vtmp, spacing=10)

    seed = np.array([200, 150, 400])
    #	Tracer()()
    tt = eik.eikonal_solver(vel, seed)

    event = np.array([832, 800, 700])

    ray = eik.ray_tracer(tt, event)
    return ray, tt

def angles(stloc, X, Y, Z):
    dX = stloc[0] - X
    dY = stloc[1] - Y
    dZ = stloc[2] - Z

    az = np.arctan2(dX, dY)
    dH = np.sqrt(dX ** 2 + dY ** 2)
    toa = np.arctan2(dH, -dZ)
    return az, toa

if __name__ == "__main__":
    ray, tt = test_full()
    az_e, toa_e = eik.angles(tt)
    x = np.arange(0, tt.shape[0]) * tt.spacing
    y = np.arange(0, tt.shape[1]) * tt.spacing
    z = np.arange(0, tt.shape[2]) * tt.spacing
    Y,X,Z = np.meshgrid(y,x,z)
    seed = np.array([200, 150, 400])
    az, toa = angles(seed, X, Y, Z)
	# print ray

	# plt.figure(1)
	# plt.subplot(121)
	# plt.title('Velocity model')
	# plt.imshow(vtmp[:, :])
	# plt.colorbar()
	# xlim = plt.xlim()
	# ylim = plt.ylim()
	# plt.subplot(122)
	# plt.title('Travel time')
	# plt.imshow(tt.data[:, :])
	# plt.plot(vel.transform_to(ray)[:, 0], vel.transform_to(ray)[:, 1])
	# plt.colorbar()
	# plt.xlim(xlim)
	# plt.ylim(ylim)
	# plt.savefig('Result.pdf')
	# plt.show()
