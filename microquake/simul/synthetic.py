from microquake.core import Trace
import numpy as np
from microquake.core.trace import Stats
from microquake.waveform import mag


# This function

# from Brune model:
# D(f,t) = M0 x R

# M0: seismic moment
# R: Average radiation pattern (0.52 and 0.63 for P and S, respectively [Aki and Richard, 2002])


@deprecated
def Seismogram(Mw, duration=0.1, sampling_rate=10000, vp=5000.0, vs=3000.0, rho=2400, SSD=10, pwave=True):
	""" Create a synthetic displacement pulse at the source seismogram based on the brune model (Brune 1970).
	This model is extensively used and generally agrees with observations from many different setting and over
	a large range of magnitude.

	The displacement time function, u(t), is expressed as follows:

		u(t) = A_0 x t x omega_0 x H(t) * exp(-t x omega_0) ,

	where t the time, omega_0, the angular frequency and H(t) is the heavyside function.
	Note that the angular frequency is calculated from the static stress drop (SSD). A0 is given by the following
	equation:

		A0 = M0 / (4 * pi * rho * v ** 3)

	References for further reading:
	- Routine data processing in earthquake seismology
	- Relating Peak Particle Velocity and Acceleration to Moment Magnitude in Passive (Micro-) Seismic Monitoring
	(www.bcengineers.com/images/BCE_Technical_Note_3.pdf)

	:param Mw: the moment magnitude of the seismic event (default: -1).
		This value determine the wave amplitude and the frequency content
	:type Mw: float
	:param duration: duration of the seismogram in seconds (default: 0.1), the pulse is centered at zero
	:type duration: float
	:param sampling_rate: sampling rate in Hz of the generated time series (default: 10000)
	:type sampling_rate: int
	:param vp: P-wave velocity of the material at the source (default: 5000 m/s)
	:type vp: float
	:param vs: S-wave velocity of the material at the source (default: 3500 m/s)
	:type vs: float
	:param rho: density of the material at the source in kg/m**3 (default: 2400 kg/m**3)
	:type rho: float
	:param SSD: Static stress drop in "bar" (default: 10 bar)
	:type SSD: float
	:param pwave: Return P-wave displacement seismogram if True and S-wave displacement seismogram if false
	:type pwave: bool
	:returns: tuple Obspy Trace containing the seismogram
	:rtype: Obspy Trace

	.. note::
	
		The velocity and acceleration can easily be obtained by differentiating the trace using the Obspy Trace method
		differentiate.

			>>> tr = Seismogram(-1)
			>>> vel = tr.differentiate()  # this creates a velocity trace
			>>> print vel.stats.processing
			['differentiate:gradient']
			>>> accel = vel.differentiate()  # this creates an acceleration trace
			>>> print accel.stats.processing
			['differentiate:gradient', 'differentiate:gradient']


		This operation is performed in place on the actual data arrays. The
	    raw data is not accessible anymore afterwards. To keep your
	    original data, use :meth:`~obspy.core.trace.Trace.copy` to create
	    a copy of your trace object.
	    This also makes an entry with information on the applied processing
	    in ``stats.processing`` of this trace.

	"""
	npts = duration * sampling_rate
	# if not npts % 2:
	# 	npts += 1

	t = np.arange(npts) / sampling_rate - duration / 2  # to have the pulse in the middle of the seismogram

	M0 = mag.Mw2M0(Mw)
	(f0p, f0s) = mag.corner_frequency(Mw, vp, vs, SSD)
	if pwave:
		W0 = 2 * np.pi * f0p
		v = vp
	else:
		W0 = 2 * np.pi * f0s
		v = vs

	A0 = M0 / (4 * np.pi * rho * v ** 3)

	d = A0 * t * W0 ** 2 * np.exp(-t * W0)
	d = np.fft.fftshift(d)
	# d[t < 0] = -d[t > 0][::-1]  # Applying heavyside function

	# Creating Trace object

	stats = Stats()
	stats.sampling_rate = sampling_rate
	stats.npts = npts
	
	return Trace(data=d, header=stats)


# class seismogram:

# 	__init__(self, magnitude=-1.0, noise_level=1.0e-5, duration=0.1, sampling_rate=10000, density=2400, velocity=5000.0):
# 		""" Create a synthetic displacement seismogram based on the brune model (Brune 1970).
#   This model is extensively used and generally
# 		agrees with observations from many different setting and over a large range of magnitude.

# 		The displacement time function, u(t), is expressed as follows:

# 			u(t) = M_0 x t x Omega_0 x H(t) * exp(-t x Omega_0),

# 		where M_0 is the seismic moment, t the time, Omega_0, the angular frequency and H(t) is the heavyside function.

# 		References for further reading:
# 		- Routine data processing in earthquake seismology
# 		- Relating Peak Particle Velocity and Acceleration to Moment Magnitude in Passive (Micro-) Seismic Monitoring
# 		(www.bcengineers.com/images/BCE_Technical_Note_3.pdf)

# 		:param magnitude: the moment magnitude of the seismic event (default: -1),
# 			this value determine the wave amplitude and the frequency content
# 		:type magnitude: float
# 		:param src_loc: location of source in cartesian coordinate
# 		:param noise_level: level of gaussian noise to add to the synthetic seismogram (default: 1e-5)
# 		:type noise_level: float
# 		:param accelerogram: if True generate an accelerogram if False generate a seismogram (default: True)
# 		:type accelerogram: bool
# 		:param duration: duration of the seismogram in seconds (default: 0.1), the pulse is centered at zero
# 		:type duration: float
# 		:param sampling_rate: sampling rate in Hz of the generated time series (default: 10000)
# 		:type sampling_rate: int
# 		:param density: density of the material at the source (default: 2400 kg/m**3)
# 		:type density: float
# 		:param velocity: velocity of the material at the source (default: 5000 m/s)
# 		:type velocity: float
# 		"""

# 		self.magnitude = magnitude
# 		self.noise_level = noise_level
# 		self.accelerogram = accelerogram
# 		self.duration = duration
# 		self.sampling_rate = sampling_rate
# 		self.density = density
# 		self.velocity = velocity
# 		self.seismogram = Stream(trace=Trace())

# 		self.__update_wavelet__()

# 	__update_wavelet__(self):
# 		"""
# 		Updating wavelet when a parameter changes
# 		"""

# 		stats = Stats()
# 		stats.sampling_rate = self.sampling_rate
# 		stats.npts = self.npts

# 		self.npts = self.duration * self.sampling_rate
# 		t = np.arange(self.npts) / self.sampling_rate - self.duration / 2
# 		Noise = randn(self.npts)
		

# 		pass
