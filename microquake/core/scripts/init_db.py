#!/usr/bin/env python

# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: init_db
#  Purpose: initialize mongo db
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
initialize mongo db

:copyright:
    microquake development team (dev@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""


import os
from microquake.core import ctl, logger, read_events
from microquake.db import Project


def step_wave_file(f, source, root, project):

    # abspath = os.path.join(root, f)
    relDir = os.path.relpath(root, source)
    waveform_fle = os.path.join(relDir, f)
    waveform_fle_name = os.path.basename(waveform_fle)

    inserted_id = project.insert_waveform(relDir, waveform_fle_name)
    if not inserted_id:
        logger.error('Error inserting file: %s' % waveform_fle)
        return False

    inserted_id = inserted_id[0]

    event_fle = '%s.xml' % os.path.splitext(waveform_fle)[0]
    event_fle = os.path.join(project.event_path, event_fle)

    if not os.path.exists(event_fle):
        return True

    cat = read_events(event_fle)  # reading the catalog file
    num_events = len(cat.events)

    dirname = os.path.dirname(event_fle)
    relDir = os.path.relpath(dirname, project.event_path)
    event_fle_name = os.path.basename(event_fle)
    inserted_ids = []
    for k, event in enumerate(cat):
        catalog_index = k
        inserted_ids.append(project.insert_event(event, relDir, event_fle_name,
                            inserted_id, catalog_index=k))

    # Update waveform with number of events
    project.waveforms_col.update_one({'_id': inserted_id},
                                     {'$set':{'num_events': num_events}})

    if len(inserted_ids) != num_events:
        logger.warning('Not all of the events were inserted: %s' % event_fle)
        return False

    return True

import argparse
parser = argparse.ArgumentParser(description='Microquake picker program')
parser.add_argument('control_file', type=str,
                    help="XML configuration file")

def main():
    args = parser.parse_args()
    control_file = args.control_file

    params = ctl.parse_control_file(control_file)

    project = Project(params.project)
    project.connect()

    project.traverse_folder(project.waveform_path, step_wave_file)

if __name__ == '__main__':

    main()
