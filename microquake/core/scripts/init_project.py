#!/usr/bin/env python

# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: init-project
#  Purpose: project initialization
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
Project initialization script

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""


import argparse


def main():
    from microquake.core import ctl
    from microquake.nlloc import init_nlloc_from_params
    from microquake.spark import init_spark_from_params
    args = parser.parser_args()
    control_file = args.control_file
    init_project(control_file)
    params = ctl.parse_control_file(control_file)
    try:
        sc = init_spark_from_params(params)
    except:
        sc = None

    nll_opts = init_nlloc_from_params(params)
    nll_opts.prepare(create_time_grids=True, tar_files=False, SparkContext=sc)


parser = argparse.ArgumentParser(description='Project initialization')

parser.add_argument('control_file', type=str,
                    help="XML configuration file")

if __name__ == '__main__':
    main()


