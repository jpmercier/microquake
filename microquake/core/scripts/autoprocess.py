#!/usr/bin/env python

# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: autoprocess
#  Purpose: module to automatically process continuous data
#   Author: microquake development team
#    Email: dev@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
program to automatically process continuous data

:copyright:
    microquake development team (dev@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""


from microquake.core import read
from microquake.waveform import continuous
from microquake.core import ctl, Stream
from microquake.core.stream import read as read_stream
from microquake.core.event import Catalog
from microquake.core.stream import is_valid, composite_traces
from microquake.waveform import pick, mag
from microquake.spark import mq_map, init_spark_from_params
from microquake.db import mongo, Project
from obspy.core import read
import argparse
import numpy as np
import os
import shutil
from IPython.core.debugger import Tracer
from microquake import spark
import tempfile

def create_event_seismograms(associate_time, st, buf):
    sttrim = st.copy()
    t = associate_time
    sttrim.trim(starttime=t - buf / 2, endtime=t + buf / 2)
    return sttrim


def output_directories(event, params):
    """
    return the file path for the event and catalog files
    associated to an event and create the required directory if they do not
    exist.
    :param event: event
    :type event: ~microquake.core.data.event.event
    :param params: parameters obtained from parsing the control file
    :return: output path for waveform and event files, respectively
    """

    path_sep = os.path.sep

    base_dir_waveform = params.project.waveform_path
    base_dir_event = params.project.event_path

    ev_time = event.preferred_origin().time
    path = ev_time.strftime(params.project.template_dir)

    waveform_path = path_sep.join([base_dir_waveform,path])
    event_path = path_sep.join([base_dir_event, path])

    if not os.path.exists(waveform_path):
        os.makedirs(waveform_path)

    if not os.path.exists(event_path):
        os.makedirs(event_path)

    base_filename = ev_time.strftime(params.project.template_file)

    waveform_file = path_sep.join([waveform_path, base_filename]) + '.mseed'
    event_file = path_sep.join([event_path, base_filename]) + '.xml'

    return waveform_file, event_file


def auto_pick(st, params, project):

    if is_valid(st):
        site = params.sensors.site
        vp = params.velgrids.vp
        vs = params.velgrids.vs
        st_valid = is_valid(st, return_stream=True)
        evt_tmp = pick.automatic_picking(st_valid.copy(), site, params)
        if not evt_tmp:
            return
        evt_mag = mag.moment_magnitude(st_valid.copy(), evt_tmp, site, vp, vs)

        waveform_file, event_file = output_directories(evt_mag, params)
        st.write(waveform_file, format='mseed')
        cat_out = Catalog(events=[evt_mag])
        cat_out.write(event_file, format='quakeml')

        insert_db(evt_mag, waveform_file, event_file, project)

        # project.insert_waveform(waveform_path, )

    return True

def auto_process(ifile_tuple, control_file):
    """
    auto_process data
    :param ifile_tuple: input file tuple

    """

    # reading files
    params = ctl.parse_control_file(control_file)
    sampling_rate = params.trigger.sampling_rate
    site = params.sensors.site
    fmt = params.project.format_continuous
    fname = ifile_tuple[0]
    st1 = read_stream(fname, format=fmt, site=site)
    if ifile_tuple[1]:
        st2 = read_stream(ifile_tuple[1], format=fmt, site=site)


    # merging streams
    trs = []
    for k, tr in enumerate(st1):
        trs.append(tr.copy())
        if ifile_tuple[1]:
            trs.append(st2[k].copy())

    st = Stream(traces=trs)
    st.merge(fill_value=0)

    # trimming the resulting stream
    seismogram_length = params.trigger.seismogram_length
    starttime = st1[0].stats.starttime
    endtime = st1[0].stats.endtime + seismogram_length + 2 * params.trigger.LTA
    st = st.trim(starttime=starttime, endtime=endtime)
    st_original = st.copy()
    st.detrend('demean')
    st.detrend('linear')
    freqmin = params.trigger.freqmin
    freqmax = params.trigger.freqmax
    st.taper(type='cosine', max_percentage=0.01, max_length=0.01)
    st = st.filter('highpass', freq=freqmin)
    print(freqmin)

    # finding triggers
    trigs = continuous.trigger(st, **dict(params.trigger))
    # print trigs

    trigs_dict = {}
    trigs_dict['station'] = []
    trigs_dict['time'] = []
    for trig in trigs:
        trigs_dict['station'].append(trig[0])
        trigs_dict['time'].append(trig[1])

    if len(np.unique(trigs_dict['station'])) < 5:
        return

    association_time = params.trigger.association_time
    min_number_trigger = params.trigger.min_number_trigger
    hold_off_time = params.trigger.hold_off_time
    associated_times, trigs_time, trig_station =  continuous.associate(
        trigs_dict, association_time, min_number_trigger, hold_off_time)

    print(associated_times)
    project = Project(params.project)
    project.connect()
    for t in associated_times:
        if (t < starttime + params.trigger.LTA) or (t > endtime +
            params.trigger.STA):
            continue
        st_event = create_event_seismograms(t, st_original, seismogram_length)
        auto_pick(st_event, params, project)

    return


def insert_db(event, waveform_path, event_path, project):

    sep = os.path.sep
    waveform_base_path = os.path.sep.join(waveform_path.split(sep)[:-1])
    waveform_file = waveform_path.split(sep)[-1]
    event_base_path = os.path.sep.join(event_path.split(sep)[:-1])
    event_file = event_path.split(sep)[-1]

    inserted_id = project.insert_waveform(waveform_base_path, waveform_file)
    if not inserted_id:
        logger.error('error inserting file: %s' % waveform_fle)
        return False

    inserted_id = inserted_id[0]

    project.insert_event(event, event_base_path, event_file,
                       inserted_id)

    return


def main():

    args = parser.parse_args()

    control_file = args.control_file
    input_files = args.input_files

    params = ctl.parse_control_file(control_file)
    input_files = np.sort(input_files)

    ifile_tuples = [(if1, if2) for if1, if2 in zip(input_files[:-1],
                                                  input_files[1:])]
    ifile_tuples.append((input_files[-1], ))

    sc = spark.init_spark_from_params(params)
    site = params.sensors.site
    args = {}
    for key in params.trigger.keys():
        args[key] = params.trigger[key]
    for key in params.velgrids.keys():
        args[key] = params.velgrids[key]
    for key in params.project.keys():
        args[key] = params.project[key]
    mq_map(sc, auto_process, ifile_tuples, control_file)
    #mq_map(sc, auto_process, input_files, control_file)
    #mq_map(sc, bubu, input_files, control_file)
    # for ftup in ifile_tuples:
    #    auto_process(ftup, project, params)

# defining the command line options

parser = argparse.ArgumentParser(description='automatic processing of'
                                              'continuous data')

parser.add_argument('control_file', type=str,
                    help="XML configuration file")

parser.add_argument('input_files', nargs='+', type=str,
                    help="a list of input files")

# parser.add_argument('--', dest='local', type=bool,
#                    help=help_local)


if __name__ == '__main__':
    main()



# if params.spark_enabled:
#

