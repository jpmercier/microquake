#!/usr/bin/env python

# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: import_ESG_SEGY
#  Purpose: module to automatically process continuous data
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
program to import ESG data into the microquake system
Inputs are a list of SEGY esg segy files, a file containing system
information and a MSAccess database (mdb file)

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""
import argparse
from microquake import spark
from microquake.core import ctl, logger, UTCDateTime, read
from microquake.core.event import (Event, Catalog, Origin, Magnitude, Pick,
                                   WaveformStreamID, Arrival, Comment,
                                   ConfidenceEllipsoid, OriginUncertainty)
import numpy as np
import subprocess
from IPython.core.debugger import Tracer
from dateutil.parser import parse
from datetime import datetime
import pytz
import os
import pickle as pickle
from microquake.spark import mq_map
# TODO calculating the takeoff angle and azimuth
from microquake.simul.eik import angles, eikonal_solver


def extract_source_from_access_db(path_to_db, tz):
    """
    :param path_to_db: path to the database
    :param tz: time zone
    :return:
    """
    try:
        data = subprocess.Popen(["mdb-export", path_to_db, 'Source'],
                                 stdout=subprocess.PIPE).communicate()[0]
    except:
        logger.error('Export of the Source table failed... mdbtools '
                     'is required for this task. Installing '
                     'mdbtools may fix the problem')

    keys = data.split('\n')[0].split(',')
    data = data.split('\n')[1:]
    db = {}
    event_time = []
    # init dictionary
    for key in keys:
        db[key] = []

    for line in data:
        tmp = line.split(',')
        if len(tmp) < 2:
            continue

        dec_second = float(tmp[1])
        # is the time in the database local or UTC time?
        tmp_time = parse(tmp[0][1:-1])
        loc_time = tz.localize(tmp_time)
        event_time.append(UTCDateTime(loc_time) + dec_second)

        for k, key in enumerate(keys):
            try:
                value = float(tmp[k])
            except:
                value = tmp[k]
            db[key].append(value)

    return event_time, db


def error_ellipsoid(Exx, Eyy, Ezz, Exy, Exz, Eyz):
    """
    Takes the error matrix in cartesian coordinates and create an
    error_ellipsoid object
    :param Exx:
    :param Eyy:
    :param Ezz:
    :param Exy:
    :param Exz:
    :param Eyz:
    :return:
    """

    M = np.array([[Exx, Exy, Exz],[Exy, Eyy, Eyz], [Exz, Eyz, Ezz]])

    value, vect = np.linalg.eig(M)
    ce = ConfidenceEllipsoid()
    ce.semi_major_axis_length = value[0]
    ce.semi_intermediate_axis_length = value[1]
    ce.semi_minor_axis_length = value[2]

    # major axis
    major_hor = np.sqrt(value[0] ** 2 + value[1] ** 2)
    ce.major_axis_plunge = np.arctan2(value[2], major_hor) * 180 / np.pi
    ce.major_axis_azimuth = np.arctan2(value[0], value[1]) * 180 / np.pi

    ou = OriginUncertainty()

    ou.confidence_ellipsoid = ce

    M_hor = np.array([[Exx, Exy], [Exy, Eyy]])
    value, vect = np.linalg.eig(M_hor)


    ou.horizontal_uncertainty = value[0]
    ou.min_horizontal_uncertainty = value[1]
    ou.max_horizontal_unvertainty = value[0]

    return ou


    #TODO calculating the major axis rotation


def parse_segy(segy_file, site, event_times, db, vp, vs, project, tz):
    """
    :param segy_file: the path to a segy file
    :param site: a site object containing the information on the stations
    :param event_times: a list of event times extracted from the database
    :param db: the database converted to a dictionary
    :param params: parameter read from the control file
    :param tz: time zone for local time
    :return:
    """
    st = read(segy_file, format='ESG_SEGY', site=site)
    p_picks = []
    s_picks = []
    time_str = segy_file.split('.')[-2][-16:] + '0000'
    time_local = datetime.strptime(time_str,"%Y%m%d%H%M%S%f")
    localized_time = tz.localize(time_local)
    time = UTCDateTime(localized_time)

    dt_min = np.min(np.abs(np.array(event_times) - time))
    origin = None
    esg_db = None

    evt = Event()
    cat = Catalog()


    if dt_min > 1:
        # origin associated to a database entry

        ev_index = np.argmin(np.abs(np.array(event_times) - time))
        esg_db = {}
        for key in db.keys():
            esg_db[key] = db[key][ev_index]
        # ev_info = db[ev_index]
        moment_mag = Magnitude()
        local_mag = Magnitude()
        origin = Origin()
        origin.x = db['Easting'][ev_index]
        origin.y = db['Northing'][ev_index]
        origin.z = db['Depth'][ev_index]
        origin.evaluation_mode = 'manual'
        origin.evaluation_status = 'confirmed'
        origin.time = event_times[ev_index]

        ou = error_ellipsoid(esg_db['EE_ERROR'], esg_db['NN_ERROR'], esg_db[
            'DD_ERROR'],esg_db['NE_ERROR'], esg_db['ED_ERROR'], esg_db[
            'ND_ERROR'])

        origin.origin_uncertainty = ou

        moment_mag.magnitude_type = 'Mw'
        local_mag.magnitude_type = 'Ml'

        moment_mag.mag = db['MomentMagnitude'][ev_index]
        local_mag.mag = db['LocalMagnitude'][ev_index]

        moment_mag.evaluation_mode = 'manual'
        moment_mag.evaluation_status = 'confirmed'

        local_mag.evaluation_mode = 'manual'
        local_mag.evaluation_status = 'confirmed'

        fp_comment = Comment()
        fp_comment.text = 'fp=%f' % db['CornerFreqP'][ev_index]
        fs_comment = Comment()
        fs_comment.text = 'fs=%f' % db['CornerFreqS'][ev_index]

        moment_mag.comments = [fp_comment, fs_comment]

        ssd_comment = Comment()
        ssd_comment.text = 'static stress drop=%f' % db['StaticStressDrop'][
            ev_index]

        dsd_comment = Comment()
        dsd_comment.text = 'dynamic stress drop=%f' % db['DynamicStressDrop'][
            ev_index]

        as_comment = Comment()
        as_comment.text = 'apparent stress=%f' % db['ApparentStress'][ev_index]

        es_ep_comment = Comment()
        es_ep_comment.text = 'ES/EP=%f' % db['EsEp'][ev_index]

        asr_comment = Comment()
        asr_comment.text = 'apparent source radius=%f' % db['AspRadius'][
            ev_index]

        evt.comments = [ssd_comment, dsd_comment, as_comment, es_ep_comment,
                        asr_comment]

    picks = []
    arrivals = []
    for k, tr in enumerate(st):
        if tr.stats.channel.upper() != 'Z':
            continue
        tr_starttime = tr.stats.starttime
        trh = tr.stats.segy.trace_header
        station = tr.stats.station
        sta = site.select(station=station).stations()[0]
        sta_loc = sta.loc
        if trh.x_coordinate_of_ensemble_position_of_this_trace != 0:
            p_arrival = tr_starttime + \
                    trh.x_coordinate_of_ensemble_position_of_this_trace / 1.0e6
            pick = Pick()
            pick.time = p_arrival
            pick.phase_hint = 'P'
            if np.all(sta_loc != np.array([1000, 1000, 1000])):
                tt = eikonal_solver(vp, seed=sta_loc, seed_label=sta.code)
            else:
                continue
            pick.waveform_id = WaveformStreamID(
                network_code=tr.stats.network, station_code=tr.stats.station,
                location_code="", channel_code=tr.stats.channel)
            pick.evaluation_mode = 'manual'
            pick.evaluation_status = 'confirmed'
            picks.append(pick)
            if origin:
                arrival = Arrival()
                arrival.pick_id = pick.resource_id.id
                arrival.distance = np.linalg.norm(sta.loc - origin.loc)
                arrival.phase = pick.phase_hint
                tt_pred = tt.interpolate(origin.loc, grid_coordinate=False)
                tt_obs = pick.time - origin.time
                arrival.time_residual = tt_obs - tt_pred
                azimuth, toa = angles(tt)
                takeoff_angle = toa.interpolate(origin.loc,
                                                grid_coordinate=False)[0]
                arrival.takeoff_angle = takeoff_angle * 360 / np.pi
                azimuth = azimuth.interpolate(origin.loc,
                                              grid_coordinate=False)[0]

                arrival.azimuth = azimuth * 360 / np.pi
                arrivals.append(arrival)


        if trh.y_coordinate_of_ensemble_position_of_this_trace != 0:
            s_arrival = tr_starttime + \
                    trh.y_coordinate_of_ensemble_position_of_this_trace / 1.0e6
            pick = Pick()
            pick.time = s_arrival
            pick.phase_hint = 'S'
            tt = eikonal_solver(vs, seed=sta_loc, seed_label=sta.code)
            pick.waveform_id = WaveformStreamID(
                network_code=tr.stats.network, station_code=tr.stats.station,
                location_code="", channel_code=tr.stats.channel)
            pick.evaluation_mode = 'manual'
            pick.evaluation_status = 'confirmed'
            picks.append(pick)
            if origin:
                arrival = Arrival()
                arrival.pick_id = pick.resource_id.id
                arrival.distance = np.linalg.norm(sta.loc - origin.loc)
                arrival.phase = pick.phase_hint
                tt_pred = tt.interpolate(origin.loc, grid_coordinate=False)
                tt_obs = pick.time - origin.time
                arrival.time_residual = tt_obs - tt_pred
                azimuth, toa = angles(tt)
                takeoff_angle = toa.interpolate(origin.loc,
                                                grid_coordinate=False)[0]
                arrival.takeoff_angle = takeoff_angle * 360 / np.pi
                azimuth = azimuth.interpolate(origin.loc,
                                              grid_coordinate=False)[0]

                arrival.azimuth = azimuth * 360 / np.pi
                arrivals.append(arrival)


    evt.picks = picks


    # TODO probably modifying the QuakeML schema
    if esg_db:
        if esg_db['T'] == 'e':
            evt.event_type = 'induced or triggered event'
        elif esg_db['T'] == 'b':
            evt.event_type = 'mining explosion'
        else:
            evt.event_type = 'not reported'
    else:
         evt.event_type = 'not reported'

    if origin:
        origin.arrivals = arrivals
        evt.origins = [origin]
        evt.magnitudes = [moment_mag, local_mag]
        evt.preferred_origin_id = origin.resource_id.id
        evt.preferred_magnitude_id = moment_mag.resource_id.id


    cat.events = [evt]

    if not os.path.exists(project.waveform_path):
        os.makedirs(project.waveform_path)

    # TODO make this OS independent
    tmp = time.strftime(project.template_dir)
    waveform_path = project.waveform_path  + os.path.sep + tmp
    if not os.path.exists(waveform_path):
        os.makedirs(waveform_path)

    event_path = project.event_path  + os.path.sep + tmp
    if not os.path.exists(event_path):
        os.makedirs(event_path)

    fname = time.strftime(project.template_file)

    stpath = waveform_path + os.path.sep + fname
    evpath = event_path + os.path.sep + fname

    st_file = stpath + '.mseed'
    ev_file = evpath + '.xml'
    db_file = evpath + '.pickle'

    st.write(st_file, format='MSEED')
    cat.write(ev_file, format='QUAKEML')

    if esg_db:
        pickle.dump(esg_db, open(db_file, 'w'))

    return True


def main():
    args = parser.parse_args()

    control_file = args.control_file
    input_files = args.input_files
    path_to_db = args.AccessDB
    try:
        tz = pytz.timezone(args.tz)
    except:
        print('The time zone provided is not valid. For a list of valid time ' \
              'zone please refer to https://gist.github.com/pamelafox/986163')
        exit()

    params = ctl.parse_control_file(control_file)
    input_files = np.sort(input_files)

    ifile_tuples = [(if1, if2) for if1, if2 in zip(input_files[:-1],
                                                  input_files[1:])]
    ifile_tuples.append((input_files[-1], ))

    sc = spark.init_spark_from_params(params)
    vp = params.velgrids.grids.vp
    vs = params.velgrids.grids.vp
    project = params.project
    site = params.sensors.site

    args = {}
    for key in params.trigger.keys():
        args[key] = params.trigger[key]
    for key in params.velgrids.keys():
        args[key] = params.velgrids[key]
    for key in params.project.keys():
        args[key] = params.project[key]
    event_time, db = extract_source_from_access_db(path_to_db, tz)
    mq_map(sc, parse_segy, input_files, site, event_time, db, vp, vs, project,
           tz)
    # for segy_file in input_files:
    #    cat, esg_db = parse_segy(segy_file, site, event_time, db, params, tz)

    #    Tracer()()


    # mq_map(sc, auto_process, ifile_tuples, control_file)
    # mq_map(sc, auto_process, input_files, control_file)
    # mq_map(sc, bubu, input_files, control_file)
    # for ftup in ifile_tuples:
    #    auto_process(ftup, project, params)



parser = argparse.ArgumentParser(description='convert ESG data into a format '
                                             'compatible with the microquake '
                                             'library')

parser.add_argument('control_file', type=str,
                    help="path to XML configuration file")

parser.add_argument('AccessDB', type=str,
                    help='Path to the Access database')

parser.add_argument('input_files', nargs='+', type=str,
                    help="a list of input files")

parser.add_argument('-tz', '--time_zone', dest='tz', type=str, default='UTC',
                    help='time zone for local time (default UTC)')


if __name__ == "__main__":
    main()


