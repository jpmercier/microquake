#!/usr/bin/env python

import numpy as np
from obspy.core import event, Stream, Trace
from obspy.core.trace import Stats
from obspy.core.event import Pick, WaveformStreamID, Event, Catalog
from obspy.core.utcdatetime import UTCDateTime
from pytz import timezone
from glob import glob
from datetime import datetime, timedelta
import pandas as pd
import matplotlib.pyplot as plt
import os

from IPython.core.debugger import Tracer

import argparse

import warnings
warnings.filterwarnings("ignore")

# The values in the first line:
# 24 - Refers to the ADC digitizing at 24 bits
# 6000 - Sampling rate
# 48000 - Sampling rate of hardware, or maximum possible sampling rate.  The hardware 'looks' at the data at 48 kHz, but converts to digital format at 6 kHz.
# 0 - Don't worry about this, not relevant for now
# 1 - Don't worry about this, not relevant for now
# 1401728904 - Time in seconds in epoch time, and is measured in seconds from 1 Jan 1970 (UTC).
# 324157 - Time in microseconds.  You will see this matches part of the file name.  With final time being the epoch time (above, in seconds) + this many microseconds
# 228 - Unique netID for mine's network, Northparkes will be something different to this.
# 52 - Site ID
# 149 - Sensor ID.  Internal ID assigned to know which sensor type it is.
# 1477 - Sample number at which seismogram triggered
# 1471 - Sample number of P-pick
# 1911 - Sample number of S-pick

parser = argparse.ArgumentParser(description='Convert a single IMS ASCII files with trigger information to MSEED and QuakeML')

parser.add_argument('--idir', metavar='<seismogram file>', type=str, nargs='+',
                    help = 'full path to seismograms files associated to a single event in the following format /dir/dir')

parser.add_argument('--Assoc_Time', metavar='<Association time in ms>', type=float, default = 100.0,
                    help = 'Maximum association time window for grouping seismograms')

#parser.add_argument('sfile', metavar='<station file>', type=str,
#                    help = 'station file. The station file needs to be a four column comma delimited file (csv) without header. The columns need to be 1) site id, 2) X coordinate, 3) Y coordinate, 4) Z coordinate' )

parser.add_argument('--odir', metavar='<output directory>', type=str, default='./',
                    help='output directory. Two files will be produced: 1) a mseed file containing the time series data and metadata and 2) a XML file (QuakeML format) containing the events information (magnitude, location) and picks')

parser.add_argument('--trgfile', metavar='<input file>', type=str, default='',
                    help='trigger file associated to an event with full path (optional)')

parser.add_argument('--Network',metavar='Network', type=str, default='Net',
                   help='Network code, default is "Net"')

parser.add_argument('--tz', metavar='Time Zone', type=str, default='utc',
                    help='Time zone, for Northparkes Australia/Sydney, for resolution copper America/Phoenix or US/Mountain')

parser.add_argument('--FTR', metavar='<Faulty Trace Removal>', type=bool, default=True,
                    help='Removal of faulty trace')

parser.add_argument('--maxSeisLength', metavar='<maximum seismogram length>', type=float, default=None,
                    help='Maximum seismogram length in second')

parser.add_argument('--stfmt', metavar='<output station name format>', type=str, default='%s',
                    help='output format for station name')

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def DateTime_to_UTCDateTime(localtime, timezone):
    localtime = localtime.replace(tzinfo=timezone)
    # utctime = localtime.astimezone(timezone('UTC'))
    # utctime.replace(tzinfo = '')

    return UTCDateTime(localtime)


def parse_seismogram(sfile, tz, net, maxSeisLength, stfmt):

    data = np.loadtxt(sfile, delimiter=',', skiprows=1)
    stats = Stats()

    header = {}

    with open(sfile) as fid:
        field = fid.readline().split(',')

    stats.sampling_rate = float(field[1])
    timetmp = datetime.fromtimestamp(float(field[5]), tz=timezone('UTC')) \
      + timedelta(seconds=float(field[6]) / 1e6)  # trigger time in second

    trgtime_UTC = DateTime_to_UTCDateTime(timetmp, tz)
    stats.starttime = trgtime_UTC - float(field[10]) / stats.sampling_rate
    stats.npts = len(data)

    if '%s' in stfmt:
        stats.station = stfmt % field[8]
    else:
        stats.station = stfmt % int(field[8])
    stats.network = net

    picks = []
    if int(field[11]) != 0:
        wf_id = WaveformStreamID()
        pick = Pick()
        pick.time = stats.starttime + float(field[11]) / stats.sampling_rate
        wf_id.network_code = field[8]
        wf_id.station_code = stats.station
        pick.waveform_id = wf_id
        pick.phase_hint = 'P'
        pick.evaluation_mode = 'manual'
        pick.evaluation_status = 'preliminary'
        picks.append(pick)

    if int(field[12]) != 0:
        wf_id = WaveformStreamID()
        pick = Pick()
        pick.time = stats.starttime + float(field[12]) / stats.sampling_rate
        wf_id.network_code = field[8]
        wf_id.station_code = stats.station
        pick.waveform_id = wf_id
        pick.phase_hint = 'S'
        pick.evaluation_mode = 'manual'
        pick.evaluation_status = 'preliminary'
        picks.append(pick)

    traces = []
    component = np.array(['X', 'Y', 'Z'])
    std = np.std(data, axis=0)
    mstd = np.max(std)
    for k, dt in enumerate(data.T):
        stats.channel = '%s' % (component[k])
        if np.sum(dt) - np.mean(dt) == 0:
            continue
        if (std[k] < mstd / 10) and parser.parse_args().FTR:
            continue
        traces.append(Trace(data=np.array(dt), header=stats))

    # Trimming the seismogram so the length does not exceed maxSeisLenght
    st = Stream(traces=traces)
    if maxSeisLength:
        if (trgtime_UTC - stats.starttime) > maxSeisLength / 2:
            st.trim(starttime=trgtime_UTC - maxSeisLength / 2)
        if (stats.endtime - trgtime_UTC) > maxSeisLength / 2:
            st.trim(endtime=trgtime_UTC + maxSeisLength / 2)

    return Stream(traces=traces), picks, trgtime_UTC

if __name__ == '__main__':
    args = parser.parse_args()

    tz = timezone(args.tz)
    net = args.Network
    odir = args.odir
    maxSeisLength = args.maxSeisLength
    stfmt = args.stfmt

    network = args.Network

    Association_time = args.Assoc_Time

    Trgtimes = []
    Endtimes = []
    Streams = []
    Picks = []
    # try:
    sfiles = np.sort(glob(args.idir[0] + '/*asc'))
    first = True
    for k, sfile in enumerate(sfiles):
        sttmp, pk, trgtime = parse_seismogram(sfile,tz,net,maxSeisLength,stfmt)
        Streams.append(sttmp)
        Trgtimes.append(trgtime)
        Endtimes.append(sttmp.traces[0].stats.endtime)
        Picks.append(pk)

    stations = list(set([tr.stats.station for tr in sttmp]))

    i = np.argsort(Trgtimes)
    Trgtimes = np.array(Trgtimes)[i]
    Endtimes = np.array(Endtimes)[i]
    dTrgtimes = np.hstack((0,np.diff(Trgtimes)))
    Streams = [Streams[k] for k in i]
    Picks = np.array(Picks)[i]

    print(np.sort(Trgtimes) - np.min(Trgtimes), dTrgtimes)

    i = 0

    indices = np.nonzero(dTrgtimes > Association_time)
    indices = []
    while 1:
        dTrgtimes = np.array([float(dt) for dt in Trgtimes - Trgtimes[i]])
        tmp = np.nonzero(dTrgtimes > (Association_time / 1000.0))[0]
        if (len(tmp) == 0) or (i + 1 == len(Trgtimes)):
            indices.append(len(Trgtimes))
            break
        i = tmp[0]
        indices.append(i)

    iprev = 0

    for i in indices:
        if i - iprev < 4:
            iprev = i
            continue
        st = Stream()
        cat = Catalog()
        Pks = []
        Traces = []
        for j in range(iprev, i):
            for tr in Streams[j]:
                Traces.append(tr)
            for p in Picks[j]:
                Pks.append(p)
        st = Stream(traces=Traces)

        timetmp = Trgtimes[iprev]
        timestr = timetmp.strftime('%Y%m%d_%H%M%S_%f')
        Quakemlfle = odir + timestr + '.xml'
        mseedfle = odir + timestr + '.mseed'

        starttime_min = st.traces[0].stats.starttime
        endtime_max = st.traces[0].stats.starttime

        for tr in st:
            if tr.stats.starttime < starttime_min:
                starttime_min = tr.stats.starttime
            if tr.stats.endtime > endtime_max:
                endtime_max = tr.stats.endtime

        st.trim(starttime=starttime_min, endtime=endtime_max, pad=False)

        if not glob(args.odir):
            mkdir_p(odir)

        event = Event(picks=Pks)
        cat.events.append(event)
        print('writing')
        st.write(mseedfle, format='MSEED')
        try:
            cat.write(Quakemlfle, format='QUAKEML')
        except:
            pass
        print(mseedfle)

        iprev = i

    try:
        tz = timezone(args.tz)
    except:
        print('The time zone provided is not a valid timezone  ...  UTC will be used instead')
        tz = timezone('utc')
