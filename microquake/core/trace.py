# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: core.py
#  Purpose: Expansion of the obspy.core.trace module
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
Expansion of the obspy.core.trace module

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""

import obspy.core.trace as obstrace
from obspy.core.trace import *

class Trace(obstrace.Trace):
    def __init__(self, trace=None, **kwargs):
        super(Trace, self).__init__(**kwargs)
        if trace:
            self.stats = trace.stats
            self.data = trace.data

    def ppv(self):
        """
        Calculate the PPV for a given trace
        :return: PPV
        """
        return np.max(np.abs(self.data))

    def plot(self, **kwargs):
        from microquake.imaging.waveform import WaveformPlotting
        waveform = WaveformPlotting(stream=self, **kwargs)
        return waveform.plotWaveform()

