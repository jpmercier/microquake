import numpy as np
import matplotlib.pyplot as plt
from microquake.core.util.attribdict import AttribDict
from copy import deepcopy

from microquake.imaging import SeisPlot, Singleton
from IPython.core.debugger import Tracer

# from microquake.core.data import SeisData


class SessionManager(object):
	__metaclass__ = Singleton

	_active_session = None

	def active_session(cls):
		if not cls._active_session:
			cls._active_session = SeisSession()
		return cls._active_session

	def new_session(cls):
		cls._active_session = SeisSession()
		return cls._active_session

class SeisSession(object):

	def __init__(self, assoc=None):

		if assoc:
			self.__associations = assoc
			self.__update_indices()
		else:
			self.__associations = []
			self.__index = {}
			self.__index_reverse = {}

		self.__station_network = None

	@property
	def associations(self):
		return self.__associations

	@property
	def network(self):
		return self.__station_network

	def __add_forward(self, assoc):
		if assoc.seismograms in self.__index:
			self.__index[assoc.seismograms].append(assoc)
		else:
			self.__index[assoc.seismograms] = [assoc]

	def __add_reverse(self, assoc):
		if assoc.catalog in self.__index_reverse:
			self.__index_reverse[assoc.catalog].append(assoc)
		else:
			self.__index_reverse[assoc.catalog] = [assoc]

	def __add(self, assoc):
		self.__add_forward(assoc)
		self.__add_reverse(assoc)

	def __remove_forward(self, assoc):
		if assoc.seismograms in self.__index:
			self.__index[assoc.seismograms].remove(assoc)

	def __remove_reverse(self, assoc):
		if assoc.catalog in self.__index_reverse:
			self.__index_reverse[assoc.catalog].remove(assoc)

	def __remove(self, assoc):
		self.__remove_forward(assoc)
		self.__remove_reverse(assoc)

	def __update_indices(self):

		self.__index = {}
		self.__index_reverse = {}
		for assoc in self.__associations:
			self.__add(assoc)

	def associate(self, seis_data=None, catalog=None, site=None):

		# check for existing association first
		session = self.select(seis_data=seis_data, catalog=catalog, site=site)
		if session.associations:
			# logger.warning('Association already exists! Nothing to do.')
			return

		assoc = SeisAssociation(seis_data=seis_data, catalog=catalog, site=site)
		self.__associations.append(assoc)
		self.__add(assoc)

	def disassociate(self, seis_data=None, catalog=None, site=None):

		session = self.select(seis_data=seis_data, catalog=catalog, site=site)
		associations = session.associations

		for assoc in associations:
			self.__associations.remove(assoc)
			self.__remove(assoc)

	def load_site(self, network=None):

		self.__station_network = network

	def __search_seis_data(self, seis_data=None):

		# search in forward index
		if seis_data not in self.__index:
			# logger.error('No associations found for the given seismogram!')
			return

		return self.__index[seis_data]

	def __search_catalog(self, catalog=None):

		# search in reverse index
		if catalog not in self.__index_reverse:
			# logger.error('No associations found for the given catalog!')
			return

		return self.__index_reverse[catalog]

	def select(self, seis_data=None, catalog=None, ):

		if seis_data:

			# search in forward index
			associations = self.__search_seis_data(seis_data)
			if not catalog:
				return SeisSession(associations)
			else:
				# search in reverse index
				associations2 = self.__search_catalog(catalog)

				assoc = set(associations) & set(associations2)
				return SeisSession(assoc)
		else:
			# search in reverse index
			associations = self.__search_catalog(catalog)
			return SeisSession(associations)


class SeisAssociation(object):

	def __init__(self, seis_data=None, seis_catalog=None, site=None):

		self.__seis_data = seis_data.copy()
		self.__event_catalog = seis_catalog.copy()
		self.__site = site.copy()

	def __setattr__(self, att, value):
		if (att == "seismograms") or (att == "stream") or (att == "seis_data"):
			self.__dict__['__seis_data'] = value.copy()
		elif (att == "catalog"):
			self.__dict__['__event_catalog'] = value.copy()
		elif (att == "site"):
			self.__dict__['__site'] = value.copy()
		else:
			self.__dict__[att] = value

	@property
	def seismograms(self):
		return self.__seis_data

	@property
	def stream(self):
		return self.__seis_data

	@property
	def seis_data(self):
		return self.__seis_data

	@property
	def catalog(self):
		return self.__event_catalog

	@property
	def site(self):
		return self.__site

	def copy(self):
		return deepcopy(self)

	def select(self, network=None, station=None, channel=None, phase=None):
		sa_out = self.copy()
		cat_out = self.catalog.copy()
		events = []

		for event in self.catalog:
			event_out = event.copy()
			pks = event.picks
			if network:
				if isinstance(network, list) or isinstance(network, np.ndarray):
					network = [net.lower() for net in network]
				else:
					network = network.lower()
				picks = []
				for pck in pks:
					if pck.waveform_id.network_code.lower() in network:
						picks.append(pck)
				pks = picks

			if station:
				if isinstance(station, list) or isinstance(station, np.ndarray):
					station = [sta.lower() for sta in station]
				else:
					station = station.lower()
				picks = []
				for pck in pks:
					if pck.waveform_id.station_code.lower() in station:
						picks.append(pck)
				pks = picks

			if channel:
				if isinstance(channel, list) or isinstance(channel, np.ndarray):
					channel = [cha.lower() for cha in channel]
				else:
					channel = channel.lower()
				picks = []
				for pck in pks:
					if pck.waveform_id.channel_code.lower() == channel:
						picks.append(pck)
				pks = picks
			if phase:
				picks = []
				for pck in pks:
					if pck.phase_hint.lower() == phase.lower():
						picks.append(pck)
				pks = picks

			event_out.picks = pks
			events.append(event_out)
		cat_out.events = events

		seis_out = self.seismograms.select(network=network, station=station, channel=channel).copy()

		site_out = self.site.select(network=network, station=station, channel=channel).copy()

		sa_out.__seis_data = seis_out
		sa_out.__event_catalog = cat_out
		sa_out.__site = site_out

		return sa_out


	def plot(self, style='all'):

		seis_plt = SeisPlot()
		seis_plt.setData(self.__seis_data)
		seis_plt.setPicks(self.__event_catalog)
		seis_plt.style = style
		seis_plt.plot()
		seis_plt.show()

	def plot_picks_distance(self):

		if not self.__event_catalog or not self.__seis_data:
			return

		if not self.__event_catalog.catalog:
			return

		evt = self.__event_catalog.catalog[0]
		if not evt.origins:
			return

		origin = evt.origins[0]
		origin_time = origin.time
		origin_pos = np.array([origin.x, origin.x, origin.z])

		dist = []
		dt = []
		stname = []
		for sens in self.__seis_data.unique_stations():

			st2 = self.__seis_data.select(station=sens)
			if not st2:
				continue

			cat2 = self.__event_catalog.filterByWaveformStream(AttribDict({'station_code': sens}))
			picks = cat2.catalog[0].picks
			if not picks:
				continue

			# sens_loc = st2.traces[0].stats.station_location
			for net in site(station=sens).networks:
				if len(net) > 0:
					sens_loc = net.stations[0].loc
			dist.append(np.linalg.norm(sens_loc - origin_pos))
			dt.append(picks[0].time - origin_time)
			stname.append(sens)
		dist = np.array(dist)
		dt = np.array(dt)

		fig, (ax, ax2) = plt.subplots(2, 1)
		ax.plot(dist, dt, 'o')

		for X, Y, Z in zip(dist, dt, stname):
			ax.annotate('{}'.format(Z), xy=(X,Y), xytext=(-5, 5), ha='right',
						textcoords='offset points')

		for v in [6500., 4000.]:
			x = np.array([0, 400])
			y = x / v
			ax.plot(x, y, 'b-')

		ax2.hist(dist/dt, bins=50)

		plt.show()

