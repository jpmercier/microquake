"""
.. module:: data
   :synopsis: BIG DESCRIPTION OF EXCHANGE DATA

.. moduleauthor:: Jean-Pascal Mercier <jean-pascal.mercier@agsis.com>

@Copyright (C) 2010 Jean-Pascal Mercier

All rights reserved.

"""
import numpy as np
import pickle as pickle
import copy

from microquake.core.util.decorator import memoize

# This is the data description for the input array describing the event
#
ev_dtype = [('id',          'int'),
			('position',    'float',    (3,)),
			('delta_t',     'float')]

st_dtype = ev_dtype

tt_dtype = [('id',          'int'),
			('event_id',    'int'),
			('traveltime',  'float')]


class TTTable(object):
	"""
	This object represent a
	:param data: u
	:param staid: u
	:param evnfile: u
	:param stafile: u
	"""
	dtype = tt_dtype

	def __init__(self, data, staid, evnfile=None, stafile=None):
		try:
			# for tname, ttype in tt_dtype:
			# 	data[tname]
			import sys
			sys.stderr.write(str(data.size))
			sys.stderr.flush()
			data = np.array(data.__getitem__([tname for tname, ttype in tt_dtype]), dtype=self.dtype)
		
		except ValueError:
			data = np.asarray(data, dtype=self.dtype)

		self.data               = data
		self.data.dtype         = self.dtype
		self.station_id         = staid

		self.__evn_file__   = evnfile
		self.__sta_file__   = stafile

	def __get_event_rows__(self):
		return self.event_table.data[self.data['event_id']]
	event_rows = property(__get_event_rows__)

	@memoize
	def __get_event_table__(self):
		return pickle.load(open(self.__evn_file__))
	event_table = property(__get_event_table__)

	def __get_station_row__(self):
		return self.station_table.data[self.station_id]
	station_row = property(__get_station_row__)

	@memoize
	def __get_station_table__(self):
		return pickle.load(open(self.__sta_file__))
	station_table = property(__get_station_table__)


class PunctualData(object):
	dtype = None

	def __init__(self, data, origin=None, scale=1):
		try:
			# for tname, ttype in self.dtype:
			# 	data[tname]
			self.data = data
		except ValueError:
			self.data = np.asarray(data, dtype=self.dtype)

		self.origin = tuple([0] * data['position'].shape[-1]) if origin is None else origin
		self.scale = scale

	def __get_position_zyx__(self):
		return self.data['position']

	def __set_position_zyx__(self, pos):
		self.data['position'] = pos
	position_zyx = property(__get_position_zyx__, __set_position_zyx__)

	def __get_position_xyz__(self):
		position_zyx = self.data['position']
		position_xyz = np.empty_like(position_zyx)
		for i in range(position_zyx.shape[1]):
			position_xyz[:, i] = position_zyx[:, -(i + 1)]
		return position_xyz

	position_xyz = property(__get_position_xyz__)

	def __get_size__(self):
		return self.data.size
	size = property(__get_size__)

	def add_gaussian_noise(self, position=0, time=0):
		pass


class EventTable(PunctualData):
	dtype = ev_dtype


class StationTable(PunctualData):
	dtype = st_dtype


class ImageData(object):
	"""
	This object represent a geometric structure representing a regular
	array of points positionned in space.

	:param shape_or_data: The numpy array or the shape of the underlying data
	:param spacing: The spacing of the grid
	:param origin: A Tuple representing the position of the lower left corner \
			of the grid
	:type origin: tuple
	"""

	def __init__(self, shape_or_data=1, spacing=1, origin=None):
		
		self.data = []
		if isinstance(shape_or_data, np.ndarray):
			self.data = shape_or_data
		else:
			self.data = np.zeros(shape_or_data)

		self.origin  = tuple([0] * self.data.ndim) if origin is None else origin
		self.spacing = spacing

	def __setattr__(self, attr, value):
		self.__dict__[attr] = value

	def transform_to(self, values):
		return (values - self.origin) / self.spacing

	def transform_from(self, values):
		return values * self.spacing + self.origin

	def check_compatibility(self, other):
		return (self.shape == other.shape) and \
				(self.spacing == other.spacing) and \
				np.all(self.origin == other.origin)

	def __get_shape__(self):
		return self.data.shape
	shape = property(__get_shape__)

	# use of this function to initialize the object is discouraged
	#  as the constructor will need to be called twice
	def homogenous_like(self, value):
		data = np.empty(self.data)  # changed to empty() for performance - no memset
		data.fill(value)
		return ImageData(data, self.spacing, origin=self.origin)

	def copy(self):
		cp = copy.deepcopy(self)
		return cp

	def inGrid(self, point):
		""" Check if the point is inside the grid
		:param point: the point to check
		:type point: numpy array
		:returns: True if point is inside the grid
		:rtype: bool
		"""
		corner1 = self.origin
		corner2 = self.origin + self.spacing * np.array(self.origin)

		return np.all((point >= corner1) & (point <= corner2))


class Station():
	def __init__(self, name, location, sensor_type='geophone'):
		self.name = name
		self.location = location
		self.component = []
		self.type = 'geophone'


class Component():
	# work in progress ...
	pass
