# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: q64.py
#  Purpose: utility to encode and decode object using base64
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
Expansion of the obspy.core.event module

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""

def encode(obj):
    """Encodes any python object as a
    :py:mod:`base64` string.
    """
    import base64
    import pickle
    buf = pickle.dumps(obj, pickle.HIGHEST_PROTOCOL)
    buf2 = base64.b64encode(buf).decode('utf-8')
    buf3 = buf2.replace('\n', ' ').encode()

    return buf3


def decode(buf):
    """Decodes a :py:mod:`base64` string into a
    :py:class:`obspy.core.stream.Stream`.
    """
    import base64
    import pickle
    buf2 = '%s\n' % (buf.decode('utf-8'),)
    buf3 = buf2.replace(' ', '\n')
    data = base64.b64decode(buf3)

    obj = pickle.loads(data)

    return obj
