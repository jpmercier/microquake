from obspy.core.util.base import *
from obspy.core.util.base import _get_entry_points
from subprocess import call

# appending elements to the obspy ENTRY_POINTS
ENTRY_POINTS['grid'] = _get_entry_points('microquake.plugin.grid', 'readFormat')
ENTRY_POINTS['grid_write'] = _get_entry_points('microquake.plugin.grid',
                                               'writeFormat')
ENTRY_POINTS['site'] = _get_entry_points('microquake.plugin.site', 'readFormat')
ENTRY_POINTS['site_write'] = _get_entry_points('microquake.plugin.site',
                                               'writeFormat')

ENTRY_POINTS['mq-waveform'] = _get_entry_points('microquake.plugin.waveform',
                                                'readFormat')
ENTRY_POINTS['mq-waveform_write'] = _get_entry_points(
    'microquake.plugin.waveform', 'writeFormat')


def proc(cmd, cwd='.', silent=True):

    from microquake.core import logger

    try:
        if silent:
            cmd = '%s > /dev/null 2>&1' % cmd
        retcode = call(cmd, shell=True, cwd=cwd)
        if retcode < 0:
            logger.error('Child was terminated by signal %d' % (retcode,))
        # else:
        # print >>sys.stderr, "Child returned", retcode
    except OSError as e:
        logger.error('Execution failed: %s' % (e,))


def align_decimal(number, left_pad=7, precision=2):
    """Format a number in a way that will align decimal points."""
    outer = '{0:>%i}.{1:<%i}' % (left_pad, precision)
    inner = '{:.%if}' % (precision,)
    return outer.format(*(inner.format(number).split('.')))


def pretty_print_array(arr):
    return '(%s)' % ''.join([align_decimal(a) for a in arr])


def np_array(arr):
    new_arr = np.empty(shape=(len(arr),), dtype=object)
    for i, el in enumerate(arr):
        new_arr[i] = el
    return new_arr
