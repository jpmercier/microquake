# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: core.py
#  Purpose: Expansion of the obspy.core.event module
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
Expansion of the obspy.core.event module

:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)
"""

import obspy.core.event as obsevent
from obspy.core.event import *
# from obspy.core.event import _event_type_class_factory

from IPython.core.debugger import Tracer


# flake8 compatible function name
def read_events(*args, **kwargs):
    cat = obsevent.read_events(*args, **kwargs)
    catout = Catalog(catalog=cat)
    return catout


def make_pick(time, phase='P', wave_data=None, SNR=None, mode='automatic', status='preliminary'):

    this_pick = Pick()
    this_pick.time = time
    this_pick.phase_hint = phase
    this_pick.evaluation_mode = mode
    this_pick.evaluation_status = status
    if wave_data is not None:
        this_pick.waveform_id = WaveformStreamID(
            network_code=wave_data.stats.network,
            station_code=wave_data.stats.station,
            location_code=wave_data.stats.location,
            channel_code=wave_data.stats.channel)
    if SNR is not None:
        this_pick.comments = [Comment(text="SNR=%.3f" % SNR)]

    return this_pick


class Catalog(obsevent.Catalog):

    def __init__(self, catalog=None, **kwargs):
        super(Catalog, self).__init__(**kwargs)
        if not catalog:
            return

        for key in catalog.__dict__.keys():
            if key == 'events':
                evts = []
                for evt in catalog.__dict__[key]:
                    evts.append(Event(evt))

                self.__dict__[key] = evts

            else:
                try:
                    self.__dict__[key] = catalog.__dict__[key]
                except:
                    continue

        def to_vtk(self, filename):
            # probably still unfinished!
            import vtk

            points = vtk.vtkPoints()
            events = vtk.vtkAppendPolyData()

            for event in self.events:
                (cx, cy, cz) = event.preferred_origin.loc
                source = vtk.vtkSphereSource()
                source.SetCenter(ce, cy, cz)
                source.SetRadius(20)
                source.SetThetaResolution(100)
                source.SetPhiResolution(100)
                source.Update()

                event = vtk.vtkPolyData()
                event.ShallowCopy(source.GetOutput())

                collars.AddInputData(collar)

        def write_vtk(filename):
            pass


class Comment(obsevent.Comment):
    __doc__ = obsevent.Comment.__doc__.replace('obspy', 'microquake')

    def __init__(self, comment=None, *args, **kwargs):
        super(Comment, self).__init__(*args, **kwargs)

    def set_value(self, name, value):
        self.text = '__MICROQUAKE__, %s, %s' % (name, str(value))

    def get_value(self):
        if not self.is_microquake:
            return self.text
        value = self.text.split(',')[2]
        try:
            return int(value)
        except:
            try:
                return float(value)
            except:
                return value.strip()

    def get_name(self):
        if not self.is_microquake:
            return self.text

        return self.text.split(',')[1].strip()

    def is_microquake(self):
        if '__MICROQUAKE__' in self.text:
            return True

        return False


def add_comment(comments, name, value):
    """
    manage adding comment to a list of comments
    :param comments: a list of microquake.core.event.Comment
    :param name: field name
    :param value: field value
    :return: a list of comments
    """
    comment = Comment()
    comment.set_value(name, value)

    if not comments:
        return [comment]

    comment_id = -1
    for k, comment in enumerate(comments):
        if not comment.is_microquake():
            continue

        if name == comment.get_name():
            comment_id = k
            continue

    if comment_id == -1:
        comments.append(comment)
    else:
        comments[comment_id] = comment

    return comments

def get_comment(comments, name):
    if not comments:
        return

    for comment in comments:
        if comment.get_name() == name:
            return comment.get_value()

    return


class Event(obsevent.Event):

    # redefining few function to make the object more Cartesian coordinates
    # friendly

    __doc__ = obsevent.Event.__doc__.replace('obspy', 'microquake')

    def __init__(self, event=None, *args, **kwargs):
        super(Event, self).__init__(*args, **kwargs)

        if event:
            for key in event.__dict__.keys():
                if key == 'origins':
                    os = []
                    po_id = None
                    if event.preferred_origin():
                        po_id = event.preferred_origin_id.id
                    for origin in event.origins:
                        os.append(Origin(origin))
                        if (origin.resource_id.id == po_id):
                            self.preferred_origin_id = ResourceIdentifier(referred_object=os[-1])
                    self.__dict__[key] = os

                elif key == 'magnitudes':
                    ms = []
                    pm_id = None
                    if event.preferred_magnitude():
                        pm_id = event.preferred_magnitude_id.id
                    for magnitude in event.magnitudes:
                        ms.append(Magnitude(magnitude=magnitude))
                        if (magnitude.resource_id.id == pm_id):
                            self.preferred_magnitude_id = ResourceIdentifier(referred_object=ms[-1])
                    self.__dict__[key] = ms

                else:
                    self.__dict__[key] = event.__dict__[key]

        self.resource_id = ResourceIdentifier(referred_object=self)

    def __setattr__(self, name, value):
        if name not in self.defaults.keys():
            try:
                value = int(value)
            except:
                try:
                    value = float(value)
                except:
                    value = '%r' % value

            if 'extra' not in self.keys():
               self['extra'] = dict({})
            if name in self['extra'].keys():
                self['extra'][name]['value']= value
            else:
                self['extra'][name] = {}
                self['extra'][name]['value'] = value
                self['extra'][name]['namespace'] = 'MICROQUAKE'
        else:
            super(Event, self).__setattr__(name, value)


    def __getattr__(self, name):
        if name not in self.defaults.keys():
            if 'extra' not in self.keys():
                return

            if hasattr(self.extra, name):
                try:
                    return int(self['extra'][name]['value'])
                except:
                    try:
                        return float(self['extra'][name]['value'])
                    except:
                        return self['extra'][name]['value']
        else:
            super(Event, self).__getattr__(name)


    def short_str(self):
        """
        Returns a short string representation of the current Event.

        Example:
        Time | x | y | z | Magnitude of the first origin, e.g.
        2011-03-11T05:46:24.120000Z | 100, 200, 10 | 9.1 MW
        """
        out = ''
        origin = None
        if self.origins:
            origin = self.preferred_origin() or self.origins[0]
            print(origin)

        if self.magnitudes:
            magnitude = self.preferred_magnitude() or self.magnitudes[0]
            out += ' | %s %-2s' % (magnitude.mag,
                                   magnitude.magnitude_type)
        if origin and origin.evaluation_mode:
            out += ' | %s' % (origin.evaluation_mode)
        return out

    def insert_origin(self, origin, preferred_origin=True):
        """
        Insert a single origin to the event object. Set the origin as
        preferred origin if preferred_origin is set to True or if the event
        object does not contain an origin yet.
        :param origin: microquake.core.event.Origin
        :param preferred_origin: insert the origin as preferred origin
        :return: None
        """

        self.origins.append(origin)

        if (len(self.origins) == 1) or (preferred_origin):
            self.preferred_origin_id = origin.resource_id.id

    def insert_magnitude(self, magnitude, preferred_magnitude=True):
        """
        Insert a single magnitude to the event object. Set the magnitude as
        preferred magnitude if preferred_magnitude is set to True or if the
        event object does not contain a magnitude yet.
        :param magnitude: microquake.core.event.Magnitude
        :param preferred_magnitude: insert the magnitude as preferred magnitude
        :return: None
        """

        self.magnitudes.append(magnitude)

        if (len(self.magnitudes) == 1) or (preferred_magnitude):
            self.preferred_magnitude_id = magnitude.resource_id.id

    def __str__(self):
        """
        Print a short summary at the top.
        """
        return "Event:\t%s\n\n%s" % (
            self.short_str(),
            "\n".join(super(Event, self).__str__().split("\n")[1:]))

    def _repr_pretty_(self, p, cycle):
        p.text(str(self))


class Origin(obsevent.Origin):

    __doc__ = obsevent.Origin.__doc__.replace('obspy', 'microquake')

    def __init__(self, origin=None, **kwargs):
        from numpy import array
        super(Origin, self).__init__(**kwargs)

        from microquake.core.event import ResourceIdentifier

        if origin:
            for key in origin.__dict__.keys():
                self.__dict__[key] = origin[key]
            self.resource_id = ResourceIdentifier(referred_object=self)
            self.resource_id.set_referred_object(self)

        self.resource_id = ResourceIdentifier(referred_object=self)
        # self.loc = array([0, 0, 0])
        # self.uncertainty = 0

    def __setattr__(self, name, value):
        if name not in self.defaults.keys():
            try:
                value = int(value)
            except:
                try:
                    value = float(value)
                except:
                    value = '%r' % value

            if 'extra' not in self.keys():
                self['extra'] = dict({})
            if name in self['extra'].keys():
                self['extra'][name]['value'] = value
            else:
                self['extra'][name] = {}
                self['extra'][name]['value'] = value
                self['extra'][name]['namespace'] = 'MICROQUAKE'
        else:
            super(Origin, self).__setattr__(name, value)

    def __getattr__(self, name):
        from numpy import array
        if name == 'loc':
            return array([self.x, self.y, self.z])
        if name not in self.defaults.keys():
            if 'extra' not in self.keys():
                return

            if hasattr(self.extra, name):
                try:
                    return int(self['extra'][name]['value'])
                except:
                    try:
                        return float(self['extra'][name]['value'])
                    except:
                        return self['extra'][name]['value']
        else:
            super(Origin, self).__getattr__(name)


    def update_arrivals(self, site):
        """
        This function calculates the distance, take off angle and azimuth for a
        set of arrivals
        :param site: a ~microquake.core.site object
        """
        import numpy as np

        oloc = self.loc
        for k, arrival in enumerate(self.arrivals):
            pick = arrival.pick_id.get_referred_object()
            site_code = pick.waveform_id.station_code
            sloc = site.select(station=site_code).stations()[0].loc
            v_evt_sta = sloc - oloc
            self.arrivals[k].distance = np.linalg.norm(v_evt_sta)
            self.arrivals[k].azimuth = np.arctan2(v_evt_sta[0], v_evt_sta[1])\
                                       * 180 / np.pi
            hor = np.linalg.norm(v_evt_sta[0:2])
            self.arrivals[k].takeoff_angle = np.arctan2(hor, -v_evt_sta[2]) *\
                                             180 / np.pi

    def copy(self):
        import copy
        origin_out = copy.deepcopy(self)
        from microquake.core.event import ResourceIdentifier
        origin_out.resource_id = ResourceIdentifier()
        return origin_out


    def __str__(self, **kwargs):

        try:
            uncertainty = str(self.origin_uncertainty.confidence_ellipsoid\
                .semi_major_axis_length)

        except:
            uncertainty = ""

        string = """
           resource_id: %s
                  time: %s
                     x: %0.2f
                     y: %0.2f
                     z: %0.2f
           uncertainty: %s
       evaluation_mode: %s
     evaluation_status: %s
                    ---------
              arrivals: %d Elements
	    """ \
        % (self.resource_id, self.time.strftime("%Y/%m/%d %H:%M:%S.%f"),
        self.x, self.y, self.z, uncertainty, self.evaluation_mode,
           self.evaluation_status,\
        len(self.arrivals))

        return string


class Magnitude(obsevent.Magnitude):

    __doc__ = obsevent.Magnitude.__doc__.replace('obspy', 'microquake')

    def __init__(self, magnitude=None, **kwargs):
        from numpy import array
        super(Magnitude, self).__init__(**kwargs)

        from microquake.core.event import ResourceIdentifier

        if magnitude:
            for key in magnitude.__dict__.keys():
                self.__dict__[key] = magnitude[key]
            self.resource_id = ResourceIdentifier(referred_object=self)
            self.resource_id.set_referred_object(self)

        self.resource_id = ResourceIdentifier(referred_object=self)

    def __setattr__(self, name, value):
        if name not in self.defaults.keys():
            if not value:
                value = -999
            try:
                value = int(value)
            except:
                try:
                    value = float(value)
                except:
                    value = '%r' % value

            if 'extra' not in self.keys():
                self['extra'] = dict({})
            if name in self['extra'].keys():
                self['extra'][name]['value'] = value
            else:
                self['extra'][name] = {}
                self['extra'][name]['value'] = value
                self['extra'][name]['namespace'] = 'MICROQUAKE'
        else:
            super(Magnitude, self).__setattr__(name, value)

    def __getattr__(self, name):
        from numpy import array
        if name == 'loc':
            return array([self.x, self.y, self.z])
        if name not in self.defaults.keys():
            if 'extra' not in self.keys():
                return

            if hasattr(self.extra, name):
                try:
                    return int(self['extra'][name]['value'])
                except:
                    try:
                        return float(self['extra'][name]['value'])
                    except:
                        return self['extra'][name]['value']
        else:
            super(Magnitude, self).__getattr__(name)

    def copy(self):
        import copy
        magnitude_out = copy.deepcopy(self)
        from microquake.core.event import ResourceIdentifier
        magnitude_out.resource_id = ResourceIdentifier()
        return magnitude_out







