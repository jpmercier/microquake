from microquake.core import event

from microquake.core.event import readEvents

o = event.Origin()

o.x = 100
o.y = 200
o.z = 300

o.x_error = 100
o.y_error = 200
o.z_error = 300

if o.latitude != 200:
	print('failed')

elif o.longitude != 100:
	print('failed')

elif o.depth != 300:
	print('failed')

elif o.latitude_error != 200:
	print('failed')

elif o.longitude_error != 100:
	print('failed')

elif o.depth_error != 300:
	print('failed')

else:
	print('success')

try:
	cat = readEvents('data/20130612011057_655099.xml')
	print('success')
except:
	print('failed')
