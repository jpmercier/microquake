# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: core.py
# Purpose: functions for picking and triggering
#
#   Author: microquake development team
#    Email: devs@microquake.org
#
# Copyright (C) 2016 microquake development team
# --------------------------------------------------------------------
"""
functions for picking and triggering modified or adapted from obspy
:copyright:
    microquake development team (devs@microquake.org)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lesser.html)

"""

from IPython.core.debugger import Tracer

def ar_pick(st, f1=100, f2=2000, lta_p=5e-3, sta_p=1e-3, lta_s=10e-3,
            sta_s=5e-3, m_p=2, m_s=2, l_p=2e-3, l_s=2e-3, pick_s=True):
    """
    Wrapper around obspy.trigger.arPick function for picking
    :param st: stream
    :param f1: frequency of lower Bandpass window
    :param f2: frequency of upper Bandpass window
    :param lta_p: length of LTA for parrival in seconds
    :param sta_p: length of STA for parrival in seconds
    :param lta_s: length of LTA for sarrival in seconds
    :param sta_s: length of STA for sarrival in seconds
    :param m_p: number of AR coefficients for parrival
    :param m_s: number of AR coefficients for sarrival
    :param l_p: length of variance window for parrival in seconds
    :param l_s: length of variance window for sarrival in seconds
    :param pick_s: if true pick also S phase, else only P
    :return: microquake.core.event.Catalog
    """

    from microquake.signal.trigger import arPick
    from microquake.core.event import Pick, WaveformStreamID, Catalog, Event
    import numpy as np
    import matplotlib.pyplot as plt

    picks = []
    for station in st.unique_stations():
        st_temp = st.copy().select(station=station)
        sr = st_temp[0].stats.sampling_rate
        if len(st_temp) == 1:
            z = st_temp[0].data
            x = z
            y = z
            # x = np.zeros(len(st_temp[0]))
            # y = np.zeros(len(st_temp[0]))

        else:
            x = st_temp.copy().select(channel='X')[0].data
            y = st_temp.copy().select(channel='Y')[0].data
            z = st_temp.copy().select(channel='Z')[0].data

        (p_p, s_p) = arPick(x, y, z, sr, f1, f2, lta_p, sta_p, lta_s, sta_s,
                            m_p, m_s, l_p, l_s, s_pick=pick_s)

        t = np.arange(0, len(x)) / sr
        plt.plot(t, st_temp.composite()[0].data, 'k')
        plt.axvline(p_p, color='r')
        plt.axvline(s_p, color='r')
        plt.show()

        print p_p, s_p
        p = p_p
        s = s_p

        p_time = st_temp[0].stats.starttime + np.float(p)
        p_pick = Pick()
        p_pick.evaluation_mode = 'automatic'
        p_pick.evaluation_status = 'preliminary'
        p_pick.phase_hint = 'P'
        p_pick.time = p_time
        p_pick.waveform_id= WaveformStreamID(
            network_code=st_temp[0].stats.network,
            station_code=st_temp[0].stats.station,
            location_code=st_temp[0].stats.location,
            channel_code=st_temp[0].stats.channel)

        picks.append(p_pick)

        if not s:
            continue

        s_time = st_temp[0].stats.starttime + np.float(s)
        s_pick = Pick()
        s_pick.evaluation_mode = 'automatic'
        s_pick.evaluation_status = 'preliminary'
        s_pick.phase_hint = 'S'
        s_pick.time = s_time
        s_pick.waveform_id= WaveformStreamID(
            network_code=st_temp[0].stats.network,
            station_code=st_temp[0].stats.station,
            location_code=st_temp[0].stats.location,
            channel_code=st_temp[0].stats.channel)
        picks.append(s_pick)

    catalog = Catalog()
    catalog.events.append(Event(picks=picks))

    return catalog
