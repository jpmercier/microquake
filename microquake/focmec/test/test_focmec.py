
from microquake.focmec import core
from microquake.core import (read, readGrid, ctl)
from microquake.core.event import readEvents
import numpy as np
reload(core)

velocity = readGrid('P', format='NLLOC')

params = ctl.parse_control_file('input.xml')
site = params.sensors.site
station = site.stations()[0]
cat = readEvents('data_example.xml')
origin = cat[0].preferred_origin()
picks = cat[0].picks
stream = read('data_example.mseed')

P_v, SV_v, SH_v = core.incidence_phase_vectors(station, velocity, origin)


